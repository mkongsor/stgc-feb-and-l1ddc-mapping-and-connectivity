import numpy as np
import csv

QSL = []
LayerL = []
FebL = []
MTFL = []
SCAL = []

Mfile = 'l1ddc_mapping_20MNIWSAC00004_2020-06-08_14:37:42.csv'
wedge_mtf = '20MNIWSAC00004'

with open(wedge_mtf+'_Appendix_3_FEB_map.txt', 'r') as test:
    read = test.read().replace('\n', '  ').replace('\t','  ') 
#    
#    i = 0
    for char_idx, char in enumerate(read):
        if read[char_idx:char_idx+2] == 'QS':
#            print('########################')
#            print('QS '+read[char_idx+4])
            QSL.append(read[char_idx+4])
#            print('Layer '+read[char_idx+7])
            LayerL.append(read[char_idx+7])
#            print('FEB '+read[char_idx+10:char_idx+14])
            FebL.append(read[char_idx+10:char_idx+14])
#            print('MTF '+read[char_idx+16:char_idx+30])
            MTFL.append(read[char_idx+16:char_idx+30])
#            print('SCA '+read[char_idx+45:char_idx+51])
            SCAL.append(str(read[char_idx+45:char_idx+51]).rstrip())
#            print('########################')

#            i += 1

QSM = []
LayerM = []
FebM = []
MTFM = []
SCAM = []

test.close()

with open(Mfile, 'r') as feb:
    read = csv.reader(feb,delimiter = ',')
    for j,row in enumerate(read):
        if 2 < j < 7 or 10 < j < 15 or 18 < j < 23:
            QSM.append(str(row[0]))
            LayerM.append(str(row[2]))
            FebM.append(str(row[1]))
            MTFM.append(str(row[6]))
            SCAM.append(str(row[3]))

feb.close()

with open(Mfile, 'r') as feb:
    read = csv.reader(feb,delimiter = ',')
    for j,row in enumerate(read):
       if 6 < j < 11 or 14 < j < 19 or 22 < j < 27: 
            QSM.append(str(row[0]))
            LayerM.append(str(row[2]))
            FebM.append(str(row[1]))
            MTFM.append(str(row[6]))
            SCAM.append(str(row[3]))


for i in range(24):
    
    SCAL[i] = SCAL[i].lstrip()

    if QSL[i] != QSM[i]:
        print('Inconsistent Quad')
    if LayerL[i] != LayerM[i]:
        print('Inconsistent Layer')
    if FebL[i] != FebM[i]:
        print('Inconsistent FEB')
    if SCAL[i] != SCAM[i]:
        print('Inconsistent SCA')
        print(SCAL[i])
        print(SCAM[i])

for i in range(24):
    print(SCAL[i])
    print(SCAM[i])

