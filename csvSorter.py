import sys
import operator
import os
import csv

def sort_csv(filename, key1, key2):
    with open(filename) as fp:
        crdr = csv.reader(fp)
        # set key1 and key2 to column numbers of keys
        filedata = sorted(crdr, key=lambda row: (row[key1], row[key2]))
    return filedata
 
if __name__ == '__main__':
    # Assure in right directory
    os.chdir(os.path.abspath(os.path.dirname(__file__)))
    filename = 'st01_al_cou.csv'
    # these are column numbers zero based
    first_key = 2
    second_key = 3
    mylist = sort_csv(filename, first_key, second_key)
    for line in mylist:
        print(line)
