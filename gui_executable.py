### L1DDC/FEB Mapping GUI
### Marius Kongsore, 2020
### mkongsor@cern.ch

import sys
import os
import subprocess
import socket

import time
import linecache

import csv
import numpy as np
import json
import math
import operator as op


from datetime import datetime


from PyQt4.QtCore import *
from PyQt4.QtGui import *

os.chdir('/afs/cern.ch/user/s/stgcic/public/sca_l1ddc_mapping')

from gui_main import Ui_MainWindow as Ui
from configure_gbtx import gbtx_configuration as gbtxconfig

import csv as csv
import csvSorter as cS

import numpy as np

class MyForm(QMainWindow):
	def __init__(self, parent=None):
		
		QWidget.__init__(self, parent)

        	self.ui = Ui()
        	self.ui.setupUi(self)

#################################
### Defining GUI interactions ###
#################################

		self.ui.pingbtn.clicked.connect(self.pingclick)		
		self.ui.pingprogress.setValue(0)
	
		self.ui.pingbtn_feb.clicked.connect(self.pingclick_feb)
		self.ui.pingprogress_feb.setValue(0)
	
		self.ui.vtrxbtn.clicked.connect(self.vtrxclick)
		self.ui.vtrxprogress.setValue(0)

		self.ui.gbtx_write.clicked.connect(self.showdialog)
		self.ui.gbtx_read.clicked.connect(self.gbtx_read)
	

		self.ui.upload_btn.clicked.connect(self.write_to_csv)

		self.ui.save_btn.clicked.connect(self.save_config)
		self.ui.open_btn.clicked.connect(self.open_config)		

		self.ui.readstat.clicked.connect(self.read_phase_lock)

#######################################
### Functions for GUI functionality ###
#######################################

	def pingclick(self):

		feb_list = [getattr(self.ui,'feb'+str(i)) for i in range(8)]
		fib_list = [getattr(self.ui,'sca_fib'+str(i)) for i in range(8)]
		estat_list = [getattr(self.ui,'estat'+str(i)) for i in range(8)]

		elinks = ['3f','7f','bf','ff','13f','17f','1bf','1ff']
		elink_errors = [getattr(self.ui,'serial'+str(i)) for i in range(8)]
		elink_serials = []
		elink_serial_texts = [getattr(self.ui,'serial'+str(i)) for i in range(8)]
		elink_successes = []
		sca_output = []

		for elink_idx, elink in enumerate(elinks): # Set indicators to 
			getattr(estat_list[elink_idx],'setValue')(0)
		self.ui.pingprogress.setValue(0)

		for feb in feb_list:
			getattr(feb,'setText')('')


		# Reading SCA mapping, excluding serial numbers

		print('Importing SCA locations')

		os.chdir('/afs/cern.ch/user/s/stgcic/public/sca_l1ddc_mapping')
		with open('wedge_elink_mapping.csv') as csv_file: # Note: This file can be edited to change the default mappings

			csv_reader = csv.reader(csv_file)

			self.completed = 0

			for line_count, row in enumerate(csv_reader):

				self.ui.pingprogress.setValue(self.completed)
				self.completed += 0.5

				if line_count == 0:
					continue
				if line_count>0 and line_count<9:
					getattr(feb_list[line_count-1],'setText')(str(row[0]))
					getattr(fib_list[line_count-1],'setText')(str(row[1]))
					continue
				if line_count == 9:
					break
				else:
					print('Error reading line '+str(line_count))
					continue
		

		self.pingloc = '/afs/cern.ch/work/r/rowang/public/FELIX/ScaSoftware/build/Demonstrators/PingSca/'
		
		hostname = socket.gethostname()
		
		self.pingcmd_base = 'ping_sca --hostname '+hostname+' --from_host_port 12340 --to_host_port 12350 --elinks '
		os.chdir(self.pingloc)

		# Reading SCA Serial Numbers

		print('Retrieving SCA serial numbers')

		for elink_idx, elink in enumerate(elinks):

			self.completed += 10
			self.ui.pingprogress.setValue(self.completed)

			print('Pinging elink ' + elink)
			self.pingcmd_full = self.pingcmd_base + ' ' + elink

			self.output = subprocess.check_output(self.pingcmd_full, shell=True)
			self.last_line = self.output.splitlines()[-1:] 
			if elink_idx<4:
				print('elink '+str(elink)+' found SCA w/ serial='+str(self.last_line)[29:34])
				sca_output.append(str(self.output.splitlines()))
				elink_serials.append(str(self.last_line)[29:34])
				getattr(elink_serial_texts[elink_idx],'setText')(str(elink_serials[elink_idx]))
				getattr(estat_list[elink_idx],'setValue')(100)

			else:
				print('elink '+str(elink)+' found SCA w/ serial='+str(self.last_line)[30:35])
				sca_output.append(str(self.output.splitlines()))
				elink_serials.append(str(self.last_line)[30:35])
				getattr(elink_serial_texts[elink_idx],'setText')(str(elink_serials[elink_idx]))
				getattr(estat_list[elink_idx],'setValue')(100)


			if (str(self.last_line)[29:34]).isdigit() == False and (str(self.last_line)[30:35]).isdigit() == False:
				print('Error pinging SCA through elink '+str(elink))
				getattr(elink_serial_texts[elink_idx],'setText')('ERROR')
				getattr(estat_list[elink_idx],'setValue')(0)
				elink_serials.append('ERROR')
				
				

		self.completed = 100
		self.ui.pingprogress.setValue(self.completed)

	def pingclick_feb(self):

### Front End Board pinging 
			
                feb_list_2 = [getattr(self.ui,'feb'+str(i)+'_2') for i in range(24)]
                fib_list_2 = [getattr(self.ui,'sca_fib'+str(i)+'_2') for i in range(24)]
                estat_list_2 = [getattr(self.ui,'estat'+str(i)+'_2') for i in range(24)]
                elinks_2 = ['0','1','2','40','41','42','80','81','82','C0','C1','C2','100','101','102','140','141','142','180','181','182','1C0','1C1','1C2']
	
	
		elink_errors_2 = [getattr(self.ui,'serial'+str(i)+'_2') for i in range(24)]
                elink_serials_2 = []
                elink_serial_texts_2 = [getattr(self.ui,'serial'+str(i)+'_2') for i in range(24)]
                elink_successes_2 = []
                sca_output_2 = []

                for elink_idx, elink in enumerate(elinks_2): # Set indicators to 
                        getattr(estat_list_2[elink_idx],'setValue')(0)
                self.ui.pingprogress.setValue(0)

                for feb in feb_list_2:
                        getattr(feb,'setText')('')

                # Reading SCA mapping, excluding serial numbers

                os.chdir('/afs/cern.ch/user/s/stgcic/public/sca_l1ddc_mapping')
                with open('feb_elink_mapping.csv') as csv_file: # Note: This file can be edited to change the default mappings

                        csv_reader = csv.reader(csv_file)

                        self.completed = 0

                        for i, row in enumerate(csv_reader):

                                self.ui.pingprogress_feb.setValue(self.completed)
                                self.completed += 0.2

                                if i == 0:
                                        continue
                                if i>0 and i<9:
					for j in range(3):
                                        	getattr(feb_list_2[(i-1)*3+j],'setText')(str(row[0]))
                                        	getattr(fib_list_2[(i-1)*3+j],'setText')(str(row[1]))
                                        continue
                                if i == 10:
                                        break
                                else:
                                        print('Error reading line '+str(i))
					continue

                self.pingloc = '/afs/cern.ch/work/r/rowang/public/FELIX/ScaSoftware/build/Demonstrators/PingSca/'

                hostname = socket.gethostname()

                self.pingcmd_base = 'ping_sca --hostname '+hostname+' --from_host_port 12340 --to_host_port 12350 --elinks '
                os.chdir(self.pingloc)

                # Reading SCA Serial Numbers

                print('Retrieving SCA serial numbers')

                for elink_idx, elink in enumerate(elinks_2):

                        self.completed += 3
                        self.ui.pingprogress.setValue(self.completed)

                        print('Pinging elink ' + elink)
                        self.pingcmd_full = self.pingcmd_base + ' ' + elink

                        self.output = subprocess.check_output(self.pingcmd_full, shell=True)
                        self.last_line = self.output.splitlines()[-1:]
                        if elink_idx<12:
                                print('elink '+str(elink)+' found SCA w/ serial='+str(self.last_line)[29:34])
                                sca_output_2.append(str(self.output.splitlines()))
                                elink_serials_2.append(str(self.last_line)[29:34])
                                getattr(elink_serial_texts_2[elink_idx],'setText')(str(elink_serials_2[elink_idx]))
                                getattr(estat_list_2[elink_idx],'setValue')(100)

                        else:
                                print('elink '+str(elink)+' found SCA w/ serial='+str(self.last_line)[30:35])
                                sca_output_2.append(str(self.output.splitlines()))
                                elink_serials_2.append(str(self.last_line)[30:35])
                                getattr(elink_serial_texts_2[elink_idx],'setText')(str(elink_serials_2[elink_idx]))
                                getattr(estat_list_2[elink_idx],'setValue')(100)


                        if (str(self.last_line)[29:34]).isdigit() == False and (str(self.last_line)[30:35]).isdigit() == False:
                                print('Error pinging SCA through elink '+str(elink))
                                getattr(elink_serial_texts_2[elink_idx],'setText')('ERROR')
                                getattr(estat_list_2[elink_idx],'setValue')(0)
                                elink_serials_2.append('ERROR')

        #       self.ui.sca_out.setText(str(sca_output))
                self.completed = 100
                self.ui.pingprogress.setValue(self.completed)
	
	def vtrxclick(self):
		
		vtrx_list = [getattr(self.ui,'vtrx'+str(i)) for i in range(12)]
		vtrx_led_list = [getattr(self.ui,'vtr_stat'+str(i)) for i in range(12)]
		vtrx_command = 'flx-info GBT'
		self.output = subprocess.check_output(vtrx_command, shell=True)
		self.status_line = self.output.splitlines()[-6]


		self.vtrxcompleted = 0
		chan_nbr = 0
		for elem in self.status_line:

			if elem=='Y':
				print('Channel number '+str(chan_nbr)+' is aligned.')
				getattr(vtrx_led_list[chan_nbr],'setValue')(100)
				getattr(vtrx_list[chan_nbr],'setText')('YES')
				chan_nbr += 1

			if elem=='N':
				print('ERROR. Channel number '+str(chan_nbr)+' is NOT aligned.')
				getattr(vtrx_led_list[chan_nbr],'setValue')(0)
				getattr(vtrx_list[chan_nbr],'setText')('NO')
				chan_nbr += 1
			
			self.vtrxcompleted += 9
			self.ui.vtrxprogress.setValue(self.vtrxcompleted)

		self.vtrxcompleted = 100
		self.ui.vtrxprogress.setValue(self.vtrxcompleted)

	def close_event(self,event):
		reply = QMessageBox.question(self, 'Message',
		"Are you sure you want to quit?", QMessageBox.Yes |
		QMessageBox.No, QMessageBox.No)

		if reply == QMessageBox.Yes:
			event.accept()
		else:
			event.ignore()

	def gbtx_config(self):

		gbtx_config_loc = '/afs/cern.ch/user/s/stgcic/public/sca_l1ddc_mapping'
		os.chdir(gbtx_config_loc)

		mtf_list = [getattr(self.ui,'mtf'+str(i)) for i in range(8)]
		mtf_id = []

		phase_labels = [getattr(self.ui,'phase'+str(i)) for i in range(5)]
		phase_values = np.zeros((5))
		gbtx_labels = [getattr(self.ui,'gbtx'+str(i)) for i in range(8)]
		gbtx_values = np.zeros((8))
		flx_labels = [getattr(self.ui,'flx'+str(i)) for i in range(8)]
		flx_values = np.zeros((8))		

		for p_idx, phase_val in enumerate(phase_values):

			if str(getattr(phase_labels[p_idx],'text')()).isdigit()==True:
				phase_values[p_idx] = int(getattr(phase_labels[p_idx],'text')())
			else:
				print('Invalid configuration!')
				return

		for g_idx, gbtx_val in enumerate(gbtx_values):

			if str(getattr(gbtx_labels[g_idx],'text')()).isdigit()==True:
				gbtx_values[g_idx] = int(getattr(gbtx_labels[g_idx],'text')())
			else:
				print('Invalid configuration!')
				return


		for f_idx, flx_val in enumerate(flx_values):

			if str(getattr(flx_labels[f_idx],'text')()).isdigit()==True:
				flx_values[f_idx] = int(getattr(flx_labels[f_idx],'text')())
			else:
				print('Invalid configuration!')
				return

		for i in range(8):

			mtf_id.append(str(getattr(mtf_list[i],'text')()))

		gbtxconfig(gbtx_values[0],gbtx_values[1],gbtx_values[2],gbtx_values[3],gbtx_values[4],gbtx_values[5],gbtx_values[6],gbtx_values[7],flx_values[0],
			flx_values[1],flx_values[2],flx_values[3],flx_values[4],flx_values[5],flx_values[6],flx_values[7],
			phase_values[0],phase_values[1],phase_values[2],phase_values[3],phase_values[4])	

		config_dict = {"gbtx0": gbtx_values[0], "gbtx1": gbtx_values[1], "gbtx2":gbtx_values[2], "gbtx3":gbtx_values[3], 
			"gbtx4":gbtx_values[4], "gbtx5":gbtx_values[5], "gbtx6":gbtx_values[6], "gbtx7": gbtx_values[7],
			"flx0": flx_values[0], "flx1":flx_values[1], "flx2":flx_values[2], "flx3":flx_values[3], 
			"flx4":flx_values[4], "flx5":flx_values[5], "flx6":flx_values[6], "flx7":flx_values[7], 
			"phase0":phase_values[0], "phase1":phase_values[1], "phase2":phase_values[2], "phase3":phase_values[3], "phase4":phase_values[4],
			"mtf0":mtf_id[0],"mtf1":mtf_id[1],"mtf2":mtf_id[2],"mtf3":mtf_id[3],"mtf4":mtf_id[4],"mtf5":mtf_id[5],"mtf6":mtf_id[6],"mtf7":mtf_id[7]
				}

		with open('config.txt', 'w') as json_file:
			json.dump(config_dict, json_file)

	def showdialog(self):
	   	msg = QMessageBox()
	   	msg.setIcon(QMessageBox.Information)
		msg.setText("Are you sure you want to configure?")
		msg.setWindowTitle("Configuration dialog")
		msg.setStandardButtons(QMessageBox.Ok | QMessageBox.Cancel)
		msg.buttonClicked.connect(self.gbtx_config)
		msg.exec_()

	def gbtx_read(self):
		
		phase_labels = [getattr(self.ui,'phase'+str(i)) for i in range(5)]
		gbtx_labels = [getattr(self.ui,'gbtx'+str(i)) for i in range(8)]
		flx_labels = [getattr(self.ui,'flx'+str(i)) for i in range(8)]

		phase_values = np.zeros((5))
		gbtx_values = np.zeros((8))
		flx_values = np.zeros((8))

		gbtx_read_loc = '/afs/cern.ch/user/s/stgcic/public/sca_l1ddc_mapping/GBTXconfigs'
		os.chdir(gbtx_read_loc)

		files = ['f'+str(i) for i in range(8)]
		gbtx_list = [getattr(self.ui,'gbtx'+str(i)) for i in range(8)]
		gch_list = [getattr(self.ui,'gch'+str(i)+'_lab') for i in range(8)]

		for f_idx,f in enumerate(files):
			with open("read_card0_link"+str(f_idx)+"_gbt1.txt","r") as fin:

				data = fin.read()	
					
				getattr(gch_list[f_idx],'setText')(data)

			fin.close()

			gbtx_settings = str(getattr(gch_list[f_idx],'text')()).split('\n',4)[-1]
			print(gbtx_settings)

			hex_list = gbtx_settings.split()			

			hex_list = [s for s in hex_list if ':' not in s]
			hex_list = [s for s in hex_list if 's' not in s]
			hex_list = [s for s in hex_list if ')' not in s]			

			bin_list = []
			num_of_bits = 8
			scale = 16

			for i, entry in enumerate(hex_list):
				n = int(hex_list[i],16)
				bStr = ''
				while n > 0:
					bStr = str(n % 2) + bStr
					n = n >> 1
				res = str(bStr)
			
				length = len(res)
				for n in range(8-length):
					res = '0'+res

				bin_list.append(res)
			os.chdir('/afs/cern.ch/user/s/stgcic/public/sca_l1ddc_mapping/GBTXconfigs')	
			with open('gbtx'+str(f_idx)+'_binary.txt', 'w') as store:
				for item in bin_list:
					store.write("%s\n" % item)

			print('Saved Hex Configurations')
			

	#		for i, label in enumerate(phase_labels):
	#			
	#			gbtx_values[i] = eval('gbtx'+str(i))
	#			flx_values[i] = eval('flx'+str(i))
	#					
	#			getattr(gbtx_list[i], 'setText')(gbtx_values[i])
	#			print('Channel '+str(i)+' GBTX connection last configured to '+str(gbtx_values[i]))

	#			getattr(flx_list[i], 'setText')(flx_values[i])
	#			print('Channel '+str(i)+' FLX-card connection last configured to '+str(flx_values[i]))

	#		if i < 5:
					
	#			phase_values[i] = eval('phase'+str(i))
	#			print('Group '+str(i)+' phase last configured to '+str(phase_values[i]))

	def open_config(self):
		
		os.chdir('/afs/cern.ch/user/s/stgcic/public/sca_l1ddc_mapping/savedSettings')
		workfile = QFileDialog.getOpenFileName(self, 'Open file', 
   '/afs/cern.ch/user/s/stgcic/public/sca_l1ddc_mapping/savedSettings/',"Settings files (*.csv *.txt)")	
		
		workfile_text = str(workfile)		
		self.ui.fileloc.setText(workfile_text)
		
	def save_config(self):
		
		os.chdir('/afs/cern.ch/user/s/stgcic/public/sca_l1ddc_mapping')
		
		phase_labels = [getattr(self.ui,'phase'+str(i)) for i in range(5)]
		gbtx_list = [getattr(self.ui,'gbtx'+str(i)) for i in range(8)]
		flx_labels = [getattr(self.ui,'flx'+str(i)) for i in range(8)]

		g = []
		f = []
		p = []

		for i in range(8):
			g.append(str(getattr(gbtx_list[i],'text')()))
			f.append(str(getattr(flx_labels[i],'text')()))

		for i in range(5):
			p.append(str(getattr(phase_labels[i],'text')()))			

		settings_list = g+f+p
		
		with open('gbtx_settings.txt', 'w') as filehandle:
			for listitem in settings_list:
				filehandle.write('%s\n' % listitem)
			
		saveFile = QFileDialog.getSaveFileName(self, 'Save file', 
   			'/afs/cern.ch/user/s/stgcic/public/sca_l1ddc_mapping/savedSettings/',"Confiugration files (*.txt *.csv)")	
		
	def write_to_csv(self):

		elink_serial_texts = [getattr(self.ui,'serial'+str(i)) for i in range(8)]
		feb_list = [getattr(self.ui,'feb'+str(i)) for i in range(8)]
		fib_list = [getattr(self.ui,'sca_fib'+str(i)) for i in range(8)]
		vtrx_list = [getattr(self.ui,'vtrx'+str(i)) for i in range(12)]
		phase_labels = [getattr(self.ui,'phase'+str(i)) for i in range(5)]

		os.chdir('/afs/cern.ch/user/s/stgcic/public/sca_l1ddc_mapping/outputConfigs')
		now = str((datetime.now()).replace(microsecond=0)).replace(' ', '_')
		wedge = self.ui.wedgeid.text()
		mtf_list = [getattr(self.ui,'mtf'+str(i)) for i in range(8)]
		
		j=8

		with open('l1ddc_mapping_'+str(wedge)+'_'+now+'.csv', mode='w') as l1ddc_settings_file:
			config_writer = csv.writer(l1ddc_settings_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
			config_writer.writerow(['APPENDIX 5 - STGC L1DDC MAPPING'])
			config_writer.writerow([str(wedge)])
			config_writer.writerow([])
			config_writer.writerow([now])
			config_writer.writerow([])
			config_writer.writerow(['L1DDC','LAYER','FEB', 'SCA ID', 'FELIX FIBERS', 'VTRX 1 CONNECTION', 'VTRX 2 CONNECTION', 'MTF NBR'])

			for i in range(8):
				if i==0 or i==2 or i==4 or i==6:
					config_writer.writerow([str(getattr(feb_list[i],'text')())[0:6], str(getattr(feb_list[i],'text')())[7:13], str(getattr(feb_list[i],'text')())[14:18], str(getattr(elink_serial_texts[i],'text')()), str(getattr(fib_list[i],'text')()),str(getattr(vtrx_list[i],'text')()),str(getattr(vtrx_list[j],'text')()),'20MNEL1CT10'+str(getattr(mtf_list[i],'text')())])
					j+=1
				else:
					config_writer.writerow([str(getattr(feb_list[i],'text')())[0:6], str(getattr(feb_list[i],'text')())[7:13], str(getattr(feb_list[i],'text')())[14:18], str(getattr(elink_serial_texts[i],'text')()), str(getattr(fib_list[i],'text')()),str(getattr(vtrx_list[i],'text')()),'','20MNEL1CT10'+str(getattr(mtf_list[i],'text')())])

			print('l1ddc_mapping_'+str(wedge)+'_'+now+'.csv')
			new_csv  = cS.sort_csv('l1ddc_mapping_'+str(wedge)+'_'+now+'.csv',0,1)
			print(new_csv)



		#	config_writer.writerow([])
		#	
		#	config_writer.writerow(['GROUP', 'PHASE'])
		#	for i in range(5):
		#		config_writer.writerow([str(i), getattr(phase_labels[i],'text')()])
		os.chdir('/afs/cern.ch/user/s/stgcic/public/sca_l1ddc_mapping')
		#os.system('chmod +x ./upload.sh')
		#os.system('./upload.sh /afs/cern.ch/user/m/mkongsor/public/L1DDC/outputConfigs/l1ddc_settings_file.csv l1ddc')


####
# For FEBs
###

                feb_list_2 = [getattr(self.ui,'feb'+str(i)+'_2') for i in range(24)]
                fib_list_2 = [getattr(self.ui,'sca_fib'+str(i)+'_2') for i in range(24)]
                estat_list_2 = [getattr(self.ui,'estat'+str(i)+'_2') for i in range(24)]
                elinks_2 = ['0','1','2','40','41','42','80','81','82','C0','C1','C2','100','101','102','140','141','142','180','181','182','1C0','1C1','1C2']

                elink_errors_2 = [getattr(self.ui,'serial'+str(i)+'_2') for i in range(24)]
                elink_serials_2 = [] 
                elink_serial_texts_2 = [getattr(self.ui,'serial'+str(i)+'_2') for i in range(24)]
                elink_successes_2 = []
                sca_output_2 = []

		if str(wedge).strip() == str():
			self.alert = QMessageBox()
			self.alert.setIcon(QMessageBox.Critical)
			self.alert.setText("Please enter the wedge ID")
			self.alert.setWindowTitle("Warning dialog")
			self.alert.setStandardButtons(QMessageBox.Ok)
			self.alert.exec_()
			return

		os.chdir('/afs/cern.ch/user/s/stgcic/public/sca_l1ddc_mapping/outputConfigs')
		
 		j=8

                with open('feb_mapping_'+str(wedge)+'_'+now+'.csv', mode='w') as feb_settings_file:
                        config_writer = csv.writer(feb_settings_file, delimiter=',', quotechar='"', quoting=csv.QUOTE_MINIMAL)
                        config_writer.writerow(['STGC FEB MAPPING', str(wedge), now])
                        config_writer.writerow([])
                        config_writer.writerow(['QUADRUPLET', 'FEB', 'LAYER', 'L1DDC', 'SCA ID', 'FELIX FIBERS'])
			q_idx=1
                        for i in range(24):
				if q_idx==4:
					q_idx=1
                               	
				config_writer.writerow([str(q_idx), str(getattr(feb_list_2[i],'text')())[14:18],str(getattr(feb_list_2[i],'text')())[7:13],str(getattr(feb_list_2[i],'text')())[0:6], str(getattr(elink_serial_texts_2[i],'text')()), str(getattr(fib_list_2[i],'text')()),str()])
 	                      	q_idx+=1
###
#		with open('feb_mapping_'+str(wedge)+'_'+now+'.csv') as csvfile:
#    			spamreader = csv.DictReader(csvfile, delimiter=";")
#    			sortedlist = sorted(spamreader, key=lambda row:(row[0],row[1]), reverse=False)

#		with open('feb_mapping_'+str(wedge)+'_'+now+'.csv', mode='w') as f:
#			fieldnames = ['column_1', 'column_2', 'column_3']
#			writer = csv.DictWriter(f, fieldnames=fieldnames)
#			writer.writeheader()

#    			for row in sortedlist:
#        			writer.writerow(row)

		print('Mapping saved')
		
		print('Mapping uploaded')

	def interpret_reg(self):

		os.chdir('/afs/cern.ch/user/s/stgcic/public/sca_l1ddc_mapping/GBTXconfigs')

		for gbtx_idx in range(8):
			with open('gbtx'+str(gbtx_idx)+'_binary.txt', "r") as bin_list:
				with open('gbtx'+str(gbtx_idx)+'_key_regs.csv', "w"):
					for bin_idx, binary in enumerate(bin_list):
	# All the registries we care about:
						if 61<bin_idx<64 or 65<bin_idx<88 or 89<bin_idx<112 or 113<bin_idx<136 or 137<bin_idx<160 or 161<bin_idx<184 or 185<bin_idx<208 or 209<bin_idx<234 or 236<bin_idx<239 or bin_idx == 241 or bin_idx == 245 or bin_idx == 248 or bin_idx == 251 or 332<bin_idx<335 or 335<bin_idx<338 or 338<bin_idx<341 or 341<bin_idx<344 or 344<bin_idx<347 or 347<bin_idx<350 or 350<bin_idx<353 or 353<bin_idx<356 or 356<bin_idx<359 or 359<bin_idx<362 or 389<bin_idx<398:
							for entry_idx, entry in enumerate(binary[::-1]): # Flip the binary string to match the CSV file
								if entry == 1:
									print('hi')


	def read_phase_lock(self):
		

		plock_list0 = [getattr(self.ui,'plock'+str(i)+'_0') for i in range(8)]
		plock_list1 = [getattr(self.ui,'plock'+str(i)+'_1') for i in range(8)]
		plock_list2 = [getattr(self.ui,'plock'+str(i)+'_3') for i in range(8)]
		plock_list3 = [getattr(self.ui,'plock'+str(i)+'_4') for i in range(8)]
		plock_list4 = [getattr(self.ui,'plock'+str(i)+'_5') for i in range(8)]
		plock_list5 = [getattr(self.ui,'plock'+str(i)+'_6') for i in range(8)]
		plock_list6 = [getattr(self.ui,'plock'+str(i)+'_7') for i in range(8)]
		plock_list7 = [getattr(self.ui,'plock'+str(i)+'_8') for i in range(8)]

		checks = np.ones((8,8))
		
		os.chdir('/afs/cern.ch/user/s/stgcic/public/sca_l1ddc_mapping/GBTXconfigs/')
		for i in range(8):
			with open('gbtx'+str(i)+'_binary.txt',"r") as binary_file:

				print('L1DDC '+str(i)+' starts here')
				for bin_idx, binary in enumerate(binary_file):

					if bin_idx == 390:
						for entry_idx, entry in enumerate(binary):
							if entry == str(0) and entry_idx < 5:
								checks[0]=0
								print('GBTX'+str(i)+' EportRX dll group '+str(entry_idx)+' is UNLOCKED')
					if bin_idx == 391:
						for entry_idx, entry in enumerate(binary):
							if entry == str(0):
								checks[1]=0
								print('GBTX'+str(i)+' EportRX phase aligner group 0 channel ' + str(entry_idx) + ' is UNLOCKED')

					if bin_idx == 392:
						for entry_idx, entry in enumerate(binary):
							if entry == str(0):
								checks[2]=0
								print('GBTX'+str(i)+' EportRX phase aligner group 1 channel ' + str(entry_idx) + ' is UNLOCKED')


					if bin_idx == 393:
						for entry_idx, entry in enumerate(binary):
							if entry == str(0):
								checks[3]=0
								print('GBTX'+str(i)+' EportRX phase aligner group 2 channel ' + str(entry_idx) + ' is UNLOCKED')


					if bin_idx == 394:
						for entry_idx, entry in enumerate(binary):
							if entry == str(0):
								checks[4]=0
								print('GBTX'+str(i)+' EportRX phase aligner group 3 channel ' + str(entry_idx) + ' is UNLOCKED')


					if bin_idx == 395:
						for entry_idx, entry in enumerate(binary):
							if entry == str(0):
								checks[5]=0
								print('GBTX'+str(i)+' EportRX phase aligner group 4 channel ' + str(entry_idx) + ' is UNLOCKED')


					if bin_idx == 396:
						for entry_idx, entry in enumerate(binary):
							if entry == str(0):
								checks[6]=0
								print('GBTX'+str(i)+' EportRX phase aligner group 5 channel ' + str(entry_idx) + ' is UNLOCKED')

					if bin_idx == 397:
						for entry_idx, entry in enumerate(binary):
							if entry == str(0):
								checks[7]=0
								print('GBTX'+str(i)+' EportRX phase aligner group 6 channel ' + str(entry_idx) + ' is UNLOCKED')

		for j in range(8):
			for i, check in enumerate(checks[j,:]):
				if check == str(0):
					getattr(plock_list[i],'setValue')(100)
				if check == str(1):
					print('GBTX'+str(j)+'Group ' + str(i) + ' is LOCKED')

		print('Lock scan completed')

	def openProduction(self):
	
		os.chdir('/afs/cern.ch/user/s/stgcic/public/sca_l1ddc_mapping/outputConfigs')
		productionFile = QFileDialog.getOpenFileName(self, 'Choose production file', 
   '/afs/cern.ch/user/s/stgcic/public/sca_l1ddc_mapping/outputConfigs/',"Settings files (*.txt)")

		productionFileText = str(productionFile)
		
#		for char, char_idx in enumerate(productionFileText)

	# Check for VTRX OK		

#		if productionFileText[char_idx+0:char_idx+4] == str(VTRX1) and productionFileText[char_idx+13:char_idx+15] == str(OK):
#				print ('P VTRX status is ' + productionFileText[char_idx+13:char_idx+15]
#		else:
#				print('ERROR')
	
#		if productionFileText[char_idx+0:char_idx+4] == str(VTRX2) and productionFileText[char_idx+13:char_idx+15] == str(OK):

#				print ('P VTRX status is ' + productionFileText[char_idx+13:char_idx+15]
#		else:
#				print('ERROR')
			
	# Check for SCA ID

#		if productionFileText[char_idx+0:char_idx+5] == str(SCA_ID) and productionFileText[char_idx+7:char_idx+11].is_integer() == True:
#				print('Production SCA ID is ' + productionFileText[char_idx+7:char_idx+11]
#			continue
#		else:
#			print('ERROR')
#			continue

#	def openProduction(self):
#
#		os.chdir('afs/cern.ch/user/mkongsor/public/L1DDC/outputConfigs')
#		digitalFile = QFileDialog.getOpenFileName(self,'Choose GUI generated file','/afs/cern.ch/user/m/mkongsor/public/L1DDC/outputConfigs/',"Settings files (*.csv)")

#		reader = csv.DictReader(digitalFile)
#			for row in reader:

#				if row == 0:
#					continue

#				if row == 1:
					

#		digitalFileText = str(digitalFile)
		
#		for char, char_idx in enumerate(productionFileText):
#
#			if digitalFlieText[char_idx:char_idx+4] == 'VTRX1':
#				continue
#
#			if digitalFileText[char_idx:char_idx+4] == 'VTRX2':
#				continue
#			
#			if digitalFileText[char_idx:char_idx+4] == 'SCAID':
#				continue
	

#	def l1ddc_compare:
			
#			if digitalFileText[char_idx:char_idx+1] == 'QS'

#			if digitalFileTex

#	def databaseDown(self): # Dwonload stuff from Andy's database.
		
		
		
		

if __name__ == '__main__':
	app = QApplication(sys.argv)
	mainwin = MyForm()
	mainwin.show()
    	sys.exit(app.exec_())
