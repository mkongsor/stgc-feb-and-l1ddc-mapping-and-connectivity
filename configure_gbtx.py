# This script interfaces with the GBTX/SCA GUI and configures the GBTXs
# Original Author: Rongkun Wang
# GUI Adaptation: Marius Kongsore

import os
import sys
import numpy as np
import argparse

sys.path.append("/afs/cern.ch/user/r/rowang/public/MyPythonUtilities")
import MyPythonSystemUtil as mpsu

sys.path.append("/afs/cern.ch/work/r/rowang/public/FELIX/GBTXConfig/")
from other_words import *
#from GBTXConfigHandler import  *

parser = argparse.ArgumentParser(description='Config gbtx.')
parser.add_argument("-t", "--train", action = "store_true",
        help = "This will have to be run first, to train the phases. then we can do static phase setting, in the same directory!",)
parser.add_argument("-n", "--not_inspect", action = "store_true",
        help = "This will cause the script not to inspect DLL(brute)!",)
args = parser.parse_args()

def gbtx_configuration(gbtx0,gbtx1,gbtx2,gbtx3,gbtx4,gbtx5,gbtx6,gbtx7,flx0,flx1,flx2,flx3,flx4,flx5,flx6,flx7,phase0,phase1,phase2,phase3,phase4):

	f=True

	#########################################################
	#  GUI Inputs go here - nothing above needs to be changed
	#########################################################

	# 1st number is fiber number / GBT link number - this is fixed, NOT the array ordering
	# 2nd number is which GBTx the fiber is connected to (1/2) on L1DDC (need to change I2C address)
	# 3rd number is flx-card (0/1 potentiall 2/3) for flx-card device to use
	l_gbtxn = [
		(0, gbtx0, flx0),
		(1, gbtx1, flx1),
		(2, gbtx2, flx2),
		(3, gbtx3, flx3),
		(4, gbtx4, flx4),
		(5, gbtx5, flx5),
		(6, gbtx6, flx6),
		(7, gbtx7, flx7),
		]

	# group 0-4 phase selection, subject to flx fmw
	g0_phase = phase0
	g1_phase = phase1
	g2_phase = phase2
	g3_phase = phase3
	g4_phase = phase4

	############################################
	# Nothing below needs to be changed
	############################################

	# words go into a gbtx config file
	# each word is one byte

	# these group0 phase words are gbt ic address specific.
	gbt_dict={
		1:words_68_77_gbtx1,
		2:words_68_77_gbtx2
		}

	os.system("mkdir -p GBTXconfigs")

	l_faulty = []
	l_not_locked = []

	for fiberNo, ICaddr, flx_card in l_gbtxn:

	    # default config
	    l_words = [
		    words_0_61,
		    phase_mode["training"],
		    # 63 inEportCtr1    group0
		    dict_phase[g0_phase],
		    words_64_67,
		    gbt_dict[ICaddr],
		    words_78_86,
		    # 87 inEportCtr25   group1
		    dict_phase[g1_phase],
		    words_88_110,
		    # 111 inEportCtr49  group2
		    dict_phase[g2_phase],
		    words_112_134,
		    # 135 inEportCtr73  group3
		    dict_phase[g3_phase],
		    words_136_158,
		    # 159 inEportCtr97  group4
		    dict_phase[g4_phase],
		    words_160_368,
		    ]

    	con = GBTXConfigHandler("write_list", l_words, flx_card, fiberNo, ICaddr)

        if args.train:
       		 # first upload the very first config with IC
       		 print("===> upload all config")
       		 con.upload_config()

       		 # do the training! or just checking directly!
       		 if not con.train_phases():
       			 #  if not con.inspect_lock():
           		 l_not_locked.append(con.read_file_name)

        	# read back config in a file for phases
       		 con.read_config()
   	else:
        	l_faulty.append( con.read_phase() )
       		con.upload_config()
        	pass
    	pass


	print("\n")
	for error in l_faulty:
	    if error:
		# None means success
		print(mpsu.TRed("WARNING") + ": file {0} is faulty, please check if there's register values inside! It's very likely you didn't open this elink or you have some felix issue.".format(error))
	for error in l_not_locked:
	    # None means success
	    print(mpsu.TRed("WARNING") + ": training of {0}, the DLL is not locked! Phase readback might not be valid. Search for INSPECT in the terminal for more details".format(error))

	# reset the TH_FanOut to none for both cards, 
	# this is fail-safe if you didn't lock THFO
	os.system("femu -n -d 0")
	os.system("femu -n -d 1")
