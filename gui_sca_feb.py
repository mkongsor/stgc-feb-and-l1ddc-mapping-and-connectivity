# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow2.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_SideWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(432, 864)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.verticalLayoutWidget_3 = QtGui.QWidget(self.centralwidget)
        self.verticalLayoutWidget_3.setGeometry(QtCore.QRect(0, 0, 431, 841))
        self.verticalLayoutWidget_3.setObjectName(_fromUtf8("verticalLayoutWidget_3"))
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.verticalLayoutWidget_3)
        self.verticalLayout_3.setMargin(0)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.frame_2 = QtGui.QFrame(self.verticalLayoutWidget_3)
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.feb7 = QtGui.QLabel(self.frame_2)
        self.feb7.setGeometry(QtCore.QRect(50, 320, 151, 20))
        self.feb7.setText(_fromUtf8(""))
        self.feb7.setObjectName(_fromUtf8("feb7"))
        self.feb3 = QtGui.QLabel(self.frame_2)
        self.feb3.setGeometry(QtCore.QRect(50, 200, 151, 20))
        self.feb3.setText(_fromUtf8(""))
        self.feb3.setObjectName(_fromUtf8("feb3"))
        self.febadd_label = QtGui.QLabel(self.frame_2)
        self.febadd_label.setGeometry(QtCore.QRect(110, 80, 31, 21))
        self.febadd_label.setObjectName(_fromUtf8("febadd_label"))
        self.feb1 = QtGui.QLabel(self.frame_2)
        self.feb1.setGeometry(QtCore.QRect(50, 140, 151, 20))
        self.feb1.setText(_fromUtf8(""))
        self.feb1.setObjectName(_fromUtf8("feb1"))
        self.elff = QtGui.QLabel(self.frame_2)
        self.elff.setGeometry(QtCore.QRect(10, 200, 21, 17))
        self.elff.setObjectName(_fromUtf8("elff"))
        self.sca_fib2 = QtGui.QLabel(self.frame_2)
        self.sca_fib2.setGeometry(QtCore.QRect(220, 170, 51, 17))
        self.sca_fib2.setText(_fromUtf8(""))
        self.sca_fib2.setObjectName(_fromUtf8("sca_fib2"))
        self.feb4 = QtGui.QLabel(self.frame_2)
        self.feb4.setGeometry(QtCore.QRect(50, 230, 151, 20))
        self.feb4.setText(_fromUtf8(""))
        self.feb4.setObjectName(_fromUtf8("feb4"))
        self.elbf = QtGui.QLabel(self.frame_2)
        self.elbf.setGeometry(QtCore.QRect(10, 170, 21, 17))
        self.elbf.setObjectName(_fromUtf8("elbf"))
        self.sca_fib0 = QtGui.QLabel(self.frame_2)
        self.sca_fib0.setGeometry(QtCore.QRect(220, 110, 51, 17))
        self.sca_fib0.setText(_fromUtf8(""))
        self.sca_fib0.setObjectName(_fromUtf8("sca_fib0"))
        self.sca_fib6 = QtGui.QLabel(self.frame_2)
        self.sca_fib6.setGeometry(QtCore.QRect(220, 290, 51, 17))
        self.sca_fib6.setText(_fromUtf8(""))
        self.sca_fib6.setObjectName(_fromUtf8("sca_fib6"))
        self.serial4 = QtGui.QLabel(self.frame_2)
        self.serial4.setGeometry(QtCore.QRect(290, 230, 67, 17))
        self.serial4.setText(_fromUtf8(""))
        self.serial4.setObjectName(_fromUtf8("serial4"))
        self.estat6 = QtGui.QProgressBar(self.frame_2)
        self.estat6.setGeometry(QtCore.QRect(380, 290, 21, 23))
        self.estat6.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat6.setMaximum(100)
        self.estat6.setProperty("value", 0)
        self.estat6.setTextVisible(False)
        self.estat6.setOrientation(QtCore.Qt.Vertical)
        self.estat6.setInvertedAppearance(False)
        self.estat6.setObjectName(_fromUtf8("estat6"))
        self.serial1 = QtGui.QLabel(self.frame_2)
        self.serial1.setGeometry(QtCore.QRect(290, 140, 67, 17))
        self.serial1.setText(_fromUtf8(""))
        self.serial1.setObjectName(_fromUtf8("serial1"))
        self.estat4 = QtGui.QProgressBar(self.frame_2)
        self.estat4.setGeometry(QtCore.QRect(380, 230, 21, 23))
        self.estat4.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat4.setMaximum(100)
        self.estat4.setProperty("value", 0)
        self.estat4.setTextVisible(False)
        self.estat4.setOrientation(QtCore.Qt.Vertical)
        self.estat4.setInvertedAppearance(False)
        self.estat4.setObjectName(_fromUtf8("estat4"))
        self.feb5 = QtGui.QLabel(self.frame_2)
        self.feb5.setGeometry(QtCore.QRect(50, 260, 151, 20))
        self.feb5.setText(_fromUtf8(""))
        self.feb5.setObjectName(_fromUtf8("feb5"))
        self.serial6 = QtGui.QLabel(self.frame_2)
        self.serial6.setGeometry(QtCore.QRect(290, 290, 67, 17))
        self.serial6.setText(_fromUtf8(""))
        self.serial6.setObjectName(_fromUtf8("serial6"))
        self.serial2 = QtGui.QLabel(self.frame_2)
        self.serial2.setGeometry(QtCore.QRect(290, 170, 67, 17))
        self.serial2.setText(_fromUtf8(""))
        self.serial2.setObjectName(_fromUtf8("serial2"))
        self.estat3 = QtGui.QProgressBar(self.frame_2)
        self.estat3.setGeometry(QtCore.QRect(380, 200, 21, 23))
        self.estat3.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat3.setMaximum(100)
        self.estat3.setProperty("value", 0)
        self.estat3.setTextVisible(False)
        self.estat3.setOrientation(QtCore.Qt.Vertical)
        self.estat3.setInvertedAppearance(False)
        self.estat3.setObjectName(_fromUtf8("estat3"))
        self.el7f = QtGui.QLabel(self.frame_2)
        self.el7f.setGeometry(QtCore.QRect(10, 140, 21, 17))
        self.el7f.setObjectName(_fromUtf8("el7f"))
        self.pingbtn = QtGui.QPushButton(self.frame_2)
        self.pingbtn.setGeometry(QtCore.QRect(10, 40, 91, 21))
        self.pingbtn.setObjectName(_fromUtf8("pingbtn"))
        self.scalabel = QtGui.QLabel(self.frame_2)
        self.scalabel.setGeometry(QtCore.QRect(50, 0, 321, 41))
        self.scalabel.setObjectName(_fromUtf8("scalabel"))
        self.feb6 = QtGui.QLabel(self.frame_2)
        self.feb6.setGeometry(QtCore.QRect(50, 290, 151, 20))
        self.feb6.setText(_fromUtf8(""))
        self.feb6.setObjectName(_fromUtf8("feb6"))
        self.estat1 = QtGui.QProgressBar(self.frame_2)
        self.estat1.setGeometry(QtCore.QRect(380, 140, 21, 23))
        self.estat1.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat1.setMaximum(100)
        self.estat1.setProperty("value", 0)
        self.estat1.setTextVisible(False)
        self.estat1.setOrientation(QtCore.Qt.Vertical)
        self.estat1.setInvertedAppearance(False)
        self.estat1.setObjectName(_fromUtf8("estat1"))
        self.el17f = QtGui.QLabel(self.frame_2)
        self.el17f.setGeometry(QtCore.QRect(10, 260, 21, 17))
        self.el17f.setObjectName(_fromUtf8("el17f"))
        self.sca_fib1 = QtGui.QLabel(self.frame_2)
        self.sca_fib1.setGeometry(QtCore.QRect(220, 140, 51, 17))
        self.sca_fib1.setText(_fromUtf8(""))
        self.sca_fib1.setObjectName(_fromUtf8("sca_fib1"))
        self.line_12 = QtGui.QFrame(self.frame_2)
        self.line_12.setGeometry(QtCore.QRect(200, 110, 20, 711))
        self.line_12.setFrameShape(QtGui.QFrame.VLine)
        self.line_12.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_12.setObjectName(_fromUtf8("line_12"))
        self.elink_label = QtGui.QLabel(self.frame_2)
        self.elink_label.setGeometry(QtCore.QRect(0, 80, 41, 21))
        self.elink_label.setObjectName(_fromUtf8("elink_label"))
        self.estat0 = QtGui.QProgressBar(self.frame_2)
        self.estat0.setGeometry(QtCore.QRect(380, 110, 21, 23))
        self.estat0.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat0.setMaximum(100)
        self.estat0.setProperty("value", 0)
        self.estat0.setTextVisible(False)
        self.estat0.setOrientation(QtCore.Qt.Vertical)
        self.estat0.setInvertedAppearance(False)
        self.estat0.setObjectName(_fromUtf8("estat0"))
        self.sca_fib4 = QtGui.QLabel(self.frame_2)
        self.sca_fib4.setGeometry(QtCore.QRect(220, 230, 51, 17))
        self.sca_fib4.setText(_fromUtf8(""))
        self.sca_fib4.setObjectName(_fromUtf8("sca_fib4"))
        self.estat7 = QtGui.QProgressBar(self.frame_2)
        self.estat7.setGeometry(QtCore.QRect(380, 320, 21, 23))
        self.estat7.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat7.setMaximum(100)
        self.estat7.setProperty("value", 0)
        self.estat7.setTextVisible(False)
        self.estat7.setOrientation(QtCore.Qt.Vertical)
        self.estat7.setInvertedAppearance(False)
        self.estat7.setObjectName(_fromUtf8("estat7"))
        self.line_2 = QtGui.QFrame(self.frame_2)
        self.line_2.setGeometry(QtCore.QRect(350, 110, 20, 711))
        self.line_2.setFrameShape(QtGui.QFrame.VLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.el1ff = QtGui.QLabel(self.frame_2)
        self.el1ff.setGeometry(QtCore.QRect(10, 320, 21, 16))
        self.el1ff.setObjectName(_fromUtf8("el1ff"))
        self.feb0 = QtGui.QLabel(self.frame_2)
        self.feb0.setGeometry(QtCore.QRect(50, 110, 151, 20))
        self.feb0.setText(_fromUtf8(""))
        self.feb0.setObjectName(_fromUtf8("feb0"))
        self.sernbr_label = QtGui.QLabel(self.frame_2)
        self.sernbr_label.setGeometry(QtCore.QRect(300, 80, 51, 21))
        self.sernbr_label.setObjectName(_fromUtf8("sernbr_label"))
        self.estat5 = QtGui.QProgressBar(self.frame_2)
        self.estat5.setGeometry(QtCore.QRect(380, 260, 21, 23))
        self.estat5.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat5.setMaximum(100)
        self.estat5.setProperty("value", 0)
        self.estat5.setTextVisible(False)
        self.estat5.setOrientation(QtCore.Qt.Vertical)
        self.estat5.setInvertedAppearance(False)
        self.estat5.setObjectName(_fromUtf8("estat5"))
        self.line_13 = QtGui.QFrame(self.frame_2)
        self.line_13.setGeometry(QtCore.QRect(270, 110, 20, 711))
        self.line_13.setFrameShape(QtGui.QFrame.VLine)
        self.line_13.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_13.setObjectName(_fromUtf8("line_13"))
        self.febadd_label_3 = QtGui.QLabel(self.frame_2)
        self.febadd_label_3.setGeometry(QtCore.QRect(370, 80, 51, 21))
        self.febadd_label_3.setObjectName(_fromUtf8("febadd_label_3"))
        self.serial0 = QtGui.QLabel(self.frame_2)
        self.serial0.setGeometry(QtCore.QRect(290, 110, 71, 17))
        self.serial0.setText(_fromUtf8(""))
        self.serial0.setObjectName(_fromUtf8("serial0"))
        self.sca_fib7 = QtGui.QLabel(self.frame_2)
        self.sca_fib7.setGeometry(QtCore.QRect(220, 320, 51, 17))
        self.sca_fib7.setText(_fromUtf8(""))
        self.sca_fib7.setObjectName(_fromUtf8("sca_fib7"))
        self.estat2 = QtGui.QProgressBar(self.frame_2)
        self.estat2.setGeometry(QtCore.QRect(380, 170, 21, 23))
        self.estat2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat2.setMaximum(100)
        self.estat2.setProperty("value", 0)
        self.estat2.setTextVisible(False)
        self.estat2.setOrientation(QtCore.Qt.Vertical)
        self.estat2.setInvertedAppearance(False)
        self.estat2.setObjectName(_fromUtf8("estat2"))
        self.pingprogress = QtGui.QProgressBar(self.frame_2)
        self.pingprogress.setGeometry(QtCore.QRect(110, 40, 291, 21))
        self.pingprogress.setProperty("value", 0)
        self.pingprogress.setObjectName(_fromUtf8("pingprogress"))
        self.el1bf = QtGui.QLabel(self.frame_2)
        self.el1bf.setGeometry(QtCore.QRect(10, 290, 31, 17))
        self.el1bf.setObjectName(_fromUtf8("el1bf"))
        self.line = QtGui.QFrame(self.frame_2)
        self.line.setGeometry(QtCore.QRect(30, 110, 20, 711))
        self.line.setFrameShape(QtGui.QFrame.VLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.febadd_label_2 = QtGui.QLabel(self.frame_2)
        self.febadd_label_2.setGeometry(QtCore.QRect(220, 80, 51, 21))
        self.febadd_label_2.setObjectName(_fromUtf8("febadd_label_2"))
        self.sca_fib5 = QtGui.QLabel(self.frame_2)
        self.sca_fib5.setGeometry(QtCore.QRect(220, 260, 51, 17))
        self.sca_fib5.setText(_fromUtf8(""))
        self.sca_fib5.setObjectName(_fromUtf8("sca_fib5"))
        self.feb2 = QtGui.QLabel(self.frame_2)
        self.feb2.setGeometry(QtCore.QRect(50, 170, 151, 20))
        self.feb2.setText(_fromUtf8(""))
        self.feb2.setObjectName(_fromUtf8("feb2"))
        self.serial5 = QtGui.QLabel(self.frame_2)
        self.serial5.setGeometry(QtCore.QRect(290, 260, 67, 17))
        self.serial5.setText(_fromUtf8(""))
        self.serial5.setObjectName(_fromUtf8("serial5"))
        self.serial7 = QtGui.QLabel(self.frame_2)
        self.serial7.setGeometry(QtCore.QRect(290, 320, 67, 17))
        self.serial7.setText(_fromUtf8(""))
        self.serial7.setObjectName(_fromUtf8("serial7"))
        self.serial3 = QtGui.QLabel(self.frame_2)
        self.serial3.setGeometry(QtCore.QRect(290, 200, 67, 17))
        self.serial3.setText(_fromUtf8(""))
        self.serial3.setObjectName(_fromUtf8("serial3"))
        self.el3f = QtGui.QLabel(self.frame_2)
        self.el3f.setGeometry(QtCore.QRect(10, 110, 21, 17))
        self.el3f.setObjectName(_fromUtf8("el3f"))
        self.el13f = QtGui.QLabel(self.frame_2)
        self.el13f.setGeometry(QtCore.QRect(10, 230, 21, 17))
        self.el13f.setObjectName(_fromUtf8("el13f"))
        self.sca_fib3 = QtGui.QLabel(self.frame_2)
        self.sca_fib3.setGeometry(QtCore.QRect(220, 200, 51, 17))
        self.sca_fib3.setText(_fromUtf8(""))
        self.sca_fib3.setObjectName(_fromUtf8("sca_fib3"))
        self.estat8 = QtGui.QProgressBar(self.frame_2)
        self.estat8.setGeometry(QtCore.QRect(380, 350, 21, 23))
        self.estat8.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat8.setMaximum(100)
        self.estat8.setProperty("value", 0)
        self.estat8.setTextVisible(False)
        self.estat8.setOrientation(QtCore.Qt.Vertical)
        self.estat8.setInvertedAppearance(False)
        self.estat8.setObjectName(_fromUtf8("estat8"))
        self.estat9 = QtGui.QProgressBar(self.frame_2)
        self.estat9.setGeometry(QtCore.QRect(380, 380, 21, 23))
        self.estat9.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat9.setMaximum(100)
        self.estat9.setProperty("value", 0)
        self.estat9.setTextVisible(False)
        self.estat9.setOrientation(QtCore.Qt.Vertical)
        self.estat9.setInvertedAppearance(False)
        self.estat9.setObjectName(_fromUtf8("estat9"))
        self.estat10 = QtGui.QProgressBar(self.frame_2)
        self.estat10.setGeometry(QtCore.QRect(380, 410, 21, 23))
        self.estat10.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat10.setMaximum(100)
        self.estat10.setProperty("value", 0)
        self.estat10.setTextVisible(False)
        self.estat10.setOrientation(QtCore.Qt.Vertical)
        self.estat10.setInvertedAppearance(False)
        self.estat10.setObjectName(_fromUtf8("estat10"))
        self.estat11 = QtGui.QProgressBar(self.frame_2)
        self.estat11.setGeometry(QtCore.QRect(380, 440, 21, 23))
        self.estat11.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat11.setMaximum(100)
        self.estat11.setProperty("value", 0)
        self.estat11.setTextVisible(False)
        self.estat11.setOrientation(QtCore.Qt.Vertical)
        self.estat11.setInvertedAppearance(False)
        self.estat11.setObjectName(_fromUtf8("estat11"))
        self.estat12 = QtGui.QProgressBar(self.frame_2)
        self.estat12.setGeometry(QtCore.QRect(380, 470, 21, 23))
        self.estat12.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat12.setMaximum(100)
        self.estat12.setProperty("value", 0)
        self.estat12.setTextVisible(False)
        self.estat12.setOrientation(QtCore.Qt.Vertical)
        self.estat12.setInvertedAppearance(False)
        self.estat12.setObjectName(_fromUtf8("estat12"))
        self.el1ff_3 = QtGui.QLabel(self.frame_2)
        self.el1ff_3.setGeometry(QtCore.QRect(10, 350, 21, 16))
        self.el1ff_3.setObjectName(_fromUtf8("el1ff_3"))
        self.el1ff_4 = QtGui.QLabel(self.frame_2)
        self.el1ff_4.setGeometry(QtCore.QRect(10, 380, 21, 16))
        self.el1ff_4.setObjectName(_fromUtf8("el1ff_4"))
        self.el1ff_5 = QtGui.QLabel(self.frame_2)
        self.el1ff_5.setGeometry(QtCore.QRect(10, 410, 21, 16))
        self.el1ff_5.setObjectName(_fromUtf8("el1ff_5"))
        self.el1ff_6 = QtGui.QLabel(self.frame_2)
        self.el1ff_6.setGeometry(QtCore.QRect(10, 440, 21, 16))
        self.el1ff_6.setObjectName(_fromUtf8("el1ff_6"))
        self.el1ff_7 = QtGui.QLabel(self.frame_2)
        self.el1ff_7.setGeometry(QtCore.QRect(10, 470, 31, 16))
        self.el1ff_7.setObjectName(_fromUtf8("el1ff_7"))
        self.el1ff_8 = QtGui.QLabel(self.frame_2)
        self.el1ff_8.setGeometry(QtCore.QRect(10, 500, 31, 16))
        self.el1ff_8.setObjectName(_fromUtf8("el1ff_8"))
        self.el1ff_9 = QtGui.QLabel(self.frame_2)
        self.el1ff_9.setGeometry(QtCore.QRect(10, 530, 31, 16))
        self.el1ff_9.setObjectName(_fromUtf8("el1ff_9"))
        self.el1ff_10 = QtGui.QLabel(self.frame_2)
        self.el1ff_10.setGeometry(QtCore.QRect(10, 560, 31, 16))
        self.el1ff_10.setObjectName(_fromUtf8("el1ff_10"))
        self.el1ff_11 = QtGui.QLabel(self.frame_2)
        self.el1ff_11.setGeometry(QtCore.QRect(10, 590, 31, 16))
        self.el1ff_11.setObjectName(_fromUtf8("el1ff_11"))
        self.el1ff_12 = QtGui.QLabel(self.frame_2)
        self.el1ff_12.setGeometry(QtCore.QRect(10, 620, 31, 16))
        self.el1ff_12.setObjectName(_fromUtf8("el1ff_12"))
        self.el1ff_13 = QtGui.QLabel(self.frame_2)
        self.el1ff_13.setGeometry(QtCore.QRect(10, 650, 31, 16))
        self.el1ff_13.setObjectName(_fromUtf8("el1ff_13"))
        self.el1ff_14 = QtGui.QLabel(self.frame_2)
        self.el1ff_14.setGeometry(QtCore.QRect(10, 680, 31, 16))
        self.el1ff_14.setObjectName(_fromUtf8("el1ff_14"))
        self.estat15 = QtGui.QProgressBar(self.frame_2)
        self.estat15.setGeometry(QtCore.QRect(380, 560, 21, 23))
        self.estat15.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat15.setMaximum(100)
        self.estat15.setProperty("value", 0)
        self.estat15.setTextVisible(False)
        self.estat15.setOrientation(QtCore.Qt.Vertical)
        self.estat15.setInvertedAppearance(False)
        self.estat15.setObjectName(_fromUtf8("estat15"))
        self.estat16 = QtGui.QProgressBar(self.frame_2)
        self.estat16.setGeometry(QtCore.QRect(380, 590, 21, 23))
        self.estat16.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat16.setMaximum(100)
        self.estat16.setProperty("value", 0)
        self.estat16.setTextVisible(False)
        self.estat16.setOrientation(QtCore.Qt.Vertical)
        self.estat16.setInvertedAppearance(False)
        self.estat16.setObjectName(_fromUtf8("estat16"))
        self.estat14 = QtGui.QProgressBar(self.frame_2)
        self.estat14.setGeometry(QtCore.QRect(380, 530, 21, 23))
        self.estat14.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat14.setMaximum(100)
        self.estat14.setProperty("value", 0)
        self.estat14.setTextVisible(False)
        self.estat14.setOrientation(QtCore.Qt.Vertical)
        self.estat14.setInvertedAppearance(False)
        self.estat14.setObjectName(_fromUtf8("estat14"))
        self.estat13 = QtGui.QProgressBar(self.frame_2)
        self.estat13.setGeometry(QtCore.QRect(380, 500, 21, 23))
        self.estat13.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat13.setMaximum(100)
        self.estat13.setProperty("value", 0)
        self.estat13.setTextVisible(False)
        self.estat13.setOrientation(QtCore.Qt.Vertical)
        self.estat13.setInvertedAppearance(False)
        self.estat13.setObjectName(_fromUtf8("estat13"))
        self.estat18 = QtGui.QProgressBar(self.frame_2)
        self.estat18.setGeometry(QtCore.QRect(380, 650, 21, 23))
        self.estat18.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat18.setMaximum(100)
        self.estat18.setProperty("value", 0)
        self.estat18.setTextVisible(False)
        self.estat18.setOrientation(QtCore.Qt.Vertical)
        self.estat18.setInvertedAppearance(False)
        self.estat18.setObjectName(_fromUtf8("estat18"))
        self.estat17 = QtGui.QProgressBar(self.frame_2)
        self.estat17.setGeometry(QtCore.QRect(380, 620, 21, 23))
        self.estat17.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat17.setMaximum(100)
        self.estat17.setProperty("value", 0)
        self.estat17.setTextVisible(False)
        self.estat17.setOrientation(QtCore.Qt.Vertical)
        self.estat17.setInvertedAppearance(False)
        self.estat17.setObjectName(_fromUtf8("estat17"))
        self.estat19 = QtGui.QProgressBar(self.frame_2)
        self.estat19.setGeometry(QtCore.QRect(380, 680, 21, 23))
        self.estat19.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat19.setMaximum(100)
        self.estat19.setProperty("value", 0)
        self.estat19.setTextVisible(False)
        self.estat19.setOrientation(QtCore.Qt.Vertical)
        self.estat19.setInvertedAppearance(False)
        self.estat19.setObjectName(_fromUtf8("estat19"))
        self.el1ff_15 = QtGui.QLabel(self.frame_2)
        self.el1ff_15.setGeometry(QtCore.QRect(10, 710, 31, 16))
        self.el1ff_15.setObjectName(_fromUtf8("el1ff_15"))
        self.el1ff_16 = QtGui.QLabel(self.frame_2)
        self.el1ff_16.setGeometry(QtCore.QRect(10, 740, 31, 16))
        self.el1ff_16.setObjectName(_fromUtf8("el1ff_16"))
        self.el1ff_17 = QtGui.QLabel(self.frame_2)
        self.el1ff_17.setGeometry(QtCore.QRect(10, 770, 31, 16))
        self.el1ff_17.setObjectName(_fromUtf8("el1ff_17"))
        self.el1ff_18 = QtGui.QLabel(self.frame_2)
        self.el1ff_18.setGeometry(QtCore.QRect(10, 800, 31, 16))
        self.el1ff_18.setObjectName(_fromUtf8("el1ff_18"))
        self.estat20 = QtGui.QProgressBar(self.frame_2)
        self.estat20.setGeometry(QtCore.QRect(380, 710, 21, 23))
        self.estat20.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat20.setMaximum(100)
        self.estat20.setProperty("value", 0)
        self.estat20.setTextVisible(False)
        self.estat20.setOrientation(QtCore.Qt.Vertical)
        self.estat20.setInvertedAppearance(False)
        self.estat20.setObjectName(_fromUtf8("estat20"))
        self.estat21 = QtGui.QProgressBar(self.frame_2)
        self.estat21.setGeometry(QtCore.QRect(380, 740, 21, 23))
        self.estat21.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat21.setMaximum(100)
        self.estat21.setProperty("value", 0)
        self.estat21.setTextVisible(False)
        self.estat21.setOrientation(QtCore.Qt.Vertical)
        self.estat21.setInvertedAppearance(False)
        self.estat21.setObjectName(_fromUtf8("estat21"))
        self.estat22 = QtGui.QProgressBar(self.frame_2)
        self.estat22.setGeometry(QtCore.QRect(380, 770, 21, 23))
        self.estat22.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat22.setMaximum(100)
        self.estat22.setProperty("value", 0)
        self.estat22.setTextVisible(False)
        self.estat22.setOrientation(QtCore.Qt.Vertical)
        self.estat22.setInvertedAppearance(False)
        self.estat22.setObjectName(_fromUtf8("estat22"))
        self.estat23 = QtGui.QProgressBar(self.frame_2)
        self.estat23.setGeometry(QtCore.QRect(380, 800, 21, 23))
        self.estat23.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat23.setMaximum(100)
        self.estat23.setProperty("value", 0)
        self.estat23.setTextVisible(False)
        self.estat23.setOrientation(QtCore.Qt.Vertical)
        self.estat23.setInvertedAppearance(False)
        self.estat23.setObjectName(_fromUtf8("estat23"))
        self.feb14 = QtGui.QLabel(self.frame_2)
        self.feb14.setGeometry(QtCore.QRect(50, 530, 151, 20))
        self.feb14.setText(_fromUtf8(""))
        self.feb14.setObjectName(_fromUtf8("feb14"))
        self.feb12 = QtGui.QLabel(self.frame_2)
        self.feb12.setGeometry(QtCore.QRect(50, 470, 151, 20))
        self.feb12.setText(_fromUtf8(""))
        self.feb12.setObjectName(_fromUtf8("feb12"))
        self.feb10 = QtGui.QLabel(self.frame_2)
        self.feb10.setGeometry(QtCore.QRect(50, 410, 151, 20))
        self.feb10.setText(_fromUtf8(""))
        self.feb10.setObjectName(_fromUtf8("feb10"))
        self.feb15 = QtGui.QLabel(self.frame_2)
        self.feb15.setGeometry(QtCore.QRect(50, 560, 151, 20))
        self.feb15.setText(_fromUtf8(""))
        self.feb15.setObjectName(_fromUtf8("feb15"))
        self.feb11 = QtGui.QLabel(self.frame_2)
        self.feb11.setGeometry(QtCore.QRect(50, 440, 151, 20))
        self.feb11.setText(_fromUtf8(""))
        self.feb11.setObjectName(_fromUtf8("feb11"))
        self.feb9 = QtGui.QLabel(self.frame_2)
        self.feb9.setGeometry(QtCore.QRect(50, 380, 151, 20))
        self.feb9.setText(_fromUtf8(""))
        self.feb9.setObjectName(_fromUtf8("feb9"))
        self.feb8 = QtGui.QLabel(self.frame_2)
        self.feb8.setGeometry(QtCore.QRect(50, 350, 151, 20))
        self.feb8.setText(_fromUtf8(""))
        self.feb8.setObjectName(_fromUtf8("feb8"))
        self.feb13 = QtGui.QLabel(self.frame_2)
        self.feb13.setGeometry(QtCore.QRect(50, 500, 151, 20))
        self.feb13.setText(_fromUtf8(""))
        self.feb13.setObjectName(_fromUtf8("feb13"))
        self.feb21 = QtGui.QLabel(self.frame_2)
        self.feb21.setGeometry(QtCore.QRect(50, 740, 151, 20))
        self.feb21.setText(_fromUtf8(""))
        self.feb21.setObjectName(_fromUtf8("feb21"))
        self.feb18 = QtGui.QLabel(self.frame_2)
        self.feb18.setGeometry(QtCore.QRect(50, 650, 151, 20))
        self.feb18.setText(_fromUtf8(""))
        self.feb18.setObjectName(_fromUtf8("feb18"))
        self.feb16 = QtGui.QLabel(self.frame_2)
        self.feb16.setGeometry(QtCore.QRect(50, 590, 151, 20))
        self.feb16.setText(_fromUtf8(""))
        self.feb16.setObjectName(_fromUtf8("feb16"))
        self.feb19 = QtGui.QLabel(self.frame_2)
        self.feb19.setGeometry(QtCore.QRect(50, 680, 151, 20))
        self.feb19.setText(_fromUtf8(""))
        self.feb19.setObjectName(_fromUtf8("feb19"))
        self.feb22 = QtGui.QLabel(self.frame_2)
        self.feb22.setGeometry(QtCore.QRect(50, 770, 151, 20))
        self.feb22.setText(_fromUtf8(""))
        self.feb22.setObjectName(_fromUtf8("feb22"))
        self.feb20 = QtGui.QLabel(self.frame_2)
        self.feb20.setGeometry(QtCore.QRect(50, 710, 151, 20))
        self.feb20.setText(_fromUtf8(""))
        self.feb20.setObjectName(_fromUtf8("feb20"))
        self.feb17 = QtGui.QLabel(self.frame_2)
        self.feb17.setGeometry(QtCore.QRect(50, 620, 151, 20))
        self.feb17.setText(_fromUtf8(""))
        self.feb17.setObjectName(_fromUtf8("feb17"))
        self.feb23 = QtGui.QLabel(self.frame_2)
        self.feb23.setGeometry(QtCore.QRect(50, 800, 151, 20))
        self.feb23.setText(_fromUtf8(""))
        self.feb23.setObjectName(_fromUtf8("feb23"))
        self.sca_fib9 = QtGui.QLabel(self.frame_2)
        self.sca_fib9.setGeometry(QtCore.QRect(220, 380, 51, 17))
        self.sca_fib9.setText(_fromUtf8(""))
        self.sca_fib9.setObjectName(_fromUtf8("sca_fib9"))
        self.sca_fib10 = QtGui.QLabel(self.frame_2)
        self.sca_fib10.setGeometry(QtCore.QRect(220, 410, 51, 17))
        self.sca_fib10.setText(_fromUtf8(""))
        self.sca_fib10.setObjectName(_fromUtf8("sca_fib10"))
        self.sca_fib11 = QtGui.QLabel(self.frame_2)
        self.sca_fib11.setGeometry(QtCore.QRect(220, 440, 51, 17))
        self.sca_fib11.setText(_fromUtf8(""))
        self.sca_fib11.setObjectName(_fromUtf8("sca_fib11"))
        self.sca_fib12 = QtGui.QLabel(self.frame_2)
        self.sca_fib12.setGeometry(QtCore.QRect(220, 470, 51, 17))
        self.sca_fib12.setText(_fromUtf8(""))
        self.sca_fib12.setObjectName(_fromUtf8("sca_fib12"))
        self.sca_fib14 = QtGui.QLabel(self.frame_2)
        self.sca_fib14.setGeometry(QtCore.QRect(220, 530, 51, 17))
        self.sca_fib14.setText(_fromUtf8(""))
        self.sca_fib14.setObjectName(_fromUtf8("sca_fib14"))
        self.sca_fib15 = QtGui.QLabel(self.frame_2)
        self.sca_fib15.setGeometry(QtCore.QRect(220, 560, 51, 17))
        self.sca_fib15.setText(_fromUtf8(""))
        self.sca_fib15.setObjectName(_fromUtf8("sca_fib15"))
        self.sca_fib13 = QtGui.QLabel(self.frame_2)
        self.sca_fib13.setGeometry(QtCore.QRect(220, 500, 51, 17))
        self.sca_fib13.setText(_fromUtf8(""))
        self.sca_fib13.setObjectName(_fromUtf8("sca_fib13"))
        self.sca_fib8 = QtGui.QLabel(self.frame_2)
        self.sca_fib8.setGeometry(QtCore.QRect(220, 350, 51, 17))
        self.sca_fib8.setText(_fromUtf8(""))
        self.sca_fib8.setObjectName(_fromUtf8("sca_fib8"))
        self.sca_fib17 = QtGui.QLabel(self.frame_2)
        self.sca_fib17.setGeometry(QtCore.QRect(220, 620, 51, 17))
        self.sca_fib17.setText(_fromUtf8(""))
        self.sca_fib17.setObjectName(_fromUtf8("sca_fib17"))
        self.sca_fib22 = QtGui.QLabel(self.frame_2)
        self.sca_fib22.setGeometry(QtCore.QRect(220, 770, 51, 17))
        self.sca_fib22.setText(_fromUtf8(""))
        self.sca_fib22.setObjectName(_fromUtf8("sca_fib22"))
        self.sca_fib16 = QtGui.QLabel(self.frame_2)
        self.sca_fib16.setGeometry(QtCore.QRect(220, 590, 51, 17))
        self.sca_fib16.setText(_fromUtf8(""))
        self.sca_fib16.setObjectName(_fromUtf8("sca_fib16"))
        self.sca_fib18 = QtGui.QLabel(self.frame_2)
        self.sca_fib18.setGeometry(QtCore.QRect(220, 650, 51, 17))
        self.sca_fib18.setText(_fromUtf8(""))
        self.sca_fib18.setObjectName(_fromUtf8("sca_fib18"))
        self.sca_fib21 = QtGui.QLabel(self.frame_2)
        self.sca_fib21.setGeometry(QtCore.QRect(220, 740, 51, 17))
        self.sca_fib21.setText(_fromUtf8(""))
        self.sca_fib21.setObjectName(_fromUtf8("sca_fib21"))
        self.sca_fib19 = QtGui.QLabel(self.frame_2)
        self.sca_fib19.setGeometry(QtCore.QRect(220, 680, 51, 17))
        self.sca_fib19.setText(_fromUtf8(""))
        self.sca_fib19.setObjectName(_fromUtf8("sca_fib19"))
        self.sca_fib23 = QtGui.QLabel(self.frame_2)
        self.sca_fib23.setGeometry(QtCore.QRect(220, 800, 51, 17))
        self.sca_fib23.setText(_fromUtf8(""))
        self.sca_fib23.setObjectName(_fromUtf8("sca_fib23"))
        self.sca_fib20 = QtGui.QLabel(self.frame_2)
        self.sca_fib20.setGeometry(QtCore.QRect(220, 710, 51, 17))
        self.sca_fib20.setText(_fromUtf8(""))
        self.sca_fib20.setObjectName(_fromUtf8("sca_fib20"))
        self.serial12 = QtGui.QLabel(self.frame_2)
        self.serial12.setGeometry(QtCore.QRect(290, 470, 67, 17))
        self.serial12.setText(_fromUtf8(""))
        self.serial12.setObjectName(_fromUtf8("serial12"))
        self.serial15 = QtGui.QLabel(self.frame_2)
        self.serial15.setGeometry(QtCore.QRect(290, 560, 67, 17))
        self.serial15.setText(_fromUtf8(""))
        self.serial15.setObjectName(_fromUtf8("serial15"))
        self.serial9 = QtGui.QLabel(self.frame_2)
        self.serial9.setGeometry(QtCore.QRect(290, 380, 67, 17))
        self.serial9.setText(_fromUtf8(""))
        self.serial9.setObjectName(_fromUtf8("serial9"))
        self.serial14 = QtGui.QLabel(self.frame_2)
        self.serial14.setGeometry(QtCore.QRect(290, 530, 67, 17))
        self.serial14.setText(_fromUtf8(""))
        self.serial14.setObjectName(_fromUtf8("serial14"))
        self.serial10 = QtGui.QLabel(self.frame_2)
        self.serial10.setGeometry(QtCore.QRect(290, 410, 67, 17))
        self.serial10.setText(_fromUtf8(""))
        self.serial10.setObjectName(_fromUtf8("serial10"))
        self.serial11 = QtGui.QLabel(self.frame_2)
        self.serial11.setGeometry(QtCore.QRect(290, 440, 67, 17))
        self.serial11.setText(_fromUtf8(""))
        self.serial11.setObjectName(_fromUtf8("serial11"))
        self.serial8 = QtGui.QLabel(self.frame_2)
        self.serial8.setGeometry(QtCore.QRect(290, 350, 71, 17))
        self.serial8.setText(_fromUtf8(""))
        self.serial8.setObjectName(_fromUtf8("serial8"))
        self.serial13 = QtGui.QLabel(self.frame_2)
        self.serial13.setGeometry(QtCore.QRect(290, 500, 67, 17))
        self.serial13.setText(_fromUtf8(""))
        self.serial13.setObjectName(_fromUtf8("serial13"))
        self.serial20 = QtGui.QLabel(self.frame_2)
        self.serial20.setGeometry(QtCore.QRect(290, 710, 67, 17))
        self.serial20.setText(_fromUtf8(""))
        self.serial20.setObjectName(_fromUtf8("serial20"))
        self.serial22 = QtGui.QLabel(self.frame_2)
        self.serial22.setGeometry(QtCore.QRect(290, 770, 67, 17))
        self.serial22.setText(_fromUtf8(""))
        self.serial22.setObjectName(_fromUtf8("serial22"))
        self.serial23 = QtGui.QLabel(self.frame_2)
        self.serial23.setGeometry(QtCore.QRect(290, 800, 67, 17))
        self.serial23.setText(_fromUtf8(""))
        self.serial23.setObjectName(_fromUtf8("serial23"))
        self.serial18 = QtGui.QLabel(self.frame_2)
        self.serial18.setGeometry(QtCore.QRect(290, 650, 67, 17))
        self.serial18.setText(_fromUtf8(""))
        self.serial18.setObjectName(_fromUtf8("serial18"))
        self.serial19 = QtGui.QLabel(self.frame_2)
        self.serial19.setGeometry(QtCore.QRect(290, 680, 67, 17))
        self.serial19.setText(_fromUtf8(""))
        self.serial19.setObjectName(_fromUtf8("serial19"))
        self.serial17 = QtGui.QLabel(self.frame_2)
        self.serial17.setGeometry(QtCore.QRect(290, 620, 67, 17))
        self.serial17.setText(_fromUtf8(""))
        self.serial17.setObjectName(_fromUtf8("serial17"))
        self.serial21 = QtGui.QLabel(self.frame_2)
        self.serial21.setGeometry(QtCore.QRect(290, 740, 67, 17))
        self.serial21.setText(_fromUtf8(""))
        self.serial21.setObjectName(_fromUtf8("serial21"))
        self.serial16 = QtGui.QLabel(self.frame_2)
        self.serial16.setGeometry(QtCore.QRect(290, 590, 71, 17))
        self.serial16.setText(_fromUtf8(""))
        self.serial16.setObjectName(_fromUtf8("serial16"))
        self.verticalLayout_3.addWidget(self.frame_2)
        self.vtrx0 = QtGui.QLabel(self.centralwidget)
        self.vtrx0.setGeometry(QtCore.QRect(340, 920, 67, 17))
        self.vtrx0.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx0.setObjectName(_fromUtf8("vtrx0"))
        self.vtrx1 = QtGui.QLabel(self.centralwidget)
        self.vtrx1.setGeometry(QtCore.QRect(430, 920, 67, 17))
        self.vtrx1.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx1.setObjectName(_fromUtf8("vtrx1"))
        self.vtrx2 = QtGui.QLabel(self.centralwidget)
        self.vtrx2.setGeometry(QtCore.QRect(520, 920, 67, 17))
        self.vtrx2.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx2.setObjectName(_fromUtf8("vtrx2"))
        self.vtrx3 = QtGui.QLabel(self.centralwidget)
        self.vtrx3.setGeometry(QtCore.QRect(620, 920, 67, 17))
        self.vtrx3.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx3.setObjectName(_fromUtf8("vtrx3"))
        self.vtrx4 = QtGui.QLabel(self.centralwidget)
        self.vtrx4.setGeometry(QtCore.QRect(720, 930, 67, 17))
        self.vtrx4.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx4.setObjectName(_fromUtf8("vtrx4"))
        self.vtrx5 = QtGui.QLabel(self.centralwidget)
        self.vtrx5.setGeometry(QtCore.QRect(850, 940, 67, 17))
        self.vtrx5.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx5.setObjectName(_fromUtf8("vtrx5"))
        self.vtrx6 = QtGui.QLabel(self.centralwidget)
        self.vtrx6.setGeometry(QtCore.QRect(1010, 920, 67, 17))
        self.vtrx6.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx6.setObjectName(_fromUtf8("vtrx6"))
        self.vtrx7 = QtGui.QLabel(self.centralwidget)
        self.vtrx7.setGeometry(QtCore.QRect(1120, 920, 67, 17))
        self.vtrx7.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx7.setObjectName(_fromUtf8("vtrx7"))
        self.vtrx8 = QtGui.QLabel(self.centralwidget)
        self.vtrx8.setGeometry(QtCore.QRect(1300, 920, 67, 17))
        self.vtrx8.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx8.setObjectName(_fromUtf8("vtrx8"))
        self.vtrx9 = QtGui.QLabel(self.centralwidget)
        self.vtrx9.setGeometry(QtCore.QRect(1480, 930, 67, 17))
        self.vtrx9.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx9.setObjectName(_fromUtf8("vtrx9"))
        self.vtrx10 = QtGui.QLabel(self.centralwidget)
        self.vtrx10.setGeometry(QtCore.QRect(1560, 920, 67, 17))
        self.vtrx10.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx10.setObjectName(_fromUtf8("vtrx10"))
        self.vtrx11 = QtGui.QLabel(self.centralwidget)
        self.vtrx11.setGeometry(QtCore.QRect(1660, 930, 67, 17))
        self.vtrx11.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx11.setObjectName(_fromUtf8("vtrx11"))
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.actionhello = QtGui.QAction(MainWindow)
        self.actionhello.setObjectName(_fromUtf8("actionhello"))

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
        self.febadd_label.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">FEB</span></p></body></html>", None))
        self.elff.setText(_translate("MainWindow", "40", None))
        self.elbf.setText(_translate("MainWindow", "2", None))
        self.el7f.setText(_translate("MainWindow", "1", None))
        self.pingbtn.setToolTip(_translate("MainWindow", "<html><head/><body><p>Press this button to initiate pinging the SCAs.</p></body></html>", None))
        self.pingbtn.setText(_translate("MainWindow", "Ping SCAs", None))
        self.scalabel.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-weight:600;\">FEB SCA Connections</span></p></body></html>", None))
        self.el17f.setText(_translate("MainWindow", "42", None))
        self.elink_label.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-weight:600;\">Elink</span></p></body></html>", None))
        self.el1ff.setText(_translate("MainWindow", "81", None))
        self.sernbr_label.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Serial</span></p></body></html>", None))
        self.febadd_label_3.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Status</span></p></body></html>", None))
        self.el1bf.setText(_translate("MainWindow", "80", None))
        self.febadd_label_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Fibers</span></p></body></html>", None))
        self.el3f.setText(_translate("MainWindow", "0", None))
        self.el13f.setText(_translate("MainWindow", "41", None))
        self.el1ff_3.setText(_translate("MainWindow", "82", None))
        self.el1ff_4.setText(_translate("MainWindow", "C0", None))
        self.el1ff_5.setText(_translate("MainWindow", "C1", None))
        self.el1ff_6.setText(_translate("MainWindow", "C2", None))
        self.el1ff_7.setText(_translate("MainWindow", "100", None))
        self.el1ff_8.setText(_translate("MainWindow", "101", None))
        self.el1ff_9.setText(_translate("MainWindow", "102", None))
        self.el1ff_10.setText(_translate("MainWindow", "140", None))
        self.el1ff_11.setText(_translate("MainWindow", "141", None))
        self.el1ff_12.setText(_translate("MainWindow", "142", None))
        self.el1ff_13.setText(_translate("MainWindow", "180", None))
        self.el1ff_14.setText(_translate("MainWindow", "181", None))
        self.el1ff_15.setText(_translate("MainWindow", "182", None))
        self.el1ff_16.setText(_translate("MainWindow", "1C0", None))
        self.el1ff_17.setText(_translate("MainWindow", "1C1", None))
        self.el1ff_18.setText(_translate("MainWindow", "1C2", None))
        self.vtrx0.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx1.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx3.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx4.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx5.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx6.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx7.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx8.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx9.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx10.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx11.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.actionhello.setText(_translate("MainWindow", "hello", None))

