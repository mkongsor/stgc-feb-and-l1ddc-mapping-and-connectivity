# Mapping Software

This software is for reading the L1DDC mappings on L1DDC post being powered on.

## GUI Usage

To open the SCA location and VTRX connection GUI, launch gui_executable.py. You will be met by a seres of panels. 
Checking and generating mapping files should happen in the following order:
    1. Check that the wedge is fully powered, and that the corresponding FELIX core is running.
    2. Type in the wedge ID in the top right corner of the GUI under the "L1DDC Settings GUI" tab. 
To check the L1DDC SCA CONNECTIONS:
    1. Navigate to the L1DDC Settings GUI tab if not already there.
    2. Press the button "ping SCAs" on the top left panel. Check that all of the status lights turn green.
       If one or several lights do not turn green, the FELIX core might not be running, or it could be a genuine connection or SCA problem.
To check the L1DDC VTRX CONNECTIONS:
    1. Press "check VTRXs" in the lower left corner. Check that all status lights turn green. IF not, this indicates a problem with the VTRX in question.
To check the FEB SCA CONNECTIONS:
    1. Navigate to the "FEB SCA Pinging" tab.
    2. Press the button "ping SCAs" on the top left panel. Check that all of the status lights turn green.
       If one or several lights do not turn green, the FELIX core might not be running, or it could be a genuine connection or SCA problem.ess the button "ping SCAs" on the top left panel. Check that all of the status lights turn green.

To EXPORT:
    1. Make sure that all of the above checks have been carried out, and that all relevant status indicators are green.
    2. Navigate to the "L1DDC Settings GUI" tab.
    3. Press "Upload L1DDC Mapping" in the upper right corner.
    4. Your FEB and L1DDC mappings should now be exported /afs/cern.ch/user/m/mkongsor/public/L1DDC/outputconfigs (an alternative export folder can easily be specified in the program).

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.
