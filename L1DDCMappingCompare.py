import numpy as np
import csv

SCAT = []
MTFT = []

same = True

# Input file here  #

# inputFile = 'l1ddc_mapping_20MNIWSAC00001_2020-03-11_17:57:59.csv'
inputFile = 'l1ddc_mapping_20MNIWSAC00004_2020-06-08_14:37:42.csv'

####################

testlogDir = '/afs/cern.ch/user/s/stgcic/public/sca_l1ddc_mapping/outputConfigs/'
productionlogDir = '/afs/cern.ch/user/s/stgcic/public/sca_l1ddc_mapping/productionLogs/stgc_l1ddc_production_testing_logs'

# Reading SCA IDs and MTF numbers from test log

with open(testlogDir + inputFile, 'r') as testlog:
    read = csv.reader(testlog,delimiter = ',')
    for j,row in enumerate(read):
        if 5 < j < 15:
            SCAT.append(str(row[3]))
	    MTFT.append(str(row[7]))

    testlog.close()

# Reading SCA IDs and MTF numbers from production logs, using MTF numbers to find correct log


for i,testMTF in enumerate(MTFT):

	try:
		with open(productionlogDir+str(testMTF)+'.txt', 'r') as prodlog:

		     read = prodlog.read().replace('\n', '  ').replace('\t','  ')

		     for char_idx, char in enumerate(read):

			   if read[char_idx:char_idx+3] == 'SCA':
				SCAP = str(read[char_idx+7:char_idx+12]).rstrip()

				# Comparing SCA IDs between logs

				if SCAP == str(SCAT[i]):
					print('L1DDC' + str(i) + ' has matching logs')
				
				else:
					same = False
					print('######################################')
					print('L1DDC' + str(i) + ' has inconsistent SCA IDs logged')
					print('TestSCA = ' + str(SCAT[i]))
					print('ProductionLogSCA = ' + SCAP)
					print('######################################')

		     prodlog.close()
		
	except:
		print('ERROR, a log file for L1DDC with MTF number ' + str(testMTF) + ' does NOT exist!')
		exit()

# Concluding whether the logs agree

if same == False:
	print('WARNING')
	print('ERROR, one or more L1DDCs have a mapping discrpancy!')			
		
if same == True:
	print('No discrepancies found')
	print('Comparison Successful!')
