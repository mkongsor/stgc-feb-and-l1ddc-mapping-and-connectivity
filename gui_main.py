# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created by: PyQt4 UI code generator 4.12.1
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    def _fromUtf8(s):
        return s

try:
    _encoding = QtGui.QApplication.UnicodeUTF8
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig, _encoding)
except AttributeError:
    def _translate(context, text, disambig):
        return QtGui.QApplication.translate(context, text, disambig)

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName(_fromUtf8("MainWindow"))
        MainWindow.resize(1797, 909)
        self.centralwidget = QtGui.QWidget(MainWindow)
        self.centralwidget.setObjectName(_fromUtf8("centralwidget"))
        self.vtrx0 = QtGui.QLabel(self.centralwidget)
        self.vtrx0.setGeometry(QtCore.QRect(340, 920, 67, 17))
        self.vtrx0.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx0.setObjectName(_fromUtf8("vtrx0"))
        self.vtrx1 = QtGui.QLabel(self.centralwidget)
        self.vtrx1.setGeometry(QtCore.QRect(430, 920, 67, 17))
        self.vtrx1.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx1.setObjectName(_fromUtf8("vtrx1"))
        self.vtrx2 = QtGui.QLabel(self.centralwidget)
        self.vtrx2.setGeometry(QtCore.QRect(520, 920, 67, 17))
        self.vtrx2.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx2.setObjectName(_fromUtf8("vtrx2"))
        self.vtrx3 = QtGui.QLabel(self.centralwidget)
        self.vtrx3.setGeometry(QtCore.QRect(620, 920, 67, 17))
        self.vtrx3.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx3.setObjectName(_fromUtf8("vtrx3"))
        self.vtrx4 = QtGui.QLabel(self.centralwidget)
        self.vtrx4.setGeometry(QtCore.QRect(720, 930, 67, 17))
        self.vtrx4.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx4.setObjectName(_fromUtf8("vtrx4"))
        self.vtrx5 = QtGui.QLabel(self.centralwidget)
        self.vtrx5.setGeometry(QtCore.QRect(850, 940, 67, 17))
        self.vtrx5.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx5.setObjectName(_fromUtf8("vtrx5"))
        self.vtrx6 = QtGui.QLabel(self.centralwidget)
        self.vtrx6.setGeometry(QtCore.QRect(1010, 920, 67, 17))
        self.vtrx6.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx6.setObjectName(_fromUtf8("vtrx6"))
        self.vtrx7 = QtGui.QLabel(self.centralwidget)
        self.vtrx7.setGeometry(QtCore.QRect(1120, 920, 67, 17))
        self.vtrx7.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx7.setObjectName(_fromUtf8("vtrx7"))
        self.vtrx8 = QtGui.QLabel(self.centralwidget)
        self.vtrx8.setGeometry(QtCore.QRect(1300, 920, 67, 17))
        self.vtrx8.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx8.setObjectName(_fromUtf8("vtrx8"))
        self.vtrx9 = QtGui.QLabel(self.centralwidget)
        self.vtrx9.setGeometry(QtCore.QRect(1480, 930, 67, 17))
        self.vtrx9.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx9.setObjectName(_fromUtf8("vtrx9"))
        self.vtrx10 = QtGui.QLabel(self.centralwidget)
        self.vtrx10.setGeometry(QtCore.QRect(1560, 920, 67, 17))
        self.vtrx10.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx10.setObjectName(_fromUtf8("vtrx10"))
        self.vtrx11 = QtGui.QLabel(self.centralwidget)
        self.vtrx11.setGeometry(QtCore.QRect(1660, 930, 67, 17))
        self.vtrx11.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx11.setObjectName(_fromUtf8("vtrx11"))
        self.tabWidget = QtGui.QTabWidget(self.centralwidget)
        self.tabWidget.setGeometry(QtCore.QRect(0, 0, 1781, 941))
        self.tabWidget.setObjectName(_fromUtf8("tabWidget"))
        self.tab = QtGui.QWidget()
        self.tab.setObjectName(_fromUtf8("tab"))
        self.verticalLayoutWidget_5 = QtGui.QWidget(self.tab)
        self.verticalLayoutWidget_5.setGeometry(QtCore.QRect(440, 0, 1331, 841))
        self.verticalLayoutWidget_5.setObjectName(_fromUtf8("verticalLayoutWidget_5"))
        self.verticalLayout_5 = QtGui.QVBoxLayout(self.verticalLayoutWidget_5)
        self.verticalLayout_5.setMargin(0)
        self.verticalLayout_5.setObjectName(_fromUtf8("verticalLayout_5"))
        self.frame_4 = QtGui.QFrame(self.verticalLayoutWidget_5)
        self.frame_4.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_4.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_4.setObjectName(_fromUtf8("frame_4"))
        self.verticalLayoutWidget_6 = QtGui.QWidget(self.frame_4)
        self.verticalLayoutWidget_6.setGeometry(QtCore.QRect(10, 80, 321, 331))
        self.verticalLayoutWidget_6.setObjectName(_fromUtf8("verticalLayoutWidget_6"))
        self.verticalLayout_6 = QtGui.QVBoxLayout(self.verticalLayoutWidget_6)
        self.verticalLayout_6.setMargin(0)
        self.verticalLayout_6.setObjectName(_fromUtf8("verticalLayout_6"))
        self.frame_5 = QtGui.QFrame(self.verticalLayoutWidget_6)
        self.frame_5.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_5.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_5.setObjectName(_fromUtf8("frame_5"))
        self.gbtx7 = QtGui.QLineEdit(self.frame_5)
        self.gbtx7.setGeometry(QtCore.QRect(110, 290, 61, 21))
        self.gbtx7.setObjectName(_fromUtf8("gbtx7"))
        self.flx6 = QtGui.QLineEdit(self.frame_5)
        self.flx6.setGeometry(QtCore.QRect(200, 260, 61, 21))
        self.flx6.setObjectName(_fromUtf8("flx6"))
        self.gbtx4 = QtGui.QLineEdit(self.frame_5)
        self.gbtx4.setGeometry(QtCore.QRect(110, 200, 61, 21))
        self.gbtx4.setObjectName(_fromUtf8("gbtx4"))
        self.fiber4 = QtGui.QLabel(self.frame_5)
        self.fiber4.setGeometry(QtCore.QRect(40, 170, 16, 17))
        self.fiber4.setObjectName(_fromUtf8("fiber4"))
        self.fiber3 = QtGui.QLabel(self.frame_5)
        self.fiber3.setGeometry(QtCore.QRect(40, 140, 16, 17))
        self.fiber3.setObjectName(_fromUtf8("fiber3"))
        self.gbtx6 = QtGui.QLineEdit(self.frame_5)
        self.gbtx6.setGeometry(QtCore.QRect(110, 260, 61, 21))
        self.gbtx6.setObjectName(_fromUtf8("gbtx6"))
        self.flx4 = QtGui.QLineEdit(self.frame_5)
        self.flx4.setGeometry(QtCore.QRect(200, 200, 61, 21))
        self.flx4.setObjectName(_fromUtf8("flx4"))
        self.fiber8 = QtGui.QLabel(self.frame_5)
        self.fiber8.setGeometry(QtCore.QRect(40, 290, 16, 17))
        self.fiber8.setObjectName(_fromUtf8("fiber8"))
        self.gbtx_nbr = QtGui.QLabel(self.frame_5)
        self.gbtx_nbr.setGeometry(QtCore.QRect(110, 50, 71, 16))
        self.gbtx_nbr.setObjectName(_fromUtf8("gbtx_nbr"))
        self.flx1 = QtGui.QLineEdit(self.frame_5)
        self.flx1.setGeometry(QtCore.QRect(200, 110, 61, 21))
        self.flx1.setObjectName(_fromUtf8("flx1"))
        self.gbtx3 = QtGui.QLineEdit(self.frame_5)
        self.gbtx3.setGeometry(QtCore.QRect(110, 170, 61, 21))
        self.gbtx3.setObjectName(_fromUtf8("gbtx3"))
        self.fiber6 = QtGui.QLabel(self.frame_5)
        self.fiber6.setGeometry(QtCore.QRect(40, 200, 16, 17))
        self.fiber6.setObjectName(_fromUtf8("fiber6"))
        self.gbtx2 = QtGui.QLineEdit(self.frame_5)
        self.gbtx2.setGeometry(QtCore.QRect(110, 140, 61, 21))
        self.gbtx2.setObjectName(_fromUtf8("gbtx2"))
        self.fiber1 = QtGui.QLabel(self.frame_5)
        self.fiber1.setGeometry(QtCore.QRect(40, 80, 16, 17))
        self.fiber1.setObjectName(_fromUtf8("fiber1"))
        self.line_11 = QtGui.QFrame(self.frame_5)
        self.line_11.setGeometry(QtCore.QRect(80, 60, 20, 261))
        self.line_11.setFrameShape(QtGui.QFrame.VLine)
        self.line_11.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_11.setObjectName(_fromUtf8("line_11"))
        self.flx0 = QtGui.QLineEdit(self.frame_5)
        self.flx0.setGeometry(QtCore.QRect(200, 80, 61, 21))
        self.flx0.setObjectName(_fromUtf8("flx0"))
        self.fiber2 = QtGui.QLabel(self.frame_5)
        self.fiber2.setGeometry(QtCore.QRect(40, 110, 16, 17))
        self.fiber2.setObjectName(_fromUtf8("fiber2"))
        self.flx3 = QtGui.QLineEdit(self.frame_5)
        self.flx3.setGeometry(QtCore.QRect(200, 170, 61, 21))
        self.flx3.setObjectName(_fromUtf8("flx3"))
        self.gbtx_nbr_2 = QtGui.QLabel(self.frame_5)
        self.gbtx_nbr_2.setGeometry(QtCore.QRect(190, 50, 111, 17))
        self.gbtx_nbr_2.setObjectName(_fromUtf8("gbtx_nbr_2"))
        self.fiber_nbr = QtGui.QLabel(self.frame_5)
        self.fiber_nbr.setGeometry(QtCore.QRect(10, 50, 71, 16))
        self.fiber_nbr.setObjectName(_fromUtf8("fiber_nbr"))
        self.gbtx0 = QtGui.QLineEdit(self.frame_5)
        self.gbtx0.setGeometry(QtCore.QRect(110, 80, 61, 21))
        self.gbtx0.setText(_fromUtf8(""))
        self.gbtx0.setObjectName(_fromUtf8("gbtx0"))
        self.gbtx5 = QtGui.QLineEdit(self.frame_5)
        self.gbtx5.setGeometry(QtCore.QRect(110, 230, 61, 21))
        self.gbtx5.setObjectName(_fromUtf8("gbtx5"))
        self.flx2 = QtGui.QLineEdit(self.frame_5)
        self.flx2.setGeometry(QtCore.QRect(200, 140, 61, 21))
        self.flx2.setObjectName(_fromUtf8("flx2"))
        self.fiber7 = QtGui.QLabel(self.frame_5)
        self.fiber7.setGeometry(QtCore.QRect(40, 260, 16, 17))
        self.fiber7.setObjectName(_fromUtf8("fiber7"))
        self.gbtx1 = QtGui.QLineEdit(self.frame_5)
        self.gbtx1.setGeometry(QtCore.QRect(110, 110, 61, 21))
        self.gbtx1.setObjectName(_fromUtf8("gbtx1"))
        self.flx5 = QtGui.QLineEdit(self.frame_5)
        self.flx5.setGeometry(QtCore.QRect(200, 230, 61, 21))
        self.flx5.setObjectName(_fromUtf8("flx5"))
        self.fiber5 = QtGui.QLabel(self.frame_5)
        self.fiber5.setGeometry(QtCore.QRect(40, 230, 16, 17))
        self.fiber5.setObjectName(_fromUtf8("fiber5"))
        self.flx7 = QtGui.QLineEdit(self.frame_5)
        self.flx7.setGeometry(QtCore.QRect(200, 290, 61, 21))
        self.flx7.setObjectName(_fromUtf8("flx7"))
        self.label_63 = QtGui.QLabel(self.frame_5)
        self.label_63.setGeometry(QtCore.QRect(10, 0, 301, 51))
        self.label_63.setObjectName(_fromUtf8("label_63"))
        self.verticalLayout_6.addWidget(self.frame_5)
        self.gbtx_write = QtGui.QPushButton(self.frame_4)
        self.gbtx_write.setGeometry(QtCore.QRect(70, 20, 141, 21))
        self.gbtx_write.setObjectName(_fromUtf8("gbtx_write"))
        self.gbtx_read = QtGui.QPushButton(self.frame_4)
        self.gbtx_read.setGeometry(QtCore.QRect(220, 20, 141, 21))
        self.gbtx_read.setObjectName(_fromUtf8("gbtx_read"))
        self.verticalLayoutWidget_7 = QtGui.QWidget(self.frame_4)
        self.verticalLayoutWidget_7.setGeometry(QtCore.QRect(10, 420, 321, 381))
        self.verticalLayoutWidget_7.setObjectName(_fromUtf8("verticalLayoutWidget_7"))
        self.verticalLayout_7 = QtGui.QVBoxLayout(self.verticalLayoutWidget_7)
        self.verticalLayout_7.setMargin(0)
        self.verticalLayout_7.setObjectName(_fromUtf8("verticalLayout_7"))
        self.frame_6 = QtGui.QFrame(self.verticalLayoutWidget_7)
        self.frame_6.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_6.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_6.setObjectName(_fromUtf8("frame_6"))
        self.label_57 = QtGui.QLabel(self.frame_6)
        self.label_57.setGeometry(QtCore.QRect(110, 110, 16, 17))
        self.label_57.setObjectName(_fromUtf8("label_57"))
        self.phase4 = QtGui.QLineEdit(self.frame_6)
        self.phase4.setGeometry(QtCore.QRect(170, 200, 41, 21))
        self.phase4.setText(_fromUtf8(""))
        self.phase4.setObjectName(_fromUtf8("phase4"))
        self.label_55 = QtGui.QLabel(self.frame_6)
        self.label_55.setGeometry(QtCore.QRect(90, 50, 51, 17))
        self.label_55.setObjectName(_fromUtf8("label_55"))
        self.label_56 = QtGui.QLabel(self.frame_6)
        self.label_56.setGeometry(QtCore.QRect(110, 80, 16, 17))
        self.label_56.setObjectName(_fromUtf8("label_56"))
        self.label_61 = QtGui.QLabel(self.frame_6)
        self.label_61.setGeometry(QtCore.QRect(170, 50, 51, 17))
        self.label_61.setObjectName(_fromUtf8("label_61"))
        self.phase1 = QtGui.QLineEdit(self.frame_6)
        self.phase1.setGeometry(QtCore.QRect(170, 110, 41, 21))
        self.phase1.setObjectName(_fromUtf8("phase1"))
        self.label_58 = QtGui.QLabel(self.frame_6)
        self.label_58.setGeometry(QtCore.QRect(110, 140, 16, 17))
        self.label_58.setObjectName(_fromUtf8("label_58"))
        self.phase3 = QtGui.QLineEdit(self.frame_6)
        self.phase3.setGeometry(QtCore.QRect(170, 170, 41, 21))
        self.phase3.setObjectName(_fromUtf8("phase3"))
        self.label_54 = QtGui.QLabel(self.frame_6)
        self.label_54.setGeometry(QtCore.QRect(0, 0, 311, 51))
        self.label_54.setObjectName(_fromUtf8("label_54"))
        self.phase2 = QtGui.QLineEdit(self.frame_6)
        self.phase2.setGeometry(QtCore.QRect(170, 140, 41, 21))
        self.phase2.setObjectName(_fromUtf8("phase2"))
        self.label_60 = QtGui.QLabel(self.frame_6)
        self.label_60.setGeometry(QtCore.QRect(110, 200, 16, 17))
        self.label_60.setObjectName(_fromUtf8("label_60"))
        self.label_59 = QtGui.QLabel(self.frame_6)
        self.label_59.setGeometry(QtCore.QRect(110, 170, 16, 17))
        self.label_59.setObjectName(_fromUtf8("label_59"))
        self.phase0 = QtGui.QLineEdit(self.frame_6)
        self.phase0.setGeometry(QtCore.QRect(170, 80, 41, 21))
        self.phase0.setObjectName(_fromUtf8("phase0"))
        self.line_7 = QtGui.QFrame(self.frame_6)
        self.line_7.setGeometry(QtCore.QRect(140, 60, 20, 221))
        self.line_7.setFrameShape(QtGui.QFrame.VLine)
        self.line_7.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_7.setObjectName(_fromUtf8("line_7"))
        self.label_67 = QtGui.QLabel(self.frame_6)
        self.label_67.setGeometry(QtCore.QRect(50, 230, 91, 20))
        self.label_67.setObjectName(_fromUtf8("label_67"))
        self.label_69 = QtGui.QLabel(self.frame_6)
        self.label_69.setGeometry(QtCore.QRect(50, 260, 91, 20))
        self.label_69.setObjectName(_fromUtf8("label_69"))
        self.verticalLayout_7.addWidget(self.frame_6)
        self.verticalLayoutWidget_8 = QtGui.QWidget(self.frame_4)
        self.verticalLayoutWidget_8.setGeometry(QtCore.QRect(670, 80, 641, 721))
        self.verticalLayoutWidget_8.setObjectName(_fromUtf8("verticalLayoutWidget_8"))
        self.verticalLayout_8 = QtGui.QVBoxLayout(self.verticalLayoutWidget_8)
        self.verticalLayout_8.setMargin(0)
        self.verticalLayout_8.setObjectName(_fromUtf8("verticalLayout_8"))
        self.frame_7 = QtGui.QFrame(self.verticalLayoutWidget_8)
        self.frame_7.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_7.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_7.setObjectName(_fromUtf8("frame_7"))
        self.verticalLayoutWidget = QtGui.QWidget(self.frame_7)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 40, 621, 671))
        self.verticalLayoutWidget.setObjectName(_fromUtf8("verticalLayoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setMargin(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.hextabs = QtGui.QTabWidget(self.verticalLayoutWidget)
        self.hextabs.setObjectName(_fromUtf8("hextabs"))
        self.gch0 = QtGui.QWidget()
        self.gch0.setAutoFillBackground(False)
        self.gch0.setObjectName(_fromUtf8("gch0"))
        self.gch0_lab = QtGui.QLabel(self.gch0)
        self.gch0_lab.setGeometry(QtCore.QRect(0, 0, 621, 581))
        self.gch0_lab.setText(_fromUtf8(""))
        self.gch0_lab.setObjectName(_fromUtf8("gch0_lab"))
        self.hextabs.addTab(self.gch0, _fromUtf8(""))
        self.gch1 = QtGui.QWidget()
        self.gch1.setObjectName(_fromUtf8("gch1"))
        self.gch1_lab = QtGui.QLabel(self.gch1)
        self.gch1_lab.setGeometry(QtCore.QRect(0, 0, 621, 581))
        self.gch1_lab.setText(_fromUtf8(""))
        self.gch1_lab.setObjectName(_fromUtf8("gch1_lab"))
        self.hextabs.addTab(self.gch1, _fromUtf8(""))
        self.gch2 = QtGui.QWidget()
        self.gch2.setObjectName(_fromUtf8("gch2"))
        self.gch2_lab = QtGui.QLabel(self.gch2)
        self.gch2_lab.setGeometry(QtCore.QRect(0, 0, 621, 581))
        self.gch2_lab.setText(_fromUtf8(""))
        self.gch2_lab.setObjectName(_fromUtf8("gch2_lab"))
        self.hextabs.addTab(self.gch2, _fromUtf8(""))
        self.gch3 = QtGui.QWidget()
        self.gch3.setObjectName(_fromUtf8("gch3"))
        self.gch3_lab = QtGui.QLabel(self.gch3)
        self.gch3_lab.setGeometry(QtCore.QRect(0, 0, 621, 581))
        self.gch3_lab.setText(_fromUtf8(""))
        self.gch3_lab.setObjectName(_fromUtf8("gch3_lab"))
        self.hextabs.addTab(self.gch3, _fromUtf8(""))
        self.gch4 = QtGui.QWidget()
        self.gch4.setObjectName(_fromUtf8("gch4"))
        self.gch4_lab = QtGui.QLabel(self.gch4)
        self.gch4_lab.setGeometry(QtCore.QRect(0, 0, 621, 581))
        self.gch4_lab.setText(_fromUtf8(""))
        self.gch4_lab.setObjectName(_fromUtf8("gch4_lab"))
        self.hextabs.addTab(self.gch4, _fromUtf8(""))
        self.gch5 = QtGui.QWidget()
        self.gch5.setObjectName(_fromUtf8("gch5"))
        self.gch5_lab = QtGui.QLabel(self.gch5)
        self.gch5_lab.setGeometry(QtCore.QRect(0, 0, 621, 581))
        self.gch5_lab.setText(_fromUtf8(""))
        self.gch5_lab.setObjectName(_fromUtf8("gch5_lab"))
        self.hextabs.addTab(self.gch5, _fromUtf8(""))
        self.gch6 = QtGui.QWidget()
        self.gch6.setObjectName(_fromUtf8("gch6"))
        self.gch6_lab = QtGui.QLabel(self.gch6)
        self.gch6_lab.setGeometry(QtCore.QRect(0, 0, 621, 581))
        self.gch6_lab.setText(_fromUtf8(""))
        self.gch6_lab.setObjectName(_fromUtf8("gch6_lab"))
        self.hextabs.addTab(self.gch6, _fromUtf8(""))
        self.gch7 = QtGui.QWidget()
        self.gch7.setObjectName(_fromUtf8("gch7"))
        self.gch7_lab = QtGui.QLabel(self.gch7)
        self.gch7_lab.setGeometry(QtCore.QRect(0, 0, 621, 581))
        self.gch7_lab.setText(_fromUtf8(""))
        self.gch7_lab.setObjectName(_fromUtf8("gch7_lab"))
        self.hextabs.addTab(self.gch7, _fromUtf8(""))
        self.verticalLayout.addWidget(self.hextabs)
        self.label_64 = QtGui.QLabel(self.frame_7)
        self.label_64.setGeometry(QtCore.QRect(90, 0, 491, 41))
        self.label_64.setObjectName(_fromUtf8("label_64"))
        self.verticalLayout_8.addWidget(self.frame_7)
        self.label_11 = QtGui.QLabel(self.frame_4)
        self.label_11.setGeometry(QtCore.QRect(30, 50, 67, 17))
        self.label_11.setObjectName(_fromUtf8("label_11"))
        self.label_12 = QtGui.QLabel(self.frame_4)
        self.label_12.setGeometry(QtCore.QRect(10, 20, 67, 17))
        self.label_12.setObjectName(_fromUtf8("label_12"))
        self.open_btn = QtGui.QPushButton(self.frame_4)
        self.open_btn.setGeometry(QtCore.QRect(70, 50, 141, 21))
        self.open_btn.setObjectName(_fromUtf8("open_btn"))
        self.save_btn = QtGui.QPushButton(self.frame_4)
        self.save_btn.setGeometry(QtCore.QRect(220, 50, 141, 21))
        self.save_btn.setObjectName(_fromUtf8("save_btn"))
        self.upload_btn = QtGui.QPushButton(self.frame_4)
        self.upload_btn.setGeometry(QtCore.QRect(900, 50, 411, 21))
        self.upload_btn.setObjectName(_fromUtf8("upload_btn"))
        self.wedgeid = QtGui.QLineEdit(self.frame_4)
        self.wedgeid.setGeometry(QtCore.QRect(1010, 20, 301, 21))
        self.wedgeid.setObjectName(_fromUtf8("wedgeid"))
        self.label_9 = QtGui.QLabel(self.frame_4)
        self.label_9.setGeometry(QtCore.QRect(900, 20, 111, 17))
        self.label_9.setObjectName(_fromUtf8("label_9"))
        self.line_3 = QtGui.QFrame(self.frame_4)
        self.line_3.setGeometry(QtCore.QRect(880, 10, 3, 61))
        self.line_3.setFrameShape(QtGui.QFrame.VLine)
        self.line_3.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_3.setObjectName(_fromUtf8("line_3"))
        self.verticalLayoutWidget_12 = QtGui.QWidget(self.frame_4)
        self.verticalLayoutWidget_12.setGeometry(QtCore.QRect(340, 80, 321, 201))
        self.verticalLayoutWidget_12.setObjectName(_fromUtf8("verticalLayoutWidget_12"))
        self.verticalLayout_12 = QtGui.QVBoxLayout(self.verticalLayoutWidget_12)
        self.verticalLayout_12.setMargin(0)
        self.verticalLayout_12.setObjectName(_fromUtf8("verticalLayout_12"))
        self.frame_13 = QtGui.QFrame(self.verticalLayoutWidget_12)
        self.frame_13.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_13.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_13.setObjectName(_fromUtf8("frame_13"))
        self.label_68 = QtGui.QLabel(self.frame_13)
        self.label_68.setGeometry(QtCore.QRect(-10, 0, 301, 51))
        self.label_68.setObjectName(_fromUtf8("label_68"))
        self.mtf0 = QtGui.QLineEdit(self.frame_13)
        self.mtf0.setGeometry(QtCore.QRect(70, 60, 61, 21))
        self.mtf0.setObjectName(_fromUtf8("mtf0"))
        self.mtf1 = QtGui.QLineEdit(self.frame_13)
        self.mtf1.setGeometry(QtCore.QRect(70, 90, 61, 21))
        self.mtf1.setObjectName(_fromUtf8("mtf1"))
        self.mtf2 = QtGui.QLineEdit(self.frame_13)
        self.mtf2.setGeometry(QtCore.QRect(70, 120, 61, 21))
        self.mtf2.setObjectName(_fromUtf8("mtf2"))
        self.mtf3 = QtGui.QLineEdit(self.frame_13)
        self.mtf3.setGeometry(QtCore.QRect(70, 150, 61, 21))
        self.mtf3.setObjectName(_fromUtf8("mtf3"))
        self.mtf4 = QtGui.QLineEdit(self.frame_13)
        self.mtf4.setGeometry(QtCore.QRect(200, 60, 61, 21))
        self.mtf4.setObjectName(_fromUtf8("mtf4"))
        self.mtf5 = QtGui.QLineEdit(self.frame_13)
        self.mtf5.setGeometry(QtCore.QRect(200, 90, 61, 21))
        self.mtf5.setObjectName(_fromUtf8("mtf5"))
        self.mtf7 = QtGui.QLineEdit(self.frame_13)
        self.mtf7.setGeometry(QtCore.QRect(200, 150, 61, 21))
        self.mtf7.setObjectName(_fromUtf8("mtf7"))
        self.mtf6 = QtGui.QLineEdit(self.frame_13)
        self.mtf6.setGeometry(QtCore.QRect(200, 120, 61, 21))
        self.mtf6.setObjectName(_fromUtf8("mtf6"))
        self.label_14 = QtGui.QLabel(self.frame_13)
        self.label_14.setGeometry(QtCore.QRect(10, 60, 67, 17))
        self.label_14.setObjectName(_fromUtf8("label_14"))
        self.label_15 = QtGui.QLabel(self.frame_13)
        self.label_15.setGeometry(QtCore.QRect(10, 90, 67, 17))
        self.label_15.setObjectName(_fromUtf8("label_15"))
        self.label_16 = QtGui.QLabel(self.frame_13)
        self.label_16.setGeometry(QtCore.QRect(10, 120, 67, 17))
        self.label_16.setObjectName(_fromUtf8("label_16"))
        self.label_17 = QtGui.QLabel(self.frame_13)
        self.label_17.setGeometry(QtCore.QRect(10, 150, 67, 17))
        self.label_17.setObjectName(_fromUtf8("label_17"))
        self.label_18 = QtGui.QLabel(self.frame_13)
        self.label_18.setGeometry(QtCore.QRect(140, 60, 67, 17))
        self.label_18.setObjectName(_fromUtf8("label_18"))
        self.label_19 = QtGui.QLabel(self.frame_13)
        self.label_19.setGeometry(QtCore.QRect(140, 90, 67, 17))
        self.label_19.setObjectName(_fromUtf8("label_19"))
        self.label_20 = QtGui.QLabel(self.frame_13)
        self.label_20.setGeometry(QtCore.QRect(140, 120, 67, 17))
        self.label_20.setObjectName(_fromUtf8("label_20"))
        self.label_21 = QtGui.QLabel(self.frame_13)
        self.label_21.setGeometry(QtCore.QRect(140, 150, 67, 17))
        self.label_21.setObjectName(_fromUtf8("label_21"))
        self.verticalLayout_12.addWidget(self.frame_13)
        self.default_vtn = QtGui.QPushButton(self.frame_4)
        self.default_vtn.setGeometry(QtCore.QRect(370, 20, 201, 21))
        self.default_vtn.setObjectName(_fromUtf8("default_vtn"))
        self.fileloc = QtGui.QLabel(self.frame_4)
        self.fileloc.setGeometry(QtCore.QRect(370, 50, 491, 17))
        self.fileloc.setObjectName(_fromUtf8("fileloc"))
        self.verticalLayoutWidget_9 = QtGui.QWidget(self.frame_4)
        self.verticalLayoutWidget_9.setGeometry(QtCore.QRect(340, 290, 321, 511))
        self.verticalLayoutWidget_9.setObjectName(_fromUtf8("verticalLayoutWidget_9"))
        self.verticalLayout_9 = QtGui.QVBoxLayout(self.verticalLayoutWidget_9)
        self.verticalLayout_9.setMargin(0)
        self.verticalLayout_9.setObjectName(_fromUtf8("verticalLayout_9"))
        self.tabWidget_3 = QtGui.QTabWidget(self.verticalLayoutWidget_9)
        self.tabWidget_3.setObjectName(_fromUtf8("tabWidget_3"))
        self.tab_13 = QtGui.QWidget()
        self.tab_13.setObjectName(_fromUtf8("tab_13"))
        self.label_133 = QtGui.QLabel(self.tab_13)
        self.label_133.setGeometry(QtCore.QRect(10, 0, 281, 41))
        self.label_133.setObjectName(_fromUtf8("label_133"))
        self.label_134 = QtGui.QLabel(self.tab_13)
        self.label_134.setGeometry(QtCore.QRect(60, 160, 31, 21))
        self.label_134.setObjectName(_fromUtf8("label_134"))
        self.gbt_stat0 = QtGui.QProgressBar(self.tab_13)
        self.gbt_stat0.setGeometry(QtCore.QRect(180, 130, 21, 23))
        self.gbt_stat0.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.gbt_stat0.setMaximum(100)
        self.gbt_stat0.setProperty("value", 0)
        self.gbt_stat0.setTextVisible(False)
        self.gbt_stat0.setOrientation(QtCore.Qt.Vertical)
        self.gbt_stat0.setInvertedAppearance(False)
        self.gbt_stat0.setObjectName(_fromUtf8("gbt_stat0"))
        self.gbt_stat1 = QtGui.QProgressBar(self.tab_13)
        self.gbt_stat1.setGeometry(QtCore.QRect(180, 160, 21, 23))
        self.gbt_stat1.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.gbt_stat1.setMaximum(100)
        self.gbt_stat1.setProperty("value", 0)
        self.gbt_stat1.setTextVisible(False)
        self.gbt_stat1.setOrientation(QtCore.Qt.Vertical)
        self.gbt_stat1.setInvertedAppearance(False)
        self.gbt_stat1.setObjectName(_fromUtf8("gbt_stat1"))
        self.label_135 = QtGui.QLabel(self.tab_13)
        self.label_135.setGeometry(QtCore.QRect(60, 310, 31, 21))
        self.label_135.setObjectName(_fromUtf8("label_135"))
        self.gbt_stat2 = QtGui.QProgressBar(self.tab_13)
        self.gbt_stat2.setGeometry(QtCore.QRect(180, 190, 21, 23))
        self.gbt_stat2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.gbt_stat2.setMaximum(100)
        self.gbt_stat2.setProperty("value", 0)
        self.gbt_stat2.setTextVisible(False)
        self.gbt_stat2.setOrientation(QtCore.Qt.Vertical)
        self.gbt_stat2.setInvertedAppearance(False)
        self.gbt_stat2.setObjectName(_fromUtf8("gbt_stat2"))
        self.label_137 = QtGui.QLabel(self.tab_13)
        self.label_137.setGeometry(QtCore.QRect(60, 250, 31, 21))
        self.label_137.setObjectName(_fromUtf8("label_137"))
        self.label_138 = QtGui.QLabel(self.tab_13)
        self.label_138.setGeometry(QtCore.QRect(50, 100, 51, 21))
        self.label_138.setObjectName(_fromUtf8("label_138"))
        self.readstat = QtGui.QPushButton(self.tab_13)
        self.readstat.setGeometry(QtCore.QRect(90, 50, 131, 25))
        self.readstat.setObjectName(_fromUtf8("readstat"))
        self.gbt_stat6 = QtGui.QProgressBar(self.tab_13)
        self.gbt_stat6.setGeometry(QtCore.QRect(180, 310, 21, 23))
        self.gbt_stat6.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.gbt_stat6.setMaximum(100)
        self.gbt_stat6.setProperty("value", 0)
        self.gbt_stat6.setTextVisible(False)
        self.gbt_stat6.setOrientation(QtCore.Qt.Vertical)
        self.gbt_stat6.setInvertedAppearance(False)
        self.gbt_stat6.setObjectName(_fromUtf8("gbt_stat6"))
        self.gbt_stat3 = QtGui.QProgressBar(self.tab_13)
        self.gbt_stat3.setGeometry(QtCore.QRect(180, 220, 21, 23))
        self.gbt_stat3.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.gbt_stat3.setMaximum(100)
        self.gbt_stat3.setProperty("value", 0)
        self.gbt_stat3.setTextVisible(False)
        self.gbt_stat3.setOrientation(QtCore.Qt.Vertical)
        self.gbt_stat3.setInvertedAppearance(False)
        self.gbt_stat3.setObjectName(_fromUtf8("gbt_stat3"))
        self.label_139 = QtGui.QLabel(self.tab_13)
        self.label_139.setGeometry(QtCore.QRect(140, 100, 161, 21))
        self.label_139.setObjectName(_fromUtf8("label_139"))
        self.gbt_stat5 = QtGui.QProgressBar(self.tab_13)
        self.gbt_stat5.setGeometry(QtCore.QRect(180, 280, 21, 23))
        self.gbt_stat5.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.gbt_stat5.setMaximum(100)
        self.gbt_stat5.setProperty("value", 0)
        self.gbt_stat5.setTextVisible(False)
        self.gbt_stat5.setOrientation(QtCore.Qt.Vertical)
        self.gbt_stat5.setInvertedAppearance(False)
        self.gbt_stat5.setObjectName(_fromUtf8("gbt_stat5"))
        self.label_140 = QtGui.QLabel(self.tab_13)
        self.label_140.setGeometry(QtCore.QRect(60, 220, 31, 21))
        self.label_140.setObjectName(_fromUtf8("label_140"))
        self.label_141 = QtGui.QLabel(self.tab_13)
        self.label_141.setGeometry(QtCore.QRect(60, 130, 31, 21))
        self.label_141.setObjectName(_fromUtf8("label_141"))
        self.label_142 = QtGui.QLabel(self.tab_13)
        self.label_142.setGeometry(QtCore.QRect(60, 280, 31, 21))
        self.label_142.setObjectName(_fromUtf8("label_142"))
        self.gbt_stat4 = QtGui.QProgressBar(self.tab_13)
        self.gbt_stat4.setGeometry(QtCore.QRect(180, 250, 21, 23))
        self.gbt_stat4.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.gbt_stat4.setMaximum(100)
        self.gbt_stat4.setProperty("value", 0)
        self.gbt_stat4.setTextVisible(False)
        self.gbt_stat4.setOrientation(QtCore.Qt.Vertical)
        self.gbt_stat4.setInvertedAppearance(False)
        self.gbt_stat4.setObjectName(_fromUtf8("gbt_stat4"))
        self.label_143 = QtGui.QLabel(self.tab_13)
        self.label_143.setGeometry(QtCore.QRect(60, 190, 31, 21))
        self.label_143.setObjectName(_fromUtf8("label_143"))
        self.label_144 = QtGui.QLabel(self.tab_13)
        self.label_144.setGeometry(QtCore.QRect(60, 340, 31, 21))
        self.label_144.setObjectName(_fromUtf8("label_144"))
        self.gbt_stat7 = QtGui.QProgressBar(self.tab_13)
        self.gbt_stat7.setGeometry(QtCore.QRect(180, 340, 21, 23))
        self.gbt_stat7.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.gbt_stat7.setMaximum(100)
        self.gbt_stat7.setProperty("value", 0)
        self.gbt_stat7.setTextVisible(False)
        self.gbt_stat7.setOrientation(QtCore.Qt.Vertical)
        self.gbt_stat7.setInvertedAppearance(False)
        self.gbt_stat7.setObjectName(_fromUtf8("gbt_stat7"))
        self.line_22 = QtGui.QFrame(self.tab_13)
        self.line_22.setGeometry(QtCore.QRect(110, 110, 20, 251))
        self.line_22.setFrameShape(QtGui.QFrame.VLine)
        self.line_22.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_22.setObjectName(_fromUtf8("line_22"))
        self.tabWidget_3.addTab(self.tab_13, _fromUtf8(""))
        self.tab_5 = QtGui.QWidget()
        self.tab_5.setObjectName(_fromUtf8("tab_5"))
        self.frame_10 = QtGui.QFrame(self.tab_5)
        self.frame_10.setGeometry(QtCore.QRect(0, 0, 319, 479))
        self.frame_10.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_10.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_10.setObjectName(_fromUtf8("frame_10"))
        self.label_70 = QtGui.QLabel(self.frame_10)
        self.label_70.setGeometry(QtCore.QRect(0, 0, 311, 51))
        self.label_70.setObjectName(_fromUtf8("label_70"))
        self.label_26 = QtGui.QLabel(self.frame_10)
        self.label_26.setGeometry(QtCore.QRect(70, 70, 171, 21))
        self.label_26.setObjectName(_fromUtf8("label_26"))
        self.label_27 = QtGui.QLabel(self.frame_10)
        self.label_27.setGeometry(QtCore.QRect(27, 89, 51, 31))
        self.label_27.setObjectName(_fromUtf8("label_27"))
        self.line_8 = QtGui.QFrame(self.frame_10)
        self.line_8.setGeometry(QtCore.QRect(80, 100, 20, 231))
        self.line_8.setFrameShape(QtGui.QFrame.VLine)
        self.line_8.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_8.setObjectName(_fromUtf8("line_8"))
        self.label_28 = QtGui.QLabel(self.frame_10)
        self.label_28.setGeometry(QtCore.QRect(117, 90, 161, 31))
        self.label_28.setObjectName(_fromUtf8("label_28"))
        self.label_29 = QtGui.QLabel(self.frame_10)
        self.label_29.setGeometry(QtCore.QRect(47, 116, 31, 31))
        self.label_29.setObjectName(_fromUtf8("label_29"))
        self.label_31 = QtGui.QLabel(self.frame_10)
        self.label_31.setGeometry(QtCore.QRect(47, 146, 31, 31))
        self.label_31.setObjectName(_fromUtf8("label_31"))
        self.label_32 = QtGui.QLabel(self.frame_10)
        self.label_32.setGeometry(QtCore.QRect(47, 176, 31, 31))
        self.label_32.setObjectName(_fromUtf8("label_32"))
        self.label_33 = QtGui.QLabel(self.frame_10)
        self.label_33.setGeometry(QtCore.QRect(47, 206, 31, 31))
        self.label_33.setObjectName(_fromUtf8("label_33"))
        self.label_34 = QtGui.QLabel(self.frame_10)
        self.label_34.setGeometry(QtCore.QRect(47, 236, 31, 31))
        self.label_34.setObjectName(_fromUtf8("label_34"))
        self.label_35 = QtGui.QLabel(self.frame_10)
        self.label_35.setGeometry(QtCore.QRect(47, 266, 31, 31))
        self.label_35.setObjectName(_fromUtf8("label_35"))
        self.label_36 = QtGui.QLabel(self.frame_10)
        self.label_36.setGeometry(QtCore.QRect(47, 296, 31, 31))
        self.label_36.setObjectName(_fromUtf8("label_36"))
        self.label_37 = QtGui.QLabel(self.frame_10)
        self.label_37.setGeometry(QtCore.QRect(100, 360, 151, 31))
        self.label_37.setObjectName(_fromUtf8("label_37"))
        self.label_38 = QtGui.QLabel(self.frame_10)
        self.label_38.setGeometry(QtCore.QRect(90, 390, 161, 21))
        self.label_38.setObjectName(_fromUtf8("label_38"))
        self.plock1_0 = QtGui.QProgressBar(self.frame_10)
        self.plock1_0.setGeometry(QtCore.QRect(170, 120, 21, 23))
        self.plock1_0.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock1_0.setMaximum(100)
        self.plock1_0.setProperty("value", 0)
        self.plock1_0.setTextVisible(False)
        self.plock1_0.setOrientation(QtCore.Qt.Vertical)
        self.plock1_0.setInvertedAppearance(False)
        self.plock1_0.setObjectName(_fromUtf8("plock1_0"))
        self.plock2_0 = QtGui.QProgressBar(self.frame_10)
        self.plock2_0.setGeometry(QtCore.QRect(170, 150, 21, 23))
        self.plock2_0.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock2_0.setMaximum(100)
        self.plock2_0.setProperty("value", 0)
        self.plock2_0.setTextVisible(False)
        self.plock2_0.setOrientation(QtCore.Qt.Vertical)
        self.plock2_0.setInvertedAppearance(False)
        self.plock2_0.setObjectName(_fromUtf8("plock2_0"))
        self.plock3_0 = QtGui.QProgressBar(self.frame_10)
        self.plock3_0.setGeometry(QtCore.QRect(170, 180, 21, 23))
        self.plock3_0.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock3_0.setMaximum(100)
        self.plock3_0.setProperty("value", 0)
        self.plock3_0.setTextVisible(False)
        self.plock3_0.setOrientation(QtCore.Qt.Vertical)
        self.plock3_0.setInvertedAppearance(False)
        self.plock3_0.setObjectName(_fromUtf8("plock3_0"))
        self.plock4_0 = QtGui.QProgressBar(self.frame_10)
        self.plock4_0.setGeometry(QtCore.QRect(170, 210, 21, 23))
        self.plock4_0.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock4_0.setMaximum(100)
        self.plock4_0.setProperty("value", 0)
        self.plock4_0.setTextVisible(False)
        self.plock4_0.setOrientation(QtCore.Qt.Vertical)
        self.plock4_0.setInvertedAppearance(False)
        self.plock4_0.setObjectName(_fromUtf8("plock4_0"))
        self.plock5_0 = QtGui.QProgressBar(self.frame_10)
        self.plock5_0.setGeometry(QtCore.QRect(170, 240, 21, 23))
        self.plock5_0.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock5_0.setMaximum(100)
        self.plock5_0.setProperty("value", 0)
        self.plock5_0.setTextVisible(False)
        self.plock5_0.setOrientation(QtCore.Qt.Vertical)
        self.plock5_0.setInvertedAppearance(False)
        self.plock5_0.setObjectName(_fromUtf8("plock5_0"))
        self.plock6_0 = QtGui.QProgressBar(self.frame_10)
        self.plock6_0.setGeometry(QtCore.QRect(170, 270, 21, 23))
        self.plock6_0.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock6_0.setMaximum(100)
        self.plock6_0.setProperty("value", 0)
        self.plock6_0.setTextVisible(False)
        self.plock6_0.setOrientation(QtCore.Qt.Vertical)
        self.plock6_0.setInvertedAppearance(False)
        self.plock6_0.setObjectName(_fromUtf8("plock6_0"))
        self.plock7_0 = QtGui.QProgressBar(self.frame_10)
        self.plock7_0.setGeometry(QtCore.QRect(170, 300, 21, 23))
        self.plock7_0.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock7_0.setMaximum(100)
        self.plock7_0.setProperty("value", 0)
        self.plock7_0.setTextVisible(False)
        self.plock7_0.setOrientation(QtCore.Qt.Vertical)
        self.plock7_0.setInvertedAppearance(False)
        self.plock7_0.setObjectName(_fromUtf8("plock7_0"))
        self.plock0_0 = QtGui.QProgressBar(self.frame_10)
        self.plock0_0.setGeometry(QtCore.QRect(130, 420, 21, 23))
        self.plock0_0.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock0_0.setMaximum(100)
        self.plock0_0.setProperty("value", 0)
        self.plock0_0.setTextVisible(False)
        self.plock0_0.setOrientation(QtCore.Qt.Vertical)
        self.plock0_0.setInvertedAppearance(False)
        self.plock0_0.setObjectName(_fromUtf8("plock0_0"))
        self.tabWidget_3.addTab(self.tab_5, _fromUtf8(""))
        self.tab_6 = QtGui.QWidget()
        self.tab_6.setObjectName(_fromUtf8("tab_6"))
        self.frame_8 = QtGui.QFrame(self.tab_6)
        self.frame_8.setGeometry(QtCore.QRect(0, 0, 319, 479))
        self.frame_8.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_8.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_8.setObjectName(_fromUtf8("frame_8"))
        self.label_66 = QtGui.QLabel(self.frame_8)
        self.label_66.setGeometry(QtCore.QRect(0, 0, 311, 51))
        self.label_66.setObjectName(_fromUtf8("label_66"))
        self.label_3 = QtGui.QLabel(self.frame_8)
        self.label_3.setGeometry(QtCore.QRect(70, 70, 171, 21))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.label_4 = QtGui.QLabel(self.frame_8)
        self.label_4.setGeometry(QtCore.QRect(27, 89, 51, 31))
        self.label_4.setObjectName(_fromUtf8("label_4"))
        self.line_6 = QtGui.QFrame(self.frame_8)
        self.line_6.setGeometry(QtCore.QRect(80, 100, 20, 231))
        self.line_6.setFrameShape(QtGui.QFrame.VLine)
        self.line_6.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_6.setObjectName(_fromUtf8("line_6"))
        self.label_5 = QtGui.QLabel(self.frame_8)
        self.label_5.setGeometry(QtCore.QRect(117, 90, 161, 31))
        self.label_5.setObjectName(_fromUtf8("label_5"))
        self.label_6 = QtGui.QLabel(self.frame_8)
        self.label_6.setGeometry(QtCore.QRect(47, 116, 31, 31))
        self.label_6.setObjectName(_fromUtf8("label_6"))
        self.label_7 = QtGui.QLabel(self.frame_8)
        self.label_7.setGeometry(QtCore.QRect(47, 146, 31, 31))
        self.label_7.setObjectName(_fromUtf8("label_7"))
        self.label_8 = QtGui.QLabel(self.frame_8)
        self.label_8.setGeometry(QtCore.QRect(47, 176, 31, 31))
        self.label_8.setObjectName(_fromUtf8("label_8"))
        self.label_10 = QtGui.QLabel(self.frame_8)
        self.label_10.setGeometry(QtCore.QRect(47, 206, 31, 31))
        self.label_10.setObjectName(_fromUtf8("label_10"))
        self.label_13 = QtGui.QLabel(self.frame_8)
        self.label_13.setGeometry(QtCore.QRect(47, 236, 31, 31))
        self.label_13.setObjectName(_fromUtf8("label_13"))
        self.label_22 = QtGui.QLabel(self.frame_8)
        self.label_22.setGeometry(QtCore.QRect(47, 266, 31, 31))
        self.label_22.setObjectName(_fromUtf8("label_22"))
        self.label_23 = QtGui.QLabel(self.frame_8)
        self.label_23.setGeometry(QtCore.QRect(47, 296, 31, 31))
        self.label_23.setObjectName(_fromUtf8("label_23"))
        self.label_24 = QtGui.QLabel(self.frame_8)
        self.label_24.setGeometry(QtCore.QRect(100, 360, 151, 31))
        self.label_24.setObjectName(_fromUtf8("label_24"))
        self.label_25 = QtGui.QLabel(self.frame_8)
        self.label_25.setGeometry(QtCore.QRect(90, 390, 161, 21))
        self.label_25.setObjectName(_fromUtf8("label_25"))
        self.plock1_1 = QtGui.QProgressBar(self.frame_8)
        self.plock1_1.setGeometry(QtCore.QRect(170, 120, 21, 23))
        self.plock1_1.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock1_1.setMaximum(100)
        self.plock1_1.setProperty("value", 0)
        self.plock1_1.setTextVisible(False)
        self.plock1_1.setOrientation(QtCore.Qt.Vertical)
        self.plock1_1.setInvertedAppearance(False)
        self.plock1_1.setObjectName(_fromUtf8("plock1_1"))
        self.plock2_1 = QtGui.QProgressBar(self.frame_8)
        self.plock2_1.setGeometry(QtCore.QRect(170, 150, 21, 23))
        self.plock2_1.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock2_1.setMaximum(100)
        self.plock2_1.setProperty("value", 0)
        self.plock2_1.setTextVisible(False)
        self.plock2_1.setOrientation(QtCore.Qt.Vertical)
        self.plock2_1.setInvertedAppearance(False)
        self.plock2_1.setObjectName(_fromUtf8("plock2_1"))
        self.plock3_1 = QtGui.QProgressBar(self.frame_8)
        self.plock3_1.setGeometry(QtCore.QRect(170, 180, 21, 23))
        self.plock3_1.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock3_1.setMaximum(100)
        self.plock3_1.setProperty("value", 0)
        self.plock3_1.setTextVisible(False)
        self.plock3_1.setOrientation(QtCore.Qt.Vertical)
        self.plock3_1.setInvertedAppearance(False)
        self.plock3_1.setObjectName(_fromUtf8("plock3_1"))
        self.plock4_1 = QtGui.QProgressBar(self.frame_8)
        self.plock4_1.setGeometry(QtCore.QRect(170, 210, 21, 23))
        self.plock4_1.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock4_1.setMaximum(100)
        self.plock4_1.setProperty("value", 0)
        self.plock4_1.setTextVisible(False)
        self.plock4_1.setOrientation(QtCore.Qt.Vertical)
        self.plock4_1.setInvertedAppearance(False)
        self.plock4_1.setObjectName(_fromUtf8("plock4_1"))
        self.plock5_1 = QtGui.QProgressBar(self.frame_8)
        self.plock5_1.setGeometry(QtCore.QRect(170, 240, 21, 23))
        self.plock5_1.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock5_1.setMaximum(100)
        self.plock5_1.setProperty("value", 0)
        self.plock5_1.setTextVisible(False)
        self.plock5_1.setOrientation(QtCore.Qt.Vertical)
        self.plock5_1.setInvertedAppearance(False)
        self.plock5_1.setObjectName(_fromUtf8("plock5_1"))
        self.plock6_1 = QtGui.QProgressBar(self.frame_8)
        self.plock6_1.setGeometry(QtCore.QRect(170, 270, 21, 23))
        self.plock6_1.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock6_1.setMaximum(100)
        self.plock6_1.setProperty("value", 0)
        self.plock6_1.setTextVisible(False)
        self.plock6_1.setOrientation(QtCore.Qt.Vertical)
        self.plock6_1.setInvertedAppearance(False)
        self.plock6_1.setObjectName(_fromUtf8("plock6_1"))
        self.plock7_1 = QtGui.QProgressBar(self.frame_8)
        self.plock7_1.setGeometry(QtCore.QRect(170, 300, 21, 23))
        self.plock7_1.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock7_1.setMaximum(100)
        self.plock7_1.setProperty("value", 0)
        self.plock7_1.setTextVisible(False)
        self.plock7_1.setOrientation(QtCore.Qt.Vertical)
        self.plock7_1.setInvertedAppearance(False)
        self.plock7_1.setObjectName(_fromUtf8("plock7_1"))
        self.plock0_1 = QtGui.QProgressBar(self.frame_8)
        self.plock0_1.setGeometry(QtCore.QRect(130, 420, 21, 23))
        self.plock0_1.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock0_1.setMaximum(100)
        self.plock0_1.setProperty("value", 0)
        self.plock0_1.setTextVisible(False)
        self.plock0_1.setOrientation(QtCore.Qt.Vertical)
        self.plock0_1.setInvertedAppearance(False)
        self.plock0_1.setObjectName(_fromUtf8("plock0_1"))
        self.tabWidget_3.addTab(self.tab_6, _fromUtf8(""))
        self.tab_7 = QtGui.QWidget()
        self.tab_7.setObjectName(_fromUtf8("tab_7"))
        self.frame_11 = QtGui.QFrame(self.tab_7)
        self.frame_11.setGeometry(QtCore.QRect(0, 0, 319, 479))
        self.frame_11.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_11.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_11.setObjectName(_fromUtf8("frame_11"))
        self.label_71 = QtGui.QLabel(self.frame_11)
        self.label_71.setGeometry(QtCore.QRect(0, 0, 311, 51))
        self.label_71.setObjectName(_fromUtf8("label_71"))
        self.label_39 = QtGui.QLabel(self.frame_11)
        self.label_39.setGeometry(QtCore.QRect(70, 70, 171, 21))
        self.label_39.setObjectName(_fromUtf8("label_39"))
        self.label_40 = QtGui.QLabel(self.frame_11)
        self.label_40.setGeometry(QtCore.QRect(27, 89, 51, 31))
        self.label_40.setObjectName(_fromUtf8("label_40"))
        self.line_9 = QtGui.QFrame(self.frame_11)
        self.line_9.setGeometry(QtCore.QRect(80, 100, 20, 231))
        self.line_9.setFrameShape(QtGui.QFrame.VLine)
        self.line_9.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_9.setObjectName(_fromUtf8("line_9"))
        self.label_41 = QtGui.QLabel(self.frame_11)
        self.label_41.setGeometry(QtCore.QRect(117, 90, 161, 31))
        self.label_41.setObjectName(_fromUtf8("label_41"))
        self.label_42 = QtGui.QLabel(self.frame_11)
        self.label_42.setGeometry(QtCore.QRect(47, 116, 31, 31))
        self.label_42.setObjectName(_fromUtf8("label_42"))
        self.label_43 = QtGui.QLabel(self.frame_11)
        self.label_43.setGeometry(QtCore.QRect(47, 146, 31, 31))
        self.label_43.setObjectName(_fromUtf8("label_43"))
        self.label_44 = QtGui.QLabel(self.frame_11)
        self.label_44.setGeometry(QtCore.QRect(47, 176, 31, 31))
        self.label_44.setObjectName(_fromUtf8("label_44"))
        self.label_45 = QtGui.QLabel(self.frame_11)
        self.label_45.setGeometry(QtCore.QRect(47, 206, 31, 31))
        self.label_45.setObjectName(_fromUtf8("label_45"))
        self.label_46 = QtGui.QLabel(self.frame_11)
        self.label_46.setGeometry(QtCore.QRect(47, 236, 31, 31))
        self.label_46.setObjectName(_fromUtf8("label_46"))
        self.label_47 = QtGui.QLabel(self.frame_11)
        self.label_47.setGeometry(QtCore.QRect(47, 266, 31, 31))
        self.label_47.setObjectName(_fromUtf8("label_47"))
        self.label_48 = QtGui.QLabel(self.frame_11)
        self.label_48.setGeometry(QtCore.QRect(47, 296, 31, 31))
        self.label_48.setObjectName(_fromUtf8("label_48"))
        self.label_49 = QtGui.QLabel(self.frame_11)
        self.label_49.setGeometry(QtCore.QRect(100, 360, 151, 31))
        self.label_49.setObjectName(_fromUtf8("label_49"))
        self.label_50 = QtGui.QLabel(self.frame_11)
        self.label_50.setGeometry(QtCore.QRect(90, 390, 161, 21))
        self.label_50.setObjectName(_fromUtf8("label_50"))
        self.plock1_3 = QtGui.QProgressBar(self.frame_11)
        self.plock1_3.setGeometry(QtCore.QRect(170, 120, 21, 23))
        self.plock1_3.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock1_3.setMaximum(100)
        self.plock1_3.setProperty("value", 0)
        self.plock1_3.setTextVisible(False)
        self.plock1_3.setOrientation(QtCore.Qt.Vertical)
        self.plock1_3.setInvertedAppearance(False)
        self.plock1_3.setObjectName(_fromUtf8("plock1_3"))
        self.plock2_3 = QtGui.QProgressBar(self.frame_11)
        self.plock2_3.setGeometry(QtCore.QRect(170, 150, 21, 23))
        self.plock2_3.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock2_3.setMaximum(100)
        self.plock2_3.setProperty("value", 0)
        self.plock2_3.setTextVisible(False)
        self.plock2_3.setOrientation(QtCore.Qt.Vertical)
        self.plock2_3.setInvertedAppearance(False)
        self.plock2_3.setObjectName(_fromUtf8("plock2_3"))
        self.plock3_3 = QtGui.QProgressBar(self.frame_11)
        self.plock3_3.setGeometry(QtCore.QRect(170, 180, 21, 23))
        self.plock3_3.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock3_3.setMaximum(100)
        self.plock3_3.setProperty("value", 0)
        self.plock3_3.setTextVisible(False)
        self.plock3_3.setOrientation(QtCore.Qt.Vertical)
        self.plock3_3.setInvertedAppearance(False)
        self.plock3_3.setObjectName(_fromUtf8("plock3_3"))
        self.plock4_3 = QtGui.QProgressBar(self.frame_11)
        self.plock4_3.setGeometry(QtCore.QRect(170, 210, 21, 23))
        self.plock4_3.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock4_3.setMaximum(100)
        self.plock4_3.setProperty("value", 0)
        self.plock4_3.setTextVisible(False)
        self.plock4_3.setOrientation(QtCore.Qt.Vertical)
        self.plock4_3.setInvertedAppearance(False)
        self.plock4_3.setObjectName(_fromUtf8("plock4_3"))
        self.plock5_3 = QtGui.QProgressBar(self.frame_11)
        self.plock5_3.setGeometry(QtCore.QRect(170, 240, 21, 23))
        self.plock5_3.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock5_3.setMaximum(100)
        self.plock5_3.setProperty("value", 0)
        self.plock5_3.setTextVisible(False)
        self.plock5_3.setOrientation(QtCore.Qt.Vertical)
        self.plock5_3.setInvertedAppearance(False)
        self.plock5_3.setObjectName(_fromUtf8("plock5_3"))
        self.plock6_3 = QtGui.QProgressBar(self.frame_11)
        self.plock6_3.setGeometry(QtCore.QRect(170, 270, 21, 23))
        self.plock6_3.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock6_3.setMaximum(100)
        self.plock6_3.setProperty("value", 0)
        self.plock6_3.setTextVisible(False)
        self.plock6_3.setOrientation(QtCore.Qt.Vertical)
        self.plock6_3.setInvertedAppearance(False)
        self.plock6_3.setObjectName(_fromUtf8("plock6_3"))
        self.plock7_3 = QtGui.QProgressBar(self.frame_11)
        self.plock7_3.setGeometry(QtCore.QRect(170, 300, 21, 23))
        self.plock7_3.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock7_3.setMaximum(100)
        self.plock7_3.setProperty("value", 0)
        self.plock7_3.setTextVisible(False)
        self.plock7_3.setOrientation(QtCore.Qt.Vertical)
        self.plock7_3.setInvertedAppearance(False)
        self.plock7_3.setObjectName(_fromUtf8("plock7_3"))
        self.plock0_3 = QtGui.QProgressBar(self.frame_11)
        self.plock0_3.setGeometry(QtCore.QRect(130, 420, 21, 23))
        self.plock0_3.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock0_3.setMaximum(100)
        self.plock0_3.setProperty("value", 0)
        self.plock0_3.setTextVisible(False)
        self.plock0_3.setOrientation(QtCore.Qt.Vertical)
        self.plock0_3.setInvertedAppearance(False)
        self.plock0_3.setObjectName(_fromUtf8("plock0_3"))
        self.tabWidget_3.addTab(self.tab_7, _fromUtf8(""))
        self.tab_8 = QtGui.QWidget()
        self.tab_8.setObjectName(_fromUtf8("tab_8"))
        self.frame_12 = QtGui.QFrame(self.tab_8)
        self.frame_12.setGeometry(QtCore.QRect(0, 0, 319, 479))
        self.frame_12.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_12.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_12.setObjectName(_fromUtf8("frame_12"))
        self.label_72 = QtGui.QLabel(self.frame_12)
        self.label_72.setGeometry(QtCore.QRect(0, 0, 311, 51))
        self.label_72.setObjectName(_fromUtf8("label_72"))
        self.label_51 = QtGui.QLabel(self.frame_12)
        self.label_51.setGeometry(QtCore.QRect(70, 70, 171, 21))
        self.label_51.setObjectName(_fromUtf8("label_51"))
        self.label_52 = QtGui.QLabel(self.frame_12)
        self.label_52.setGeometry(QtCore.QRect(27, 89, 51, 31))
        self.label_52.setObjectName(_fromUtf8("label_52"))
        self.line_10 = QtGui.QFrame(self.frame_12)
        self.line_10.setGeometry(QtCore.QRect(80, 100, 20, 231))
        self.line_10.setFrameShape(QtGui.QFrame.VLine)
        self.line_10.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_10.setObjectName(_fromUtf8("line_10"))
        self.label_53 = QtGui.QLabel(self.frame_12)
        self.label_53.setGeometry(QtCore.QRect(117, 90, 161, 31))
        self.label_53.setObjectName(_fromUtf8("label_53"))
        self.label_65 = QtGui.QLabel(self.frame_12)
        self.label_65.setGeometry(QtCore.QRect(47, 116, 31, 31))
        self.label_65.setObjectName(_fromUtf8("label_65"))
        self.label_73 = QtGui.QLabel(self.frame_12)
        self.label_73.setGeometry(QtCore.QRect(47, 146, 31, 31))
        self.label_73.setObjectName(_fromUtf8("label_73"))
        self.label_74 = QtGui.QLabel(self.frame_12)
        self.label_74.setGeometry(QtCore.QRect(47, 176, 31, 31))
        self.label_74.setObjectName(_fromUtf8("label_74"))
        self.label_75 = QtGui.QLabel(self.frame_12)
        self.label_75.setGeometry(QtCore.QRect(47, 206, 31, 31))
        self.label_75.setObjectName(_fromUtf8("label_75"))
        self.label_76 = QtGui.QLabel(self.frame_12)
        self.label_76.setGeometry(QtCore.QRect(47, 236, 31, 31))
        self.label_76.setObjectName(_fromUtf8("label_76"))
        self.label_77 = QtGui.QLabel(self.frame_12)
        self.label_77.setGeometry(QtCore.QRect(47, 266, 31, 31))
        self.label_77.setObjectName(_fromUtf8("label_77"))
        self.label_78 = QtGui.QLabel(self.frame_12)
        self.label_78.setGeometry(QtCore.QRect(47, 296, 31, 31))
        self.label_78.setObjectName(_fromUtf8("label_78"))
        self.label_79 = QtGui.QLabel(self.frame_12)
        self.label_79.setGeometry(QtCore.QRect(100, 360, 151, 31))
        self.label_79.setObjectName(_fromUtf8("label_79"))
        self.label_80 = QtGui.QLabel(self.frame_12)
        self.label_80.setGeometry(QtCore.QRect(90, 390, 161, 21))
        self.label_80.setObjectName(_fromUtf8("label_80"))
        self.plock1_4 = QtGui.QProgressBar(self.frame_12)
        self.plock1_4.setGeometry(QtCore.QRect(170, 120, 21, 23))
        self.plock1_4.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock1_4.setMaximum(100)
        self.plock1_4.setProperty("value", 0)
        self.plock1_4.setTextVisible(False)
        self.plock1_4.setOrientation(QtCore.Qt.Vertical)
        self.plock1_4.setInvertedAppearance(False)
        self.plock1_4.setObjectName(_fromUtf8("plock1_4"))
        self.plock2_4 = QtGui.QProgressBar(self.frame_12)
        self.plock2_4.setGeometry(QtCore.QRect(170, 150, 21, 23))
        self.plock2_4.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock2_4.setMaximum(100)
        self.plock2_4.setProperty("value", 0)
        self.plock2_4.setTextVisible(False)
        self.plock2_4.setOrientation(QtCore.Qt.Vertical)
        self.plock2_4.setInvertedAppearance(False)
        self.plock2_4.setObjectName(_fromUtf8("plock2_4"))
        self.plock3_4 = QtGui.QProgressBar(self.frame_12)
        self.plock3_4.setGeometry(QtCore.QRect(170, 180, 21, 23))
        self.plock3_4.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock3_4.setMaximum(100)
        self.plock3_4.setProperty("value", 0)
        self.plock3_4.setTextVisible(False)
        self.plock3_4.setOrientation(QtCore.Qt.Vertical)
        self.plock3_4.setInvertedAppearance(False)
        self.plock3_4.setObjectName(_fromUtf8("plock3_4"))
        self.plock4_4 = QtGui.QProgressBar(self.frame_12)
        self.plock4_4.setGeometry(QtCore.QRect(170, 210, 21, 23))
        self.plock4_4.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock4_4.setMaximum(100)
        self.plock4_4.setProperty("value", 0)
        self.plock4_4.setTextVisible(False)
        self.plock4_4.setOrientation(QtCore.Qt.Vertical)
        self.plock4_4.setInvertedAppearance(False)
        self.plock4_4.setObjectName(_fromUtf8("plock4_4"))
        self.plock5_4 = QtGui.QProgressBar(self.frame_12)
        self.plock5_4.setGeometry(QtCore.QRect(170, 240, 21, 23))
        self.plock5_4.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock5_4.setMaximum(100)
        self.plock5_4.setProperty("value", 0)
        self.plock5_4.setTextVisible(False)
        self.plock5_4.setOrientation(QtCore.Qt.Vertical)
        self.plock5_4.setInvertedAppearance(False)
        self.plock5_4.setObjectName(_fromUtf8("plock5_4"))
        self.plock6_4 = QtGui.QProgressBar(self.frame_12)
        self.plock6_4.setGeometry(QtCore.QRect(170, 270, 21, 23))
        self.plock6_4.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock6_4.setMaximum(100)
        self.plock6_4.setProperty("value", 0)
        self.plock6_4.setTextVisible(False)
        self.plock6_4.setOrientation(QtCore.Qt.Vertical)
        self.plock6_4.setInvertedAppearance(False)
        self.plock6_4.setObjectName(_fromUtf8("plock6_4"))
        self.plock7_4 = QtGui.QProgressBar(self.frame_12)
        self.plock7_4.setGeometry(QtCore.QRect(170, 300, 21, 23))
        self.plock7_4.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock7_4.setMaximum(100)
        self.plock7_4.setProperty("value", 0)
        self.plock7_4.setTextVisible(False)
        self.plock7_4.setOrientation(QtCore.Qt.Vertical)
        self.plock7_4.setInvertedAppearance(False)
        self.plock7_4.setObjectName(_fromUtf8("plock7_4"))
        self.plock0_4 = QtGui.QProgressBar(self.frame_12)
        self.plock0_4.setGeometry(QtCore.QRect(130, 420, 21, 23))
        self.plock0_4.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock0_4.setMaximum(100)
        self.plock0_4.setProperty("value", 0)
        self.plock0_4.setTextVisible(False)
        self.plock0_4.setOrientation(QtCore.Qt.Vertical)
        self.plock0_4.setInvertedAppearance(False)
        self.plock0_4.setObjectName(_fromUtf8("plock0_4"))
        self.tabWidget_3.addTab(self.tab_8, _fromUtf8(""))
        self.tab_9 = QtGui.QWidget()
        self.tab_9.setObjectName(_fromUtf8("tab_9"))
        self.frame_14 = QtGui.QFrame(self.tab_9)
        self.frame_14.setGeometry(QtCore.QRect(0, 0, 319, 479))
        self.frame_14.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_14.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_14.setObjectName(_fromUtf8("frame_14"))
        self.label_81 = QtGui.QLabel(self.frame_14)
        self.label_81.setGeometry(QtCore.QRect(0, 0, 311, 51))
        self.label_81.setObjectName(_fromUtf8("label_81"))
        self.label_82 = QtGui.QLabel(self.frame_14)
        self.label_82.setGeometry(QtCore.QRect(70, 70, 171, 21))
        self.label_82.setObjectName(_fromUtf8("label_82"))
        self.label_83 = QtGui.QLabel(self.frame_14)
        self.label_83.setGeometry(QtCore.QRect(27, 89, 51, 31))
        self.label_83.setObjectName(_fromUtf8("label_83"))
        self.line_14 = QtGui.QFrame(self.frame_14)
        self.line_14.setGeometry(QtCore.QRect(80, 100, 20, 231))
        self.line_14.setFrameShape(QtGui.QFrame.VLine)
        self.line_14.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_14.setObjectName(_fromUtf8("line_14"))
        self.label_84 = QtGui.QLabel(self.frame_14)
        self.label_84.setGeometry(QtCore.QRect(117, 90, 161, 31))
        self.label_84.setObjectName(_fromUtf8("label_84"))
        self.label_85 = QtGui.QLabel(self.frame_14)
        self.label_85.setGeometry(QtCore.QRect(47, 116, 31, 31))
        self.label_85.setObjectName(_fromUtf8("label_85"))
        self.label_86 = QtGui.QLabel(self.frame_14)
        self.label_86.setGeometry(QtCore.QRect(47, 146, 31, 31))
        self.label_86.setObjectName(_fromUtf8("label_86"))
        self.label_87 = QtGui.QLabel(self.frame_14)
        self.label_87.setGeometry(QtCore.QRect(47, 176, 31, 31))
        self.label_87.setObjectName(_fromUtf8("label_87"))
        self.label_88 = QtGui.QLabel(self.frame_14)
        self.label_88.setGeometry(QtCore.QRect(47, 206, 31, 31))
        self.label_88.setObjectName(_fromUtf8("label_88"))
        self.label_89 = QtGui.QLabel(self.frame_14)
        self.label_89.setGeometry(QtCore.QRect(47, 236, 31, 31))
        self.label_89.setObjectName(_fromUtf8("label_89"))
        self.label_90 = QtGui.QLabel(self.frame_14)
        self.label_90.setGeometry(QtCore.QRect(47, 266, 31, 31))
        self.label_90.setObjectName(_fromUtf8("label_90"))
        self.label_91 = QtGui.QLabel(self.frame_14)
        self.label_91.setGeometry(QtCore.QRect(47, 296, 31, 31))
        self.label_91.setObjectName(_fromUtf8("label_91"))
        self.label_92 = QtGui.QLabel(self.frame_14)
        self.label_92.setGeometry(QtCore.QRect(100, 360, 151, 31))
        self.label_92.setObjectName(_fromUtf8("label_92"))
        self.label_93 = QtGui.QLabel(self.frame_14)
        self.label_93.setGeometry(QtCore.QRect(90, 390, 161, 21))
        self.label_93.setObjectName(_fromUtf8("label_93"))
        self.plock1_5 = QtGui.QProgressBar(self.frame_14)
        self.plock1_5.setGeometry(QtCore.QRect(170, 120, 21, 23))
        self.plock1_5.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock1_5.setMaximum(100)
        self.plock1_5.setProperty("value", 0)
        self.plock1_5.setTextVisible(False)
        self.plock1_5.setOrientation(QtCore.Qt.Vertical)
        self.plock1_5.setInvertedAppearance(False)
        self.plock1_5.setObjectName(_fromUtf8("plock1_5"))
        self.plock2_5 = QtGui.QProgressBar(self.frame_14)
        self.plock2_5.setGeometry(QtCore.QRect(170, 150, 21, 23))
        self.plock2_5.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock2_5.setMaximum(100)
        self.plock2_5.setProperty("value", 0)
        self.plock2_5.setTextVisible(False)
        self.plock2_5.setOrientation(QtCore.Qt.Vertical)
        self.plock2_5.setInvertedAppearance(False)
        self.plock2_5.setObjectName(_fromUtf8("plock2_5"))
        self.plock3_5 = QtGui.QProgressBar(self.frame_14)
        self.plock3_5.setGeometry(QtCore.QRect(170, 180, 21, 23))
        self.plock3_5.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock3_5.setMaximum(100)
        self.plock3_5.setProperty("value", 0)
        self.plock3_5.setTextVisible(False)
        self.plock3_5.setOrientation(QtCore.Qt.Vertical)
        self.plock3_5.setInvertedAppearance(False)
        self.plock3_5.setObjectName(_fromUtf8("plock3_5"))
        self.plock4_5 = QtGui.QProgressBar(self.frame_14)
        self.plock4_5.setGeometry(QtCore.QRect(170, 210, 21, 23))
        self.plock4_5.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock4_5.setMaximum(100)
        self.plock4_5.setProperty("value", 0)
        self.plock4_5.setTextVisible(False)
        self.plock4_5.setOrientation(QtCore.Qt.Vertical)
        self.plock4_5.setInvertedAppearance(False)
        self.plock4_5.setObjectName(_fromUtf8("plock4_5"))
        self.plock5_5 = QtGui.QProgressBar(self.frame_14)
        self.plock5_5.setGeometry(QtCore.QRect(170, 240, 21, 23))
        self.plock5_5.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock5_5.setMaximum(100)
        self.plock5_5.setProperty("value", 0)
        self.plock5_5.setTextVisible(False)
        self.plock5_5.setOrientation(QtCore.Qt.Vertical)
        self.plock5_5.setInvertedAppearance(False)
        self.plock5_5.setObjectName(_fromUtf8("plock5_5"))
        self.plock6_5 = QtGui.QProgressBar(self.frame_14)
        self.plock6_5.setGeometry(QtCore.QRect(170, 270, 21, 23))
        self.plock6_5.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock6_5.setMaximum(100)
        self.plock6_5.setProperty("value", 0)
        self.plock6_5.setTextVisible(False)
        self.plock6_5.setOrientation(QtCore.Qt.Vertical)
        self.plock6_5.setInvertedAppearance(False)
        self.plock6_5.setObjectName(_fromUtf8("plock6_5"))
        self.plock7_5 = QtGui.QProgressBar(self.frame_14)
        self.plock7_5.setGeometry(QtCore.QRect(170, 300, 21, 23))
        self.plock7_5.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock7_5.setMaximum(100)
        self.plock7_5.setProperty("value", 0)
        self.plock7_5.setTextVisible(False)
        self.plock7_5.setOrientation(QtCore.Qt.Vertical)
        self.plock7_5.setInvertedAppearance(False)
        self.plock7_5.setObjectName(_fromUtf8("plock7_5"))
        self.plock0_5 = QtGui.QProgressBar(self.frame_14)
        self.plock0_5.setGeometry(QtCore.QRect(130, 420, 21, 23))
        self.plock0_5.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock0_5.setMaximum(100)
        self.plock0_5.setProperty("value", 0)
        self.plock0_5.setTextVisible(False)
        self.plock0_5.setOrientation(QtCore.Qt.Vertical)
        self.plock0_5.setInvertedAppearance(False)
        self.plock0_5.setObjectName(_fromUtf8("plock0_5"))
        self.tabWidget_3.addTab(self.tab_9, _fromUtf8(""))
        self.tab_10 = QtGui.QWidget()
        self.tab_10.setObjectName(_fromUtf8("tab_10"))
        self.frame_15 = QtGui.QFrame(self.tab_10)
        self.frame_15.setGeometry(QtCore.QRect(0, 0, 319, 479))
        self.frame_15.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_15.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_15.setObjectName(_fromUtf8("frame_15"))
        self.label_94 = QtGui.QLabel(self.frame_15)
        self.label_94.setGeometry(QtCore.QRect(0, 0, 311, 51))
        self.label_94.setObjectName(_fromUtf8("label_94"))
        self.label_95 = QtGui.QLabel(self.frame_15)
        self.label_95.setGeometry(QtCore.QRect(70, 70, 171, 21))
        self.label_95.setObjectName(_fromUtf8("label_95"))
        self.label_96 = QtGui.QLabel(self.frame_15)
        self.label_96.setGeometry(QtCore.QRect(27, 89, 51, 31))
        self.label_96.setObjectName(_fromUtf8("label_96"))
        self.line_19 = QtGui.QFrame(self.frame_15)
        self.line_19.setGeometry(QtCore.QRect(80, 100, 20, 231))
        self.line_19.setFrameShape(QtGui.QFrame.VLine)
        self.line_19.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_19.setObjectName(_fromUtf8("line_19"))
        self.label_97 = QtGui.QLabel(self.frame_15)
        self.label_97.setGeometry(QtCore.QRect(117, 90, 161, 31))
        self.label_97.setObjectName(_fromUtf8("label_97"))
        self.label_98 = QtGui.QLabel(self.frame_15)
        self.label_98.setGeometry(QtCore.QRect(47, 116, 31, 31))
        self.label_98.setObjectName(_fromUtf8("label_98"))
        self.label_99 = QtGui.QLabel(self.frame_15)
        self.label_99.setGeometry(QtCore.QRect(47, 146, 31, 31))
        self.label_99.setObjectName(_fromUtf8("label_99"))
        self.label_100 = QtGui.QLabel(self.frame_15)
        self.label_100.setGeometry(QtCore.QRect(47, 176, 31, 31))
        self.label_100.setObjectName(_fromUtf8("label_100"))
        self.label_101 = QtGui.QLabel(self.frame_15)
        self.label_101.setGeometry(QtCore.QRect(47, 206, 31, 31))
        self.label_101.setObjectName(_fromUtf8("label_101"))
        self.label_102 = QtGui.QLabel(self.frame_15)
        self.label_102.setGeometry(QtCore.QRect(47, 236, 31, 31))
        self.label_102.setObjectName(_fromUtf8("label_102"))
        self.label_103 = QtGui.QLabel(self.frame_15)
        self.label_103.setGeometry(QtCore.QRect(47, 266, 31, 31))
        self.label_103.setObjectName(_fromUtf8("label_103"))
        self.label_104 = QtGui.QLabel(self.frame_15)
        self.label_104.setGeometry(QtCore.QRect(47, 296, 31, 31))
        self.label_104.setObjectName(_fromUtf8("label_104"))
        self.label_105 = QtGui.QLabel(self.frame_15)
        self.label_105.setGeometry(QtCore.QRect(100, 360, 151, 31))
        self.label_105.setObjectName(_fromUtf8("label_105"))
        self.label_106 = QtGui.QLabel(self.frame_15)
        self.label_106.setGeometry(QtCore.QRect(90, 390, 161, 21))
        self.label_106.setObjectName(_fromUtf8("label_106"))
        self.plock1_6 = QtGui.QProgressBar(self.frame_15)
        self.plock1_6.setGeometry(QtCore.QRect(170, 120, 21, 23))
        self.plock1_6.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock1_6.setMaximum(100)
        self.plock1_6.setProperty("value", 0)
        self.plock1_6.setTextVisible(False)
        self.plock1_6.setOrientation(QtCore.Qt.Vertical)
        self.plock1_6.setInvertedAppearance(False)
        self.plock1_6.setObjectName(_fromUtf8("plock1_6"))
        self.plock2_6 = QtGui.QProgressBar(self.frame_15)
        self.plock2_6.setGeometry(QtCore.QRect(170, 150, 21, 23))
        self.plock2_6.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock2_6.setMaximum(100)
        self.plock2_6.setProperty("value", 0)
        self.plock2_6.setTextVisible(False)
        self.plock2_6.setOrientation(QtCore.Qt.Vertical)
        self.plock2_6.setInvertedAppearance(False)
        self.plock2_6.setObjectName(_fromUtf8("plock2_6"))
        self.plock3_6 = QtGui.QProgressBar(self.frame_15)
        self.plock3_6.setGeometry(QtCore.QRect(170, 180, 21, 23))
        self.plock3_6.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock3_6.setMaximum(100)
        self.plock3_6.setProperty("value", 0)
        self.plock3_6.setTextVisible(False)
        self.plock3_6.setOrientation(QtCore.Qt.Vertical)
        self.plock3_6.setInvertedAppearance(False)
        self.plock3_6.setObjectName(_fromUtf8("plock3_6"))
        self.plock4_6 = QtGui.QProgressBar(self.frame_15)
        self.plock4_6.setGeometry(QtCore.QRect(170, 210, 21, 23))
        self.plock4_6.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock4_6.setMaximum(100)
        self.plock4_6.setProperty("value", 0)
        self.plock4_6.setTextVisible(False)
        self.plock4_6.setOrientation(QtCore.Qt.Vertical)
        self.plock4_6.setInvertedAppearance(False)
        self.plock4_6.setObjectName(_fromUtf8("plock4_6"))
        self.plock5_6 = QtGui.QProgressBar(self.frame_15)
        self.plock5_6.setGeometry(QtCore.QRect(170, 240, 21, 23))
        self.plock5_6.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock5_6.setMaximum(100)
        self.plock5_6.setProperty("value", 0)
        self.plock5_6.setTextVisible(False)
        self.plock5_6.setOrientation(QtCore.Qt.Vertical)
        self.plock5_6.setInvertedAppearance(False)
        self.plock5_6.setObjectName(_fromUtf8("plock5_6"))
        self.plock6_6 = QtGui.QProgressBar(self.frame_15)
        self.plock6_6.setGeometry(QtCore.QRect(170, 270, 21, 23))
        self.plock6_6.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock6_6.setMaximum(100)
        self.plock6_6.setProperty("value", 0)
        self.plock6_6.setTextVisible(False)
        self.plock6_6.setOrientation(QtCore.Qt.Vertical)
        self.plock6_6.setInvertedAppearance(False)
        self.plock6_6.setObjectName(_fromUtf8("plock6_6"))
        self.plock7_6 = QtGui.QProgressBar(self.frame_15)
        self.plock7_6.setGeometry(QtCore.QRect(170, 300, 21, 23))
        self.plock7_6.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock7_6.setMaximum(100)
        self.plock7_6.setProperty("value", 0)
        self.plock7_6.setTextVisible(False)
        self.plock7_6.setOrientation(QtCore.Qt.Vertical)
        self.plock7_6.setInvertedAppearance(False)
        self.plock7_6.setObjectName(_fromUtf8("plock7_6"))
        self.plock0_6 = QtGui.QProgressBar(self.frame_15)
        self.plock0_6.setGeometry(QtCore.QRect(130, 420, 21, 23))
        self.plock0_6.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock0_6.setMaximum(100)
        self.plock0_6.setProperty("value", 0)
        self.plock0_6.setTextVisible(False)
        self.plock0_6.setOrientation(QtCore.Qt.Vertical)
        self.plock0_6.setInvertedAppearance(False)
        self.plock0_6.setObjectName(_fromUtf8("plock0_6"))
        self.tabWidget_3.addTab(self.tab_10, _fromUtf8(""))
        self.tab_11 = QtGui.QWidget()
        self.tab_11.setObjectName(_fromUtf8("tab_11"))
        self.frame_16 = QtGui.QFrame(self.tab_11)
        self.frame_16.setGeometry(QtCore.QRect(0, 0, 319, 479))
        self.frame_16.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_16.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_16.setObjectName(_fromUtf8("frame_16"))
        self.label_107 = QtGui.QLabel(self.frame_16)
        self.label_107.setGeometry(QtCore.QRect(0, 0, 311, 51))
        self.label_107.setObjectName(_fromUtf8("label_107"))
        self.label_108 = QtGui.QLabel(self.frame_16)
        self.label_108.setGeometry(QtCore.QRect(70, 70, 171, 21))
        self.label_108.setObjectName(_fromUtf8("label_108"))
        self.label_109 = QtGui.QLabel(self.frame_16)
        self.label_109.setGeometry(QtCore.QRect(27, 89, 51, 31))
        self.label_109.setObjectName(_fromUtf8("label_109"))
        self.line_20 = QtGui.QFrame(self.frame_16)
        self.line_20.setGeometry(QtCore.QRect(80, 100, 20, 231))
        self.line_20.setFrameShape(QtGui.QFrame.VLine)
        self.line_20.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_20.setObjectName(_fromUtf8("line_20"))
        self.label_110 = QtGui.QLabel(self.frame_16)
        self.label_110.setGeometry(QtCore.QRect(117, 90, 161, 31))
        self.label_110.setObjectName(_fromUtf8("label_110"))
        self.label_111 = QtGui.QLabel(self.frame_16)
        self.label_111.setGeometry(QtCore.QRect(47, 116, 31, 31))
        self.label_111.setObjectName(_fromUtf8("label_111"))
        self.label_112 = QtGui.QLabel(self.frame_16)
        self.label_112.setGeometry(QtCore.QRect(47, 146, 31, 31))
        self.label_112.setObjectName(_fromUtf8("label_112"))
        self.label_113 = QtGui.QLabel(self.frame_16)
        self.label_113.setGeometry(QtCore.QRect(47, 176, 31, 31))
        self.label_113.setObjectName(_fromUtf8("label_113"))
        self.label_114 = QtGui.QLabel(self.frame_16)
        self.label_114.setGeometry(QtCore.QRect(47, 206, 31, 31))
        self.label_114.setObjectName(_fromUtf8("label_114"))
        self.label_115 = QtGui.QLabel(self.frame_16)
        self.label_115.setGeometry(QtCore.QRect(47, 236, 31, 31))
        self.label_115.setObjectName(_fromUtf8("label_115"))
        self.label_116 = QtGui.QLabel(self.frame_16)
        self.label_116.setGeometry(QtCore.QRect(47, 266, 31, 31))
        self.label_116.setObjectName(_fromUtf8("label_116"))
        self.label_117 = QtGui.QLabel(self.frame_16)
        self.label_117.setGeometry(QtCore.QRect(47, 296, 31, 31))
        self.label_117.setObjectName(_fromUtf8("label_117"))
        self.label_118 = QtGui.QLabel(self.frame_16)
        self.label_118.setGeometry(QtCore.QRect(100, 360, 151, 31))
        self.label_118.setObjectName(_fromUtf8("label_118"))
        self.label_119 = QtGui.QLabel(self.frame_16)
        self.label_119.setGeometry(QtCore.QRect(90, 390, 161, 21))
        self.label_119.setObjectName(_fromUtf8("label_119"))
        self.plock1_7 = QtGui.QProgressBar(self.frame_16)
        self.plock1_7.setGeometry(QtCore.QRect(170, 120, 21, 23))
        self.plock1_7.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock1_7.setMaximum(100)
        self.plock1_7.setProperty("value", 0)
        self.plock1_7.setTextVisible(False)
        self.plock1_7.setOrientation(QtCore.Qt.Vertical)
        self.plock1_7.setInvertedAppearance(False)
        self.plock1_7.setObjectName(_fromUtf8("plock1_7"))
        self.plock2_7 = QtGui.QProgressBar(self.frame_16)
        self.plock2_7.setGeometry(QtCore.QRect(170, 150, 21, 23))
        self.plock2_7.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock2_7.setMaximum(100)
        self.plock2_7.setProperty("value", 0)
        self.plock2_7.setTextVisible(False)
        self.plock2_7.setOrientation(QtCore.Qt.Vertical)
        self.plock2_7.setInvertedAppearance(False)
        self.plock2_7.setObjectName(_fromUtf8("plock2_7"))
        self.plock3_7 = QtGui.QProgressBar(self.frame_16)
        self.plock3_7.setGeometry(QtCore.QRect(170, 180, 21, 23))
        self.plock3_7.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock3_7.setMaximum(100)
        self.plock3_7.setProperty("value", 0)
        self.plock3_7.setTextVisible(False)
        self.plock3_7.setOrientation(QtCore.Qt.Vertical)
        self.plock3_7.setInvertedAppearance(False)
        self.plock3_7.setObjectName(_fromUtf8("plock3_7"))
        self.plock4_7 = QtGui.QProgressBar(self.frame_16)
        self.plock4_7.setGeometry(QtCore.QRect(170, 210, 21, 23))
        self.plock4_7.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock4_7.setMaximum(100)
        self.plock4_7.setProperty("value", 0)
        self.plock4_7.setTextVisible(False)
        self.plock4_7.setOrientation(QtCore.Qt.Vertical)
        self.plock4_7.setInvertedAppearance(False)
        self.plock4_7.setObjectName(_fromUtf8("plock4_7"))
        self.plock5_7 = QtGui.QProgressBar(self.frame_16)
        self.plock5_7.setGeometry(QtCore.QRect(170, 240, 21, 23))
        self.plock5_7.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock5_7.setMaximum(100)
        self.plock5_7.setProperty("value", 0)
        self.plock5_7.setTextVisible(False)
        self.plock5_7.setOrientation(QtCore.Qt.Vertical)
        self.plock5_7.setInvertedAppearance(False)
        self.plock5_7.setObjectName(_fromUtf8("plock5_7"))
        self.plock6_7 = QtGui.QProgressBar(self.frame_16)
        self.plock6_7.setGeometry(QtCore.QRect(170, 270, 21, 23))
        self.plock6_7.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock6_7.setMaximum(100)
        self.plock6_7.setProperty("value", 0)
        self.plock6_7.setTextVisible(False)
        self.plock6_7.setOrientation(QtCore.Qt.Vertical)
        self.plock6_7.setInvertedAppearance(False)
        self.plock6_7.setObjectName(_fromUtf8("plock6_7"))
        self.plock7_7 = QtGui.QProgressBar(self.frame_16)
        self.plock7_7.setGeometry(QtCore.QRect(170, 300, 21, 23))
        self.plock7_7.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock7_7.setMaximum(100)
        self.plock7_7.setProperty("value", 0)
        self.plock7_7.setTextVisible(False)
        self.plock7_7.setOrientation(QtCore.Qt.Vertical)
        self.plock7_7.setInvertedAppearance(False)
        self.plock7_7.setObjectName(_fromUtf8("plock7_7"))
        self.plock0_7 = QtGui.QProgressBar(self.frame_16)
        self.plock0_7.setGeometry(QtCore.QRect(130, 420, 21, 23))
        self.plock0_7.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock0_7.setMaximum(100)
        self.plock0_7.setProperty("value", 0)
        self.plock0_7.setTextVisible(False)
        self.plock0_7.setOrientation(QtCore.Qt.Vertical)
        self.plock0_7.setInvertedAppearance(False)
        self.plock0_7.setObjectName(_fromUtf8("plock0_7"))
        self.tabWidget_3.addTab(self.tab_11, _fromUtf8(""))
        self.tab_12 = QtGui.QWidget()
        self.tab_12.setObjectName(_fromUtf8("tab_12"))
        self.frame_17 = QtGui.QFrame(self.tab_12)
        self.frame_17.setGeometry(QtCore.QRect(0, 0, 319, 479))
        self.frame_17.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_17.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_17.setObjectName(_fromUtf8("frame_17"))
        self.label_120 = QtGui.QLabel(self.frame_17)
        self.label_120.setGeometry(QtCore.QRect(0, 0, 311, 51))
        self.label_120.setObjectName(_fromUtf8("label_120"))
        self.label_121 = QtGui.QLabel(self.frame_17)
        self.label_121.setGeometry(QtCore.QRect(70, 70, 171, 21))
        self.label_121.setObjectName(_fromUtf8("label_121"))
        self.label_122 = QtGui.QLabel(self.frame_17)
        self.label_122.setGeometry(QtCore.QRect(27, 89, 51, 31))
        self.label_122.setObjectName(_fromUtf8("label_122"))
        self.line_21 = QtGui.QFrame(self.frame_17)
        self.line_21.setGeometry(QtCore.QRect(80, 100, 20, 231))
        self.line_21.setFrameShape(QtGui.QFrame.VLine)
        self.line_21.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_21.setObjectName(_fromUtf8("line_21"))
        self.label_123 = QtGui.QLabel(self.frame_17)
        self.label_123.setGeometry(QtCore.QRect(117, 90, 161, 31))
        self.label_123.setObjectName(_fromUtf8("label_123"))
        self.label_124 = QtGui.QLabel(self.frame_17)
        self.label_124.setGeometry(QtCore.QRect(47, 116, 31, 31))
        self.label_124.setObjectName(_fromUtf8("label_124"))
        self.label_125 = QtGui.QLabel(self.frame_17)
        self.label_125.setGeometry(QtCore.QRect(47, 146, 31, 31))
        self.label_125.setObjectName(_fromUtf8("label_125"))
        self.label_126 = QtGui.QLabel(self.frame_17)
        self.label_126.setGeometry(QtCore.QRect(47, 176, 31, 31))
        self.label_126.setObjectName(_fromUtf8("label_126"))
        self.label_127 = QtGui.QLabel(self.frame_17)
        self.label_127.setGeometry(QtCore.QRect(47, 206, 31, 31))
        self.label_127.setObjectName(_fromUtf8("label_127"))
        self.label_128 = QtGui.QLabel(self.frame_17)
        self.label_128.setGeometry(QtCore.QRect(47, 236, 31, 31))
        self.label_128.setObjectName(_fromUtf8("label_128"))
        self.label_129 = QtGui.QLabel(self.frame_17)
        self.label_129.setGeometry(QtCore.QRect(47, 266, 31, 31))
        self.label_129.setObjectName(_fromUtf8("label_129"))
        self.label_130 = QtGui.QLabel(self.frame_17)
        self.label_130.setGeometry(QtCore.QRect(47, 296, 31, 31))
        self.label_130.setObjectName(_fromUtf8("label_130"))
        self.label_131 = QtGui.QLabel(self.frame_17)
        self.label_131.setGeometry(QtCore.QRect(100, 360, 151, 31))
        self.label_131.setObjectName(_fromUtf8("label_131"))
        self.label_132 = QtGui.QLabel(self.frame_17)
        self.label_132.setGeometry(QtCore.QRect(90, 390, 161, 21))
        self.label_132.setObjectName(_fromUtf8("label_132"))
        self.plock1_8 = QtGui.QProgressBar(self.frame_17)
        self.plock1_8.setGeometry(QtCore.QRect(170, 120, 21, 23))
        self.plock1_8.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock1_8.setMaximum(100)
        self.plock1_8.setProperty("value", 0)
        self.plock1_8.setTextVisible(False)
        self.plock1_8.setOrientation(QtCore.Qt.Vertical)
        self.plock1_8.setInvertedAppearance(False)
        self.plock1_8.setObjectName(_fromUtf8("plock1_8"))
        self.plock2_8 = QtGui.QProgressBar(self.frame_17)
        self.plock2_8.setGeometry(QtCore.QRect(170, 150, 21, 23))
        self.plock2_8.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock2_8.setMaximum(100)
        self.plock2_8.setProperty("value", 0)
        self.plock2_8.setTextVisible(False)
        self.plock2_8.setOrientation(QtCore.Qt.Vertical)
        self.plock2_8.setInvertedAppearance(False)
        self.plock2_8.setObjectName(_fromUtf8("plock2_8"))
        self.plock3_8 = QtGui.QProgressBar(self.frame_17)
        self.plock3_8.setGeometry(QtCore.QRect(170, 180, 21, 23))
        self.plock3_8.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock3_8.setMaximum(100)
        self.plock3_8.setProperty("value", 0)
        self.plock3_8.setTextVisible(False)
        self.plock3_8.setOrientation(QtCore.Qt.Vertical)
        self.plock3_8.setInvertedAppearance(False)
        self.plock3_8.setObjectName(_fromUtf8("plock3_8"))
        self.plock4_8 = QtGui.QProgressBar(self.frame_17)
        self.plock4_8.setGeometry(QtCore.QRect(170, 210, 21, 23))
        self.plock4_8.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock4_8.setMaximum(100)
        self.plock4_8.setProperty("value", 0)
        self.plock4_8.setTextVisible(False)
        self.plock4_8.setOrientation(QtCore.Qt.Vertical)
        self.plock4_8.setInvertedAppearance(False)
        self.plock4_8.setObjectName(_fromUtf8("plock4_8"))
        self.plock5_8 = QtGui.QProgressBar(self.frame_17)
        self.plock5_8.setGeometry(QtCore.QRect(170, 240, 21, 23))
        self.plock5_8.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock5_8.setMaximum(100)
        self.plock5_8.setProperty("value", 0)
        self.plock5_8.setTextVisible(False)
        self.plock5_8.setOrientation(QtCore.Qt.Vertical)
        self.plock5_8.setInvertedAppearance(False)
        self.plock5_8.setObjectName(_fromUtf8("plock5_8"))
        self.plock6_8 = QtGui.QProgressBar(self.frame_17)
        self.plock6_8.setGeometry(QtCore.QRect(170, 270, 21, 23))
        self.plock6_8.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock6_8.setMaximum(100)
        self.plock6_8.setProperty("value", 0)
        self.plock6_8.setTextVisible(False)
        self.plock6_8.setOrientation(QtCore.Qt.Vertical)
        self.plock6_8.setInvertedAppearance(False)
        self.plock6_8.setObjectName(_fromUtf8("plock6_8"))
        self.plock7_8 = QtGui.QProgressBar(self.frame_17)
        self.plock7_8.setGeometry(QtCore.QRect(170, 300, 21, 23))
        self.plock7_8.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock7_8.setMaximum(100)
        self.plock7_8.setProperty("value", 0)
        self.plock7_8.setTextVisible(False)
        self.plock7_8.setOrientation(QtCore.Qt.Vertical)
        self.plock7_8.setInvertedAppearance(False)
        self.plock7_8.setObjectName(_fromUtf8("plock7_8"))
        self.plock0_8 = QtGui.QProgressBar(self.frame_17)
        self.plock0_8.setGeometry(QtCore.QRect(130, 420, 21, 23))
        self.plock0_8.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.plock0_8.setMaximum(100)
        self.plock0_8.setProperty("value", 0)
        self.plock0_8.setTextVisible(False)
        self.plock0_8.setOrientation(QtCore.Qt.Vertical)
        self.plock0_8.setInvertedAppearance(False)
        self.plock0_8.setObjectName(_fromUtf8("plock0_8"))
        self.tabWidget_3.addTab(self.tab_12, _fromUtf8(""))
        self.verticalLayout_9.addWidget(self.tabWidget_3)
        self.verticalLayout_5.addWidget(self.frame_4)
        self.verticalLayoutWidget_3 = QtGui.QWidget(self.tab)
        self.verticalLayoutWidget_3.setGeometry(QtCore.QRect(0, 90, 431, 381))
        self.verticalLayoutWidget_3.setObjectName(_fromUtf8("verticalLayoutWidget_3"))
        self.verticalLayout_3 = QtGui.QVBoxLayout(self.verticalLayoutWidget_3)
        self.verticalLayout_3.setMargin(0)
        self.verticalLayout_3.setObjectName(_fromUtf8("verticalLayout_3"))
        self.frame_2 = QtGui.QFrame(self.verticalLayoutWidget_3)
        self.frame_2.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_2.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_2.setObjectName(_fromUtf8("frame_2"))
        self.feb7 = QtGui.QLabel(self.frame_2)
        self.feb7.setGeometry(QtCore.QRect(50, 320, 151, 20))
        self.feb7.setText(_fromUtf8(""))
        self.feb7.setObjectName(_fromUtf8("feb7"))
        self.feb3 = QtGui.QLabel(self.frame_2)
        self.feb3.setGeometry(QtCore.QRect(50, 200, 151, 20))
        self.feb3.setText(_fromUtf8(""))
        self.feb3.setObjectName(_fromUtf8("feb3"))
        self.febadd_label = QtGui.QLabel(self.frame_2)
        self.febadd_label.setGeometry(QtCore.QRect(110, 80, 31, 21))
        self.febadd_label.setObjectName(_fromUtf8("febadd_label"))
        self.feb1 = QtGui.QLabel(self.frame_2)
        self.feb1.setGeometry(QtCore.QRect(50, 140, 151, 20))
        self.feb1.setText(_fromUtf8(""))
        self.feb1.setObjectName(_fromUtf8("feb1"))
        self.elff = QtGui.QLabel(self.frame_2)
        self.elff.setGeometry(QtCore.QRect(10, 200, 21, 17))
        self.elff.setObjectName(_fromUtf8("elff"))
        self.sca_fib2 = QtGui.QLabel(self.frame_2)
        self.sca_fib2.setGeometry(QtCore.QRect(220, 170, 51, 17))
        self.sca_fib2.setText(_fromUtf8(""))
        self.sca_fib2.setObjectName(_fromUtf8("sca_fib2"))
        self.feb4 = QtGui.QLabel(self.frame_2)
        self.feb4.setGeometry(QtCore.QRect(50, 230, 151, 20))
        self.feb4.setText(_fromUtf8(""))
        self.feb4.setObjectName(_fromUtf8("feb4"))
        self.elbf = QtGui.QLabel(self.frame_2)
        self.elbf.setGeometry(QtCore.QRect(10, 170, 21, 17))
        self.elbf.setObjectName(_fromUtf8("elbf"))
        self.sca_fib0 = QtGui.QLabel(self.frame_2)
        self.sca_fib0.setGeometry(QtCore.QRect(220, 110, 51, 17))
        self.sca_fib0.setText(_fromUtf8(""))
        self.sca_fib0.setObjectName(_fromUtf8("sca_fib0"))
        self.sca_fib6 = QtGui.QLabel(self.frame_2)
        self.sca_fib6.setGeometry(QtCore.QRect(220, 290, 51, 17))
        self.sca_fib6.setText(_fromUtf8(""))
        self.sca_fib6.setObjectName(_fromUtf8("sca_fib6"))
        self.serial4 = QtGui.QLabel(self.frame_2)
        self.serial4.setGeometry(QtCore.QRect(290, 230, 67, 17))
        self.serial4.setText(_fromUtf8(""))
        self.serial4.setObjectName(_fromUtf8("serial4"))
        self.estat6 = QtGui.QProgressBar(self.frame_2)
        self.estat6.setGeometry(QtCore.QRect(380, 290, 21, 23))
        self.estat6.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat6.setMaximum(100)
        self.estat6.setProperty("value", 0)
        self.estat6.setTextVisible(False)
        self.estat6.setOrientation(QtCore.Qt.Vertical)
        self.estat6.setInvertedAppearance(False)
        self.estat6.setObjectName(_fromUtf8("estat6"))
        self.serial1 = QtGui.QLabel(self.frame_2)
        self.serial1.setGeometry(QtCore.QRect(290, 140, 67, 17))
        self.serial1.setText(_fromUtf8(""))
        self.serial1.setObjectName(_fromUtf8("serial1"))
        self.estat4 = QtGui.QProgressBar(self.frame_2)
        self.estat4.setGeometry(QtCore.QRect(380, 230, 21, 23))
        self.estat4.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat4.setMaximum(100)
        self.estat4.setProperty("value", 0)
        self.estat4.setTextVisible(False)
        self.estat4.setOrientation(QtCore.Qt.Vertical)
        self.estat4.setInvertedAppearance(False)
        self.estat4.setObjectName(_fromUtf8("estat4"))
        self.feb5 = QtGui.QLabel(self.frame_2)
        self.feb5.setGeometry(QtCore.QRect(50, 260, 151, 20))
        self.feb5.setText(_fromUtf8(""))
        self.feb5.setObjectName(_fromUtf8("feb5"))
        self.serial6 = QtGui.QLabel(self.frame_2)
        self.serial6.setGeometry(QtCore.QRect(290, 290, 67, 17))
        self.serial6.setText(_fromUtf8(""))
        self.serial6.setObjectName(_fromUtf8("serial6"))
        self.serial2 = QtGui.QLabel(self.frame_2)
        self.serial2.setGeometry(QtCore.QRect(290, 170, 67, 17))
        self.serial2.setText(_fromUtf8(""))
        self.serial2.setObjectName(_fromUtf8("serial2"))
        self.estat3 = QtGui.QProgressBar(self.frame_2)
        self.estat3.setGeometry(QtCore.QRect(380, 200, 21, 23))
        self.estat3.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat3.setMaximum(100)
        self.estat3.setProperty("value", 0)
        self.estat3.setTextVisible(False)
        self.estat3.setOrientation(QtCore.Qt.Vertical)
        self.estat3.setInvertedAppearance(False)
        self.estat3.setObjectName(_fromUtf8("estat3"))
        self.el7f = QtGui.QLabel(self.frame_2)
        self.el7f.setGeometry(QtCore.QRect(10, 140, 21, 17))
        self.el7f.setObjectName(_fromUtf8("el7f"))
        self.pingbtn = QtGui.QPushButton(self.frame_2)
        self.pingbtn.setGeometry(QtCore.QRect(10, 40, 91, 21))
        self.pingbtn.setObjectName(_fromUtf8("pingbtn"))
        self.scalabel = QtGui.QLabel(self.frame_2)
        self.scalabel.setGeometry(QtCore.QRect(50, 0, 321, 41))
        self.scalabel.setObjectName(_fromUtf8("scalabel"))
        self.feb6 = QtGui.QLabel(self.frame_2)
        self.feb6.setGeometry(QtCore.QRect(50, 290, 151, 20))
        self.feb6.setText(_fromUtf8(""))
        self.feb6.setObjectName(_fromUtf8("feb6"))
        self.estat1 = QtGui.QProgressBar(self.frame_2)
        self.estat1.setGeometry(QtCore.QRect(380, 140, 21, 23))
        self.estat1.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat1.setMaximum(100)
        self.estat1.setProperty("value", 0)
        self.estat1.setTextVisible(False)
        self.estat1.setOrientation(QtCore.Qt.Vertical)
        self.estat1.setInvertedAppearance(False)
        self.estat1.setObjectName(_fromUtf8("estat1"))
        self.el17f = QtGui.QLabel(self.frame_2)
        self.el17f.setGeometry(QtCore.QRect(10, 260, 21, 17))
        self.el17f.setObjectName(_fromUtf8("el17f"))
        self.sca_fib1 = QtGui.QLabel(self.frame_2)
        self.sca_fib1.setGeometry(QtCore.QRect(220, 140, 51, 17))
        self.sca_fib1.setText(_fromUtf8(""))
        self.sca_fib1.setObjectName(_fromUtf8("sca_fib1"))
        self.line_12 = QtGui.QFrame(self.frame_2)
        self.line_12.setGeometry(QtCore.QRect(200, 110, 20, 231))
        self.line_12.setFrameShape(QtGui.QFrame.VLine)
        self.line_12.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_12.setObjectName(_fromUtf8("line_12"))
        self.elink_label = QtGui.QLabel(self.frame_2)
        self.elink_label.setGeometry(QtCore.QRect(0, 80, 41, 21))
        self.elink_label.setObjectName(_fromUtf8("elink_label"))
        self.estat0 = QtGui.QProgressBar(self.frame_2)
        self.estat0.setGeometry(QtCore.QRect(380, 110, 21, 23))
        self.estat0.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat0.setMaximum(100)
        self.estat0.setProperty("value", 0)
        self.estat0.setTextVisible(False)
        self.estat0.setOrientation(QtCore.Qt.Vertical)
        self.estat0.setInvertedAppearance(False)
        self.estat0.setObjectName(_fromUtf8("estat0"))
        self.sca_fib4 = QtGui.QLabel(self.frame_2)
        self.sca_fib4.setGeometry(QtCore.QRect(220, 230, 51, 17))
        self.sca_fib4.setText(_fromUtf8(""))
        self.sca_fib4.setObjectName(_fromUtf8("sca_fib4"))
        self.estat7 = QtGui.QProgressBar(self.frame_2)
        self.estat7.setGeometry(QtCore.QRect(380, 320, 21, 23))
        self.estat7.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat7.setMaximum(100)
        self.estat7.setProperty("value", 0)
        self.estat7.setTextVisible(False)
        self.estat7.setOrientation(QtCore.Qt.Vertical)
        self.estat7.setInvertedAppearance(False)
        self.estat7.setObjectName(_fromUtf8("estat7"))
        self.line_2 = QtGui.QFrame(self.frame_2)
        self.line_2.setGeometry(QtCore.QRect(350, 110, 20, 231))
        self.line_2.setFrameShape(QtGui.QFrame.VLine)
        self.line_2.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_2.setObjectName(_fromUtf8("line_2"))
        self.el1ff = QtGui.QLabel(self.frame_2)
        self.el1ff.setGeometry(QtCore.QRect(10, 320, 21, 16))
        self.el1ff.setObjectName(_fromUtf8("el1ff"))
        self.feb0 = QtGui.QLabel(self.frame_2)
        self.feb0.setGeometry(QtCore.QRect(50, 110, 151, 20))
        self.feb0.setText(_fromUtf8(""))
        self.feb0.setObjectName(_fromUtf8("feb0"))
        self.sernbr_label = QtGui.QLabel(self.frame_2)
        self.sernbr_label.setGeometry(QtCore.QRect(300, 80, 51, 21))
        self.sernbr_label.setObjectName(_fromUtf8("sernbr_label"))
        self.estat5 = QtGui.QProgressBar(self.frame_2)
        self.estat5.setGeometry(QtCore.QRect(380, 260, 21, 23))
        self.estat5.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat5.setMaximum(100)
        self.estat5.setProperty("value", 0)
        self.estat5.setTextVisible(False)
        self.estat5.setOrientation(QtCore.Qt.Vertical)
        self.estat5.setInvertedAppearance(False)
        self.estat5.setObjectName(_fromUtf8("estat5"))
        self.line_13 = QtGui.QFrame(self.frame_2)
        self.line_13.setGeometry(QtCore.QRect(270, 110, 20, 231))
        self.line_13.setFrameShape(QtGui.QFrame.VLine)
        self.line_13.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_13.setObjectName(_fromUtf8("line_13"))
        self.febadd_label_3 = QtGui.QLabel(self.frame_2)
        self.febadd_label_3.setGeometry(QtCore.QRect(370, 80, 51, 21))
        self.febadd_label_3.setObjectName(_fromUtf8("febadd_label_3"))
        self.serial0 = QtGui.QLabel(self.frame_2)
        self.serial0.setGeometry(QtCore.QRect(290, 110, 71, 17))
        self.serial0.setText(_fromUtf8(""))
        self.serial0.setObjectName(_fromUtf8("serial0"))
        self.sca_fib7 = QtGui.QLabel(self.frame_2)
        self.sca_fib7.setGeometry(QtCore.QRect(220, 320, 51, 17))
        self.sca_fib7.setText(_fromUtf8(""))
        self.sca_fib7.setObjectName(_fromUtf8("sca_fib7"))
        self.estat2 = QtGui.QProgressBar(self.frame_2)
        self.estat2.setGeometry(QtCore.QRect(380, 170, 21, 23))
        self.estat2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat2.setMaximum(100)
        self.estat2.setProperty("value", 0)
        self.estat2.setTextVisible(False)
        self.estat2.setOrientation(QtCore.Qt.Vertical)
        self.estat2.setInvertedAppearance(False)
        self.estat2.setObjectName(_fromUtf8("estat2"))
        self.pingprogress = QtGui.QProgressBar(self.frame_2)
        self.pingprogress.setGeometry(QtCore.QRect(110, 40, 291, 21))
        self.pingprogress.setProperty("value", 0)
        self.pingprogress.setObjectName(_fromUtf8("pingprogress"))
        self.el1bf = QtGui.QLabel(self.frame_2)
        self.el1bf.setGeometry(QtCore.QRect(10, 290, 31, 17))
        self.el1bf.setObjectName(_fromUtf8("el1bf"))
        self.line = QtGui.QFrame(self.frame_2)
        self.line.setGeometry(QtCore.QRect(30, 110, 20, 231))
        self.line.setFrameShape(QtGui.QFrame.VLine)
        self.line.setFrameShadow(QtGui.QFrame.Sunken)
        self.line.setObjectName(_fromUtf8("line"))
        self.febadd_label_2 = QtGui.QLabel(self.frame_2)
        self.febadd_label_2.setGeometry(QtCore.QRect(220, 80, 51, 21))
        self.febadd_label_2.setObjectName(_fromUtf8("febadd_label_2"))
        self.sca_fib5 = QtGui.QLabel(self.frame_2)
        self.sca_fib5.setGeometry(QtCore.QRect(220, 260, 51, 17))
        self.sca_fib5.setText(_fromUtf8(""))
        self.sca_fib5.setObjectName(_fromUtf8("sca_fib5"))
        self.feb2 = QtGui.QLabel(self.frame_2)
        self.feb2.setGeometry(QtCore.QRect(50, 170, 151, 20))
        self.feb2.setText(_fromUtf8(""))
        self.feb2.setObjectName(_fromUtf8("feb2"))
        self.serial5 = QtGui.QLabel(self.frame_2)
        self.serial5.setGeometry(QtCore.QRect(290, 260, 67, 17))
        self.serial5.setText(_fromUtf8(""))
        self.serial5.setObjectName(_fromUtf8("serial5"))
        self.serial7 = QtGui.QLabel(self.frame_2)
        self.serial7.setGeometry(QtCore.QRect(290, 320, 67, 17))
        self.serial7.setText(_fromUtf8(""))
        self.serial7.setObjectName(_fromUtf8("serial7"))
        self.serial3 = QtGui.QLabel(self.frame_2)
        self.serial3.setGeometry(QtCore.QRect(290, 200, 67, 17))
        self.serial3.setText(_fromUtf8(""))
        self.serial3.setObjectName(_fromUtf8("serial3"))
        self.el3f = QtGui.QLabel(self.frame_2)
        self.el3f.setGeometry(QtCore.QRect(10, 110, 21, 17))
        self.el3f.setObjectName(_fromUtf8("el3f"))
        self.el13f = QtGui.QLabel(self.frame_2)
        self.el13f.setGeometry(QtCore.QRect(10, 230, 21, 17))
        self.el13f.setObjectName(_fromUtf8("el13f"))
        self.sca_fib3 = QtGui.QLabel(self.frame_2)
        self.sca_fib3.setGeometry(QtCore.QRect(220, 200, 51, 17))
        self.sca_fib3.setText(_fromUtf8(""))
        self.sca_fib3.setObjectName(_fromUtf8("sca_fib3"))
        self.verticalLayout_3.addWidget(self.frame_2)
        self.verticalLayoutWidget_2 = QtGui.QWidget(self.tab)
        self.verticalLayoutWidget_2.setGeometry(QtCore.QRect(0, 0, 431, 80))
        self.verticalLayoutWidget_2.setObjectName(_fromUtf8("verticalLayoutWidget_2"))
        self.verticalLayout_2 = QtGui.QVBoxLayout(self.verticalLayoutWidget_2)
        self.verticalLayout_2.setMargin(0)
        self.verticalLayout_2.setObjectName(_fromUtf8("verticalLayout_2"))
        self.frame = QtGui.QFrame(self.verticalLayoutWidget_2)
        self.frame.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame.setFrameShadow(QtGui.QFrame.Raised)
        self.frame.setObjectName(_fromUtf8("frame"))
        self.label_30 = QtGui.QLabel(self.frame)
        self.label_30.setGeometry(QtCore.QRect(10, 0, 421, 41))
        self.label_30.setObjectName(_fromUtf8("label_30"))
        self.label_2 = QtGui.QLabel(self.frame)
        self.label_2.setGeometry(QtCore.QRect(10, 31, 301, 21))
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.label = QtGui.QLabel(self.frame)
        self.label.setGeometry(QtCore.QRect(10, 50, 291, 21))
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout_2.addWidget(self.frame)
        self.verticalLayoutWidget_4 = QtGui.QWidget(self.tab)
        self.verticalLayoutWidget_4.setGeometry(QtCore.QRect(0, 460, 431, 381))
        self.verticalLayoutWidget_4.setObjectName(_fromUtf8("verticalLayoutWidget_4"))
        self.verticalLayout_4 = QtGui.QVBoxLayout(self.verticalLayoutWidget_4)
        self.verticalLayout_4.setMargin(0)
        self.verticalLayout_4.setObjectName(_fromUtf8("verticalLayout_4"))
        self.frame_3 = QtGui.QFrame(self.verticalLayoutWidget_4)
        self.frame_3.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_3.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_3.setObjectName(_fromUtf8("frame_3"))
        self.label_62 = QtGui.QLabel(self.frame_3)
        self.label_62.setGeometry(QtCore.QRect(80, 20, 251, 41))
        self.label_62.setObjectName(_fromUtf8("label_62"))
        self.vtr_stat0 = QtGui.QProgressBar(self.frame_3)
        self.vtr_stat0.setGeometry(QtCore.QRect(240, 100, 21, 23))
        self.vtr_stat0.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.vtr_stat0.setMaximum(100)
        self.vtr_stat0.setProperty("value", 0)
        self.vtr_stat0.setTextVisible(False)
        self.vtr_stat0.setOrientation(QtCore.Qt.Vertical)
        self.vtr_stat0.setInvertedAppearance(False)
        self.vtr_stat0.setObjectName(_fromUtf8("vtr_stat0"))
        self.vtr_stat4 = QtGui.QProgressBar(self.frame_3)
        self.vtr_stat4.setGeometry(QtCore.QRect(240, 260, 21, 23))
        self.vtr_stat4.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.vtr_stat4.setMaximum(100)
        self.vtr_stat4.setProperty("value", 0)
        self.vtr_stat4.setTextVisible(False)
        self.vtr_stat4.setOrientation(QtCore.Qt.Vertical)
        self.vtr_stat4.setInvertedAppearance(False)
        self.vtr_stat4.setObjectName(_fromUtf8("vtr_stat4"))
        self.vtr_stat1 = QtGui.QProgressBar(self.frame_3)
        self.vtr_stat1.setGeometry(QtCore.QRect(240, 140, 21, 23))
        self.vtr_stat1.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.vtr_stat1.setMaximum(100)
        self.vtr_stat1.setProperty("value", 0)
        self.vtr_stat1.setTextVisible(False)
        self.vtr_stat1.setOrientation(QtCore.Qt.Vertical)
        self.vtr_stat1.setInvertedAppearance(False)
        self.vtr_stat1.setObjectName(_fromUtf8("vtr_stat1"))
        self.vtr_stat2 = QtGui.QProgressBar(self.frame_3)
        self.vtr_stat2.setGeometry(QtCore.QRect(240, 180, 21, 23))
        self.vtr_stat2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.vtr_stat2.setMaximum(100)
        self.vtr_stat2.setProperty("value", 0)
        self.vtr_stat2.setTextVisible(False)
        self.vtr_stat2.setOrientation(QtCore.Qt.Vertical)
        self.vtr_stat2.setInvertedAppearance(False)
        self.vtr_stat2.setObjectName(_fromUtf8("vtr_stat2"))
        self.vtr_stat3 = QtGui.QProgressBar(self.frame_3)
        self.vtr_stat3.setGeometry(QtCore.QRect(240, 220, 21, 23))
        self.vtr_stat3.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.vtr_stat3.setMaximum(100)
        self.vtr_stat3.setProperty("value", 0)
        self.vtr_stat3.setTextVisible(False)
        self.vtr_stat3.setOrientation(QtCore.Qt.Vertical)
        self.vtr_stat3.setInvertedAppearance(False)
        self.vtr_stat3.setObjectName(_fromUtf8("vtr_stat3"))
        self.vtr_stat5 = QtGui.QProgressBar(self.frame_3)
        self.vtr_stat5.setGeometry(QtCore.QRect(240, 300, 21, 23))
        self.vtr_stat5.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.vtr_stat5.setMaximum(100)
        self.vtr_stat5.setProperty("value", 0)
        self.vtr_stat5.setTextVisible(False)
        self.vtr_stat5.setOrientation(QtCore.Qt.Vertical)
        self.vtr_stat5.setInvertedAppearance(False)
        self.vtr_stat5.setObjectName(_fromUtf8("vtr_stat5"))
        self.vtr_stat7 = QtGui.QProgressBar(self.frame_3)
        self.vtr_stat7.setGeometry(QtCore.QRect(370, 140, 21, 23))
        self.vtr_stat7.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.vtr_stat7.setMaximum(100)
        self.vtr_stat7.setProperty("value", 0)
        self.vtr_stat7.setTextVisible(False)
        self.vtr_stat7.setOrientation(QtCore.Qt.Vertical)
        self.vtr_stat7.setInvertedAppearance(False)
        self.vtr_stat7.setObjectName(_fromUtf8("vtr_stat7"))
        self.vtr_stat9 = QtGui.QProgressBar(self.frame_3)
        self.vtr_stat9.setGeometry(QtCore.QRect(370, 220, 21, 23))
        self.vtr_stat9.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.vtr_stat9.setMaximum(100)
        self.vtr_stat9.setProperty("value", 0)
        self.vtr_stat9.setTextVisible(False)
        self.vtr_stat9.setOrientation(QtCore.Qt.Vertical)
        self.vtr_stat9.setInvertedAppearance(False)
        self.vtr_stat9.setObjectName(_fromUtf8("vtr_stat9"))
        self.vtr_stat6 = QtGui.QProgressBar(self.frame_3)
        self.vtr_stat6.setGeometry(QtCore.QRect(370, 100, 21, 23))
        self.vtr_stat6.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.vtr_stat6.setMaximum(100)
        self.vtr_stat6.setProperty("value", 0)
        self.vtr_stat6.setTextVisible(False)
        self.vtr_stat6.setOrientation(QtCore.Qt.Vertical)
        self.vtr_stat6.setInvertedAppearance(False)
        self.vtr_stat6.setObjectName(_fromUtf8("vtr_stat6"))
        self.vtr_stat8 = QtGui.QProgressBar(self.frame_3)
        self.vtr_stat8.setGeometry(QtCore.QRect(370, 180, 21, 23))
        self.vtr_stat8.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.vtr_stat8.setMaximum(100)
        self.vtr_stat8.setProperty("value", 0)
        self.vtr_stat8.setTextVisible(False)
        self.vtr_stat8.setOrientation(QtCore.Qt.Vertical)
        self.vtr_stat8.setInvertedAppearance(False)
        self.vtr_stat8.setObjectName(_fromUtf8("vtr_stat8"))
        self.vtr_stat11 = QtGui.QProgressBar(self.frame_3)
        self.vtr_stat11.setGeometry(QtCore.QRect(370, 300, 21, 23))
        self.vtr_stat11.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.vtr_stat11.setMaximum(100)
        self.vtr_stat11.setProperty("value", 0)
        self.vtr_stat11.setTextVisible(False)
        self.vtr_stat11.setOrientation(QtCore.Qt.Vertical)
        self.vtr_stat11.setInvertedAppearance(False)
        self.vtr_stat11.setObjectName(_fromUtf8("vtr_stat11"))
        self.vtr_stat10 = QtGui.QProgressBar(self.frame_3)
        self.vtr_stat10.setGeometry(QtCore.QRect(370, 260, 21, 23))
        self.vtr_stat10.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.vtr_stat10.setMaximum(100)
        self.vtr_stat10.setProperty("value", 0)
        self.vtr_stat10.setTextVisible(False)
        self.vtr_stat10.setOrientation(QtCore.Qt.Vertical)
        self.vtr_stat10.setInvertedAppearance(False)
        self.vtr_stat10.setObjectName(_fromUtf8("vtr_stat10"))
        self.febadd_label_4 = QtGui.QLabel(self.frame_3)
        self.febadd_label_4.setGeometry(QtCore.QRect(230, 70, 51, 21))
        self.febadd_label_4.setObjectName(_fromUtf8("febadd_label_4"))
        self.febadd_label_5 = QtGui.QLabel(self.frame_3)
        self.febadd_label_5.setGeometry(QtCore.QRect(360, 70, 51, 21))
        self.febadd_label_5.setObjectName(_fromUtf8("febadd_label_5"))
        self.line_15 = QtGui.QFrame(self.frame_3)
        self.line_15.setGeometry(QtCore.QRect(210, 100, 20, 231))
        self.line_15.setFrameShape(QtGui.QFrame.VLine)
        self.line_15.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_15.setObjectName(_fromUtf8("line_15"))
        self.line_16 = QtGui.QFrame(self.frame_3)
        self.line_16.setGeometry(QtCore.QRect(340, 100, 20, 231))
        self.line_16.setFrameShape(QtGui.QFrame.VLine)
        self.line_16.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_16.setObjectName(_fromUtf8("line_16"))
        self.elink_label_2 = QtGui.QLabel(self.frame_3)
        self.elink_label_2.setGeometry(QtCore.QRect(170, 70, 41, 21))
        self.elink_label_2.setObjectName(_fromUtf8("elink_label_2"))
        self.elink_label_3 = QtGui.QLabel(self.frame_3)
        self.elink_label_3.setGeometry(QtCore.QRect(300, 70, 41, 21))
        self.elink_label_3.setObjectName(_fromUtf8("elink_label_3"))
        self.el3f_2 = QtGui.QLabel(self.frame_3)
        self.el3f_2.setGeometry(QtCore.QRect(180, 100, 21, 17))
        self.el3f_2.setObjectName(_fromUtf8("el3f_2"))
        self.el3f_3 = QtGui.QLabel(self.frame_3)
        self.el3f_3.setGeometry(QtCore.QRect(180, 140, 21, 17))
        self.el3f_3.setObjectName(_fromUtf8("el3f_3"))
        self.el3f_4 = QtGui.QLabel(self.frame_3)
        self.el3f_4.setGeometry(QtCore.QRect(180, 180, 21, 16))
        self.el3f_4.setObjectName(_fromUtf8("el3f_4"))
        self.el3f_5 = QtGui.QLabel(self.frame_3)
        self.el3f_5.setGeometry(QtCore.QRect(180, 220, 21, 16))
        self.el3f_5.setObjectName(_fromUtf8("el3f_5"))
        self.el3f_6 = QtGui.QLabel(self.frame_3)
        self.el3f_6.setGeometry(QtCore.QRect(180, 260, 21, 16))
        self.el3f_6.setObjectName(_fromUtf8("el3f_6"))
        self.el3f_7 = QtGui.QLabel(self.frame_3)
        self.el3f_7.setGeometry(QtCore.QRect(180, 300, 21, 16))
        self.el3f_7.setObjectName(_fromUtf8("el3f_7"))
        self.el3f_8 = QtGui.QLabel(self.frame_3)
        self.el3f_8.setGeometry(QtCore.QRect(310, 100, 21, 17))
        self.el3f_8.setObjectName(_fromUtf8("el3f_8"))
        self.el3f_9 = QtGui.QLabel(self.frame_3)
        self.el3f_9.setGeometry(QtCore.QRect(310, 140, 21, 17))
        self.el3f_9.setObjectName(_fromUtf8("el3f_9"))
        self.el3f_10 = QtGui.QLabel(self.frame_3)
        self.el3f_10.setGeometry(QtCore.QRect(310, 180, 21, 17))
        self.el3f_10.setObjectName(_fromUtf8("el3f_10"))
        self.el3f_11 = QtGui.QLabel(self.frame_3)
        self.el3f_11.setGeometry(QtCore.QRect(310, 220, 21, 17))
        self.el3f_11.setObjectName(_fromUtf8("el3f_11"))
        self.el3f_12 = QtGui.QLabel(self.frame_3)
        self.el3f_12.setGeometry(QtCore.QRect(310, 260, 21, 17))
        self.el3f_12.setObjectName(_fromUtf8("el3f_12"))
        self.el3f_13 = QtGui.QLabel(self.frame_3)
        self.el3f_13.setGeometry(QtCore.QRect(310, 300, 21, 17))
        self.el3f_13.setObjectName(_fromUtf8("el3f_13"))
        self.vtrxbtn = QtGui.QPushButton(self.frame_3)
        self.vtrxbtn.setGeometry(QtCore.QRect(40, 150, 101, 21))
        self.vtrxbtn.setObjectName(_fromUtf8("vtrxbtn"))
        self.vtrxprogress = QtGui.QProgressBar(self.frame_3)
        self.vtrxprogress.setGeometry(QtCore.QRect(20, 180, 141, 21))
        self.vtrxprogress.setProperty("value", 0)
        self.vtrxprogress.setObjectName(_fromUtf8("vtrxprogress"))
        self.verticalLayout_4.addWidget(self.frame_3)
        self.tabWidget.addTab(self.tab, _fromUtf8(""))
        self.tab_2 = QtGui.QWidget()
        self.tab_2.setObjectName(_fromUtf8("tab_2"))
        self.vtrx5_2 = QtGui.QLabel(self.tab_2)
        self.vtrx5_2.setGeometry(QtCore.QRect(850, 940, 67, 17))
        self.vtrx5_2.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx5_2.setObjectName(_fromUtf8("vtrx5_2"))
        self.vtrx7_2 = QtGui.QLabel(self.tab_2)
        self.vtrx7_2.setGeometry(QtCore.QRect(1120, 920, 67, 17))
        self.vtrx7_2.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx7_2.setObjectName(_fromUtf8("vtrx7_2"))
        self.verticalLayoutWidget_10 = QtGui.QWidget(self.tab_2)
        self.verticalLayoutWidget_10.setGeometry(QtCore.QRect(0, 0, 431, 841))
        self.verticalLayoutWidget_10.setObjectName(_fromUtf8("verticalLayoutWidget_10"))
        self.verticalLayout_10 = QtGui.QVBoxLayout(self.verticalLayoutWidget_10)
        self.verticalLayout_10.setMargin(0)
        self.verticalLayout_10.setObjectName(_fromUtf8("verticalLayout_10"))
        self.frame_9 = QtGui.QFrame(self.verticalLayoutWidget_10)
        self.frame_9.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_9.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_9.setObjectName(_fromUtf8("frame_9"))
        self.feb7_2 = QtGui.QLabel(self.frame_9)
        self.feb7_2.setGeometry(QtCore.QRect(50, 320, 151, 20))
        self.feb7_2.setText(_fromUtf8(""))
        self.feb7_2.setObjectName(_fromUtf8("feb7_2"))
        self.feb3_2 = QtGui.QLabel(self.frame_9)
        self.feb3_2.setGeometry(QtCore.QRect(50, 200, 151, 20))
        self.feb3_2.setText(_fromUtf8(""))
        self.feb3_2.setObjectName(_fromUtf8("feb3_2"))
        self.febadd_label_6 = QtGui.QLabel(self.frame_9)
        self.febadd_label_6.setGeometry(QtCore.QRect(110, 80, 31, 21))
        self.febadd_label_6.setObjectName(_fromUtf8("febadd_label_6"))
        self.feb1_2 = QtGui.QLabel(self.frame_9)
        self.feb1_2.setGeometry(QtCore.QRect(50, 140, 151, 20))
        self.feb1_2.setText(_fromUtf8(""))
        self.feb1_2.setObjectName(_fromUtf8("feb1_2"))
        self.elff_2 = QtGui.QLabel(self.frame_9)
        self.elff_2.setGeometry(QtCore.QRect(10, 200, 21, 17))
        self.elff_2.setObjectName(_fromUtf8("elff_2"))
        self.sca_fib2_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib2_2.setGeometry(QtCore.QRect(220, 170, 51, 17))
        self.sca_fib2_2.setText(_fromUtf8(""))
        self.sca_fib2_2.setObjectName(_fromUtf8("sca_fib2_2"))
        self.feb4_2 = QtGui.QLabel(self.frame_9)
        self.feb4_2.setGeometry(QtCore.QRect(50, 230, 151, 20))
        self.feb4_2.setText(_fromUtf8(""))
        self.feb4_2.setObjectName(_fromUtf8("feb4_2"))
        self.elbf_2 = QtGui.QLabel(self.frame_9)
        self.elbf_2.setGeometry(QtCore.QRect(10, 170, 21, 17))
        self.elbf_2.setObjectName(_fromUtf8("elbf_2"))
        self.sca_fib0_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib0_2.setGeometry(QtCore.QRect(220, 110, 51, 17))
        self.sca_fib0_2.setText(_fromUtf8(""))
        self.sca_fib0_2.setObjectName(_fromUtf8("sca_fib0_2"))
        self.sca_fib6_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib6_2.setGeometry(QtCore.QRect(220, 290, 51, 17))
        self.sca_fib6_2.setText(_fromUtf8(""))
        self.sca_fib6_2.setObjectName(_fromUtf8("sca_fib6_2"))
        self.serial4_2 = QtGui.QLabel(self.frame_9)
        self.serial4_2.setGeometry(QtCore.QRect(290, 230, 67, 17))
        self.serial4_2.setText(_fromUtf8(""))
        self.serial4_2.setObjectName(_fromUtf8("serial4_2"))
        self.estat6_2 = QtGui.QProgressBar(self.frame_9)
        self.estat6_2.setGeometry(QtCore.QRect(380, 290, 21, 23))
        self.estat6_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat6_2.setMaximum(100)
        self.estat6_2.setProperty("value", 0)
        self.estat6_2.setTextVisible(False)
        self.estat6_2.setOrientation(QtCore.Qt.Vertical)
        self.estat6_2.setInvertedAppearance(False)
        self.estat6_2.setObjectName(_fromUtf8("estat6_2"))
        self.serial1_2 = QtGui.QLabel(self.frame_9)
        self.serial1_2.setGeometry(QtCore.QRect(290, 140, 67, 17))
        self.serial1_2.setText(_fromUtf8(""))
        self.serial1_2.setObjectName(_fromUtf8("serial1_2"))
        self.estat4_2 = QtGui.QProgressBar(self.frame_9)
        self.estat4_2.setGeometry(QtCore.QRect(380, 230, 21, 23))
        self.estat4_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat4_2.setMaximum(100)
        self.estat4_2.setProperty("value", 0)
        self.estat4_2.setTextVisible(False)
        self.estat4_2.setOrientation(QtCore.Qt.Vertical)
        self.estat4_2.setInvertedAppearance(False)
        self.estat4_2.setObjectName(_fromUtf8("estat4_2"))
        self.feb5_2 = QtGui.QLabel(self.frame_9)
        self.feb5_2.setGeometry(QtCore.QRect(50, 260, 151, 20))
        self.feb5_2.setText(_fromUtf8(""))
        self.feb5_2.setObjectName(_fromUtf8("feb5_2"))
        self.serial6_2 = QtGui.QLabel(self.frame_9)
        self.serial6_2.setGeometry(QtCore.QRect(290, 290, 67, 17))
        self.serial6_2.setText(_fromUtf8(""))
        self.serial6_2.setObjectName(_fromUtf8("serial6_2"))
        self.serial2_2 = QtGui.QLabel(self.frame_9)
        self.serial2_2.setGeometry(QtCore.QRect(290, 170, 67, 17))
        self.serial2_2.setText(_fromUtf8(""))
        self.serial2_2.setObjectName(_fromUtf8("serial2_2"))
        self.estat3_2 = QtGui.QProgressBar(self.frame_9)
        self.estat3_2.setGeometry(QtCore.QRect(380, 200, 21, 23))
        self.estat3_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat3_2.setMaximum(100)
        self.estat3_2.setProperty("value", 0)
        self.estat3_2.setTextVisible(False)
        self.estat3_2.setOrientation(QtCore.Qt.Vertical)
        self.estat3_2.setInvertedAppearance(False)
        self.estat3_2.setObjectName(_fromUtf8("estat3_2"))
        self.el7f_2 = QtGui.QLabel(self.frame_9)
        self.el7f_2.setGeometry(QtCore.QRect(10, 140, 21, 17))
        self.el7f_2.setObjectName(_fromUtf8("el7f_2"))
        self.pingbtn_feb = QtGui.QPushButton(self.frame_9)
        self.pingbtn_feb.setGeometry(QtCore.QRect(10, 40, 91, 21))
        self.pingbtn_feb.setObjectName(_fromUtf8("pingbtn_feb"))
        self.scalabel_2 = QtGui.QLabel(self.frame_9)
        self.scalabel_2.setGeometry(QtCore.QRect(50, 0, 321, 41))
        self.scalabel_2.setObjectName(_fromUtf8("scalabel_2"))
        self.feb6_2 = QtGui.QLabel(self.frame_9)
        self.feb6_2.setGeometry(QtCore.QRect(50, 290, 151, 20))
        self.feb6_2.setText(_fromUtf8(""))
        self.feb6_2.setObjectName(_fromUtf8("feb6_2"))
        self.estat1_2 = QtGui.QProgressBar(self.frame_9)
        self.estat1_2.setGeometry(QtCore.QRect(380, 140, 21, 23))
        self.estat1_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat1_2.setMaximum(100)
        self.estat1_2.setProperty("value", 0)
        self.estat1_2.setTextVisible(False)
        self.estat1_2.setOrientation(QtCore.Qt.Vertical)
        self.estat1_2.setInvertedAppearance(False)
        self.estat1_2.setObjectName(_fromUtf8("estat1_2"))
        self.el17f_2 = QtGui.QLabel(self.frame_9)
        self.el17f_2.setGeometry(QtCore.QRect(10, 260, 21, 17))
        self.el17f_2.setObjectName(_fromUtf8("el17f_2"))
        self.sca_fib1_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib1_2.setGeometry(QtCore.QRect(220, 140, 51, 17))
        self.sca_fib1_2.setText(_fromUtf8(""))
        self.sca_fib1_2.setObjectName(_fromUtf8("sca_fib1_2"))
        self.line_17 = QtGui.QFrame(self.frame_9)
        self.line_17.setGeometry(QtCore.QRect(200, 110, 20, 711))
        self.line_17.setFrameShape(QtGui.QFrame.VLine)
        self.line_17.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_17.setObjectName(_fromUtf8("line_17"))
        self.elink_label_4 = QtGui.QLabel(self.frame_9)
        self.elink_label_4.setGeometry(QtCore.QRect(0, 80, 41, 21))
        self.elink_label_4.setObjectName(_fromUtf8("elink_label_4"))
        self.estat0_2 = QtGui.QProgressBar(self.frame_9)
        self.estat0_2.setGeometry(QtCore.QRect(380, 110, 21, 23))
        self.estat0_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat0_2.setMaximum(100)
        self.estat0_2.setProperty("value", 0)
        self.estat0_2.setTextVisible(False)
        self.estat0_2.setOrientation(QtCore.Qt.Vertical)
        self.estat0_2.setInvertedAppearance(False)
        self.estat0_2.setObjectName(_fromUtf8("estat0_2"))
        self.sca_fib4_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib4_2.setGeometry(QtCore.QRect(220, 230, 51, 17))
        self.sca_fib4_2.setText(_fromUtf8(""))
        self.sca_fib4_2.setObjectName(_fromUtf8("sca_fib4_2"))
        self.estat7_2 = QtGui.QProgressBar(self.frame_9)
        self.estat7_2.setGeometry(QtCore.QRect(380, 320, 21, 23))
        self.estat7_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat7_2.setMaximum(100)
        self.estat7_2.setProperty("value", 0)
        self.estat7_2.setTextVisible(False)
        self.estat7_2.setOrientation(QtCore.Qt.Vertical)
        self.estat7_2.setInvertedAppearance(False)
        self.estat7_2.setObjectName(_fromUtf8("estat7_2"))
        self.line_4 = QtGui.QFrame(self.frame_9)
        self.line_4.setGeometry(QtCore.QRect(350, 110, 20, 711))
        self.line_4.setFrameShape(QtGui.QFrame.VLine)
        self.line_4.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_4.setObjectName(_fromUtf8("line_4"))
        self.el1ff_2 = QtGui.QLabel(self.frame_9)
        self.el1ff_2.setGeometry(QtCore.QRect(10, 320, 21, 16))
        self.el1ff_2.setObjectName(_fromUtf8("el1ff_2"))
        self.feb0_2 = QtGui.QLabel(self.frame_9)
        self.feb0_2.setGeometry(QtCore.QRect(50, 110, 151, 20))
        self.feb0_2.setText(_fromUtf8(""))
        self.feb0_2.setObjectName(_fromUtf8("feb0_2"))
        self.sernbr_label_2 = QtGui.QLabel(self.frame_9)
        self.sernbr_label_2.setGeometry(QtCore.QRect(300, 80, 51, 21))
        self.sernbr_label_2.setObjectName(_fromUtf8("sernbr_label_2"))
        self.estat5_2 = QtGui.QProgressBar(self.frame_9)
        self.estat5_2.setGeometry(QtCore.QRect(380, 260, 21, 23))
        self.estat5_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat5_2.setMaximum(100)
        self.estat5_2.setProperty("value", 0)
        self.estat5_2.setTextVisible(False)
        self.estat5_2.setOrientation(QtCore.Qt.Vertical)
        self.estat5_2.setInvertedAppearance(False)
        self.estat5_2.setObjectName(_fromUtf8("estat5_2"))
        self.line_18 = QtGui.QFrame(self.frame_9)
        self.line_18.setGeometry(QtCore.QRect(270, 110, 20, 711))
        self.line_18.setFrameShape(QtGui.QFrame.VLine)
        self.line_18.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_18.setObjectName(_fromUtf8("line_18"))
        self.febadd_label_7 = QtGui.QLabel(self.frame_9)
        self.febadd_label_7.setGeometry(QtCore.QRect(370, 80, 51, 21))
        self.febadd_label_7.setObjectName(_fromUtf8("febadd_label_7"))
        self.serial0_2 = QtGui.QLabel(self.frame_9)
        self.serial0_2.setGeometry(QtCore.QRect(290, 110, 71, 17))
        self.serial0_2.setText(_fromUtf8(""))
        self.serial0_2.setObjectName(_fromUtf8("serial0_2"))
        self.sca_fib7_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib7_2.setGeometry(QtCore.QRect(220, 320, 51, 17))
        self.sca_fib7_2.setText(_fromUtf8(""))
        self.sca_fib7_2.setObjectName(_fromUtf8("sca_fib7_2"))
        self.estat2_2 = QtGui.QProgressBar(self.frame_9)
        self.estat2_2.setGeometry(QtCore.QRect(380, 170, 21, 23))
        self.estat2_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat2_2.setMaximum(100)
        self.estat2_2.setProperty("value", 0)
        self.estat2_2.setTextVisible(False)
        self.estat2_2.setOrientation(QtCore.Qt.Vertical)
        self.estat2_2.setInvertedAppearance(False)
        self.estat2_2.setObjectName(_fromUtf8("estat2_2"))
        self.pingprogress_feb = QtGui.QProgressBar(self.frame_9)
        self.pingprogress_feb.setGeometry(QtCore.QRect(110, 40, 291, 21))
        self.pingprogress_feb.setProperty("value", 0)
        self.pingprogress_feb.setObjectName(_fromUtf8("pingprogress_feb"))
        self.el1bf_2 = QtGui.QLabel(self.frame_9)
        self.el1bf_2.setGeometry(QtCore.QRect(10, 290, 31, 17))
        self.el1bf_2.setObjectName(_fromUtf8("el1bf_2"))
        self.line_5 = QtGui.QFrame(self.frame_9)
        self.line_5.setGeometry(QtCore.QRect(30, 110, 20, 711))
        self.line_5.setFrameShape(QtGui.QFrame.VLine)
        self.line_5.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_5.setObjectName(_fromUtf8("line_5"))
        self.febadd_label_8 = QtGui.QLabel(self.frame_9)
        self.febadd_label_8.setGeometry(QtCore.QRect(220, 80, 51, 21))
        self.febadd_label_8.setObjectName(_fromUtf8("febadd_label_8"))
        self.sca_fib5_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib5_2.setGeometry(QtCore.QRect(220, 260, 51, 17))
        self.sca_fib5_2.setText(_fromUtf8(""))
        self.sca_fib5_2.setObjectName(_fromUtf8("sca_fib5_2"))
        self.feb2_2 = QtGui.QLabel(self.frame_9)
        self.feb2_2.setGeometry(QtCore.QRect(50, 170, 151, 20))
        self.feb2_2.setText(_fromUtf8(""))
        self.feb2_2.setObjectName(_fromUtf8("feb2_2"))
        self.serial5_2 = QtGui.QLabel(self.frame_9)
        self.serial5_2.setGeometry(QtCore.QRect(290, 260, 67, 17))
        self.serial5_2.setText(_fromUtf8(""))
        self.serial5_2.setObjectName(_fromUtf8("serial5_2"))
        self.serial7_2 = QtGui.QLabel(self.frame_9)
        self.serial7_2.setGeometry(QtCore.QRect(290, 320, 67, 17))
        self.serial7_2.setText(_fromUtf8(""))
        self.serial7_2.setObjectName(_fromUtf8("serial7_2"))
        self.serial3_2 = QtGui.QLabel(self.frame_9)
        self.serial3_2.setGeometry(QtCore.QRect(290, 200, 67, 17))
        self.serial3_2.setText(_fromUtf8(""))
        self.serial3_2.setObjectName(_fromUtf8("serial3_2"))
        self.el3f_14 = QtGui.QLabel(self.frame_9)
        self.el3f_14.setGeometry(QtCore.QRect(10, 110, 21, 17))
        self.el3f_14.setObjectName(_fromUtf8("el3f_14"))
        self.el13f_2 = QtGui.QLabel(self.frame_9)
        self.el13f_2.setGeometry(QtCore.QRect(10, 230, 21, 17))
        self.el13f_2.setObjectName(_fromUtf8("el13f_2"))
        self.sca_fib3_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib3_2.setGeometry(QtCore.QRect(220, 200, 51, 17))
        self.sca_fib3_2.setText(_fromUtf8(""))
        self.sca_fib3_2.setObjectName(_fromUtf8("sca_fib3_2"))
        self.estat8_2 = QtGui.QProgressBar(self.frame_9)
        self.estat8_2.setGeometry(QtCore.QRect(380, 350, 21, 23))
        self.estat8_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat8_2.setMaximum(100)
        self.estat8_2.setProperty("value", 0)
        self.estat8_2.setTextVisible(False)
        self.estat8_2.setOrientation(QtCore.Qt.Vertical)
        self.estat8_2.setInvertedAppearance(False)
        self.estat8_2.setObjectName(_fromUtf8("estat8_2"))
        self.estat9_2 = QtGui.QProgressBar(self.frame_9)
        self.estat9_2.setGeometry(QtCore.QRect(380, 380, 21, 23))
        self.estat9_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat9_2.setMaximum(100)
        self.estat9_2.setProperty("value", 0)
        self.estat9_2.setTextVisible(False)
        self.estat9_2.setOrientation(QtCore.Qt.Vertical)
        self.estat9_2.setInvertedAppearance(False)
        self.estat9_2.setObjectName(_fromUtf8("estat9_2"))
        self.estat10_2 = QtGui.QProgressBar(self.frame_9)
        self.estat10_2.setGeometry(QtCore.QRect(380, 410, 21, 23))
        self.estat10_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat10_2.setMaximum(100)
        self.estat10_2.setProperty("value", 0)
        self.estat10_2.setTextVisible(False)
        self.estat10_2.setOrientation(QtCore.Qt.Vertical)
        self.estat10_2.setInvertedAppearance(False)
        self.estat10_2.setObjectName(_fromUtf8("estat10_2"))
        self.estat11_2 = QtGui.QProgressBar(self.frame_9)
        self.estat11_2.setGeometry(QtCore.QRect(380, 440, 21, 23))
        self.estat11_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat11_2.setMaximum(100)
        self.estat11_2.setProperty("value", 0)
        self.estat11_2.setTextVisible(False)
        self.estat11_2.setOrientation(QtCore.Qt.Vertical)
        self.estat11_2.setInvertedAppearance(False)
        self.estat11_2.setObjectName(_fromUtf8("estat11_2"))
        self.estat12_2 = QtGui.QProgressBar(self.frame_9)
        self.estat12_2.setGeometry(QtCore.QRect(380, 470, 21, 23))
        self.estat12_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat12_2.setMaximum(100)
        self.estat12_2.setProperty("value", 0)
        self.estat12_2.setTextVisible(False)
        self.estat12_2.setOrientation(QtCore.Qt.Vertical)
        self.estat12_2.setInvertedAppearance(False)
        self.estat12_2.setObjectName(_fromUtf8("estat12_2"))
        self.el1ff_3 = QtGui.QLabel(self.frame_9)
        self.el1ff_3.setGeometry(QtCore.QRect(10, 350, 21, 16))
        self.el1ff_3.setObjectName(_fromUtf8("el1ff_3"))
        self.el1ff_4 = QtGui.QLabel(self.frame_9)
        self.el1ff_4.setGeometry(QtCore.QRect(10, 380, 21, 16))
        self.el1ff_4.setObjectName(_fromUtf8("el1ff_4"))
        self.el1ff_5 = QtGui.QLabel(self.frame_9)
        self.el1ff_5.setGeometry(QtCore.QRect(10, 410, 21, 16))
        self.el1ff_5.setObjectName(_fromUtf8("el1ff_5"))
        self.el1ff_6 = QtGui.QLabel(self.frame_9)
        self.el1ff_6.setGeometry(QtCore.QRect(10, 440, 21, 16))
        self.el1ff_6.setObjectName(_fromUtf8("el1ff_6"))
        self.el1ff_7 = QtGui.QLabel(self.frame_9)
        self.el1ff_7.setGeometry(QtCore.QRect(10, 470, 31, 16))
        self.el1ff_7.setObjectName(_fromUtf8("el1ff_7"))
        self.el1ff_8 = QtGui.QLabel(self.frame_9)
        self.el1ff_8.setGeometry(QtCore.QRect(10, 500, 31, 16))
        self.el1ff_8.setObjectName(_fromUtf8("el1ff_8"))
        self.el1ff_9 = QtGui.QLabel(self.frame_9)
        self.el1ff_9.setGeometry(QtCore.QRect(10, 530, 31, 16))
        self.el1ff_9.setObjectName(_fromUtf8("el1ff_9"))
        self.el1ff_10 = QtGui.QLabel(self.frame_9)
        self.el1ff_10.setGeometry(QtCore.QRect(10, 560, 31, 16))
        self.el1ff_10.setObjectName(_fromUtf8("el1ff_10"))
        self.el1ff_11 = QtGui.QLabel(self.frame_9)
        self.el1ff_11.setGeometry(QtCore.QRect(10, 590, 31, 16))
        self.el1ff_11.setObjectName(_fromUtf8("el1ff_11"))
        self.el1ff_12 = QtGui.QLabel(self.frame_9)
        self.el1ff_12.setGeometry(QtCore.QRect(10, 620, 31, 16))
        self.el1ff_12.setObjectName(_fromUtf8("el1ff_12"))
        self.el1ff_13 = QtGui.QLabel(self.frame_9)
        self.el1ff_13.setGeometry(QtCore.QRect(10, 650, 31, 16))
        self.el1ff_13.setObjectName(_fromUtf8("el1ff_13"))
        self.el1ff_14 = QtGui.QLabel(self.frame_9)
        self.el1ff_14.setGeometry(QtCore.QRect(10, 680, 31, 16))
        self.el1ff_14.setObjectName(_fromUtf8("el1ff_14"))
        self.estat15_2 = QtGui.QProgressBar(self.frame_9)
        self.estat15_2.setGeometry(QtCore.QRect(380, 560, 21, 23))
        self.estat15_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat15_2.setMaximum(100)
        self.estat15_2.setProperty("value", 0)
        self.estat15_2.setTextVisible(False)
        self.estat15_2.setOrientation(QtCore.Qt.Vertical)
        self.estat15_2.setInvertedAppearance(False)
        self.estat15_2.setObjectName(_fromUtf8("estat15_2"))
        self.estat16_2 = QtGui.QProgressBar(self.frame_9)
        self.estat16_2.setGeometry(QtCore.QRect(380, 590, 21, 23))
        self.estat16_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat16_2.setMaximum(100)
        self.estat16_2.setProperty("value", 0)
        self.estat16_2.setTextVisible(False)
        self.estat16_2.setOrientation(QtCore.Qt.Vertical)
        self.estat16_2.setInvertedAppearance(False)
        self.estat16_2.setObjectName(_fromUtf8("estat16_2"))
        self.estat14_2 = QtGui.QProgressBar(self.frame_9)
        self.estat14_2.setGeometry(QtCore.QRect(380, 530, 21, 23))
        self.estat14_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat14_2.setMaximum(100)
        self.estat14_2.setProperty("value", 0)
        self.estat14_2.setTextVisible(False)
        self.estat14_2.setOrientation(QtCore.Qt.Vertical)
        self.estat14_2.setInvertedAppearance(False)
        self.estat14_2.setObjectName(_fromUtf8("estat14_2"))
        self.estat13_2 = QtGui.QProgressBar(self.frame_9)
        self.estat13_2.setGeometry(QtCore.QRect(380, 500, 21, 23))
        self.estat13_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat13_2.setMaximum(100)
        self.estat13_2.setProperty("value", 0)
        self.estat13_2.setTextVisible(False)
        self.estat13_2.setOrientation(QtCore.Qt.Vertical)
        self.estat13_2.setInvertedAppearance(False)
        self.estat13_2.setObjectName(_fromUtf8("estat13_2"))
        self.estat18_2 = QtGui.QProgressBar(self.frame_9)
        self.estat18_2.setGeometry(QtCore.QRect(380, 650, 21, 23))
        self.estat18_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat18_2.setMaximum(100)
        self.estat18_2.setProperty("value", 0)
        self.estat18_2.setTextVisible(False)
        self.estat18_2.setOrientation(QtCore.Qt.Vertical)
        self.estat18_2.setInvertedAppearance(False)
        self.estat18_2.setObjectName(_fromUtf8("estat18_2"))
        self.estat17_2 = QtGui.QProgressBar(self.frame_9)
        self.estat17_2.setGeometry(QtCore.QRect(380, 620, 21, 23))
        self.estat17_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat17_2.setMaximum(100)
        self.estat17_2.setProperty("value", 0)
        self.estat17_2.setTextVisible(False)
        self.estat17_2.setOrientation(QtCore.Qt.Vertical)
        self.estat17_2.setInvertedAppearance(False)
        self.estat17_2.setObjectName(_fromUtf8("estat17_2"))
        self.estat19_2 = QtGui.QProgressBar(self.frame_9)
        self.estat19_2.setGeometry(QtCore.QRect(380, 680, 21, 23))
        self.estat19_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat19_2.setMaximum(100)
        self.estat19_2.setProperty("value", 0)
        self.estat19_2.setTextVisible(False)
        self.estat19_2.setOrientation(QtCore.Qt.Vertical)
        self.estat19_2.setInvertedAppearance(False)
        self.estat19_2.setObjectName(_fromUtf8("estat19_2"))
        self.el1ff_15 = QtGui.QLabel(self.frame_9)
        self.el1ff_15.setGeometry(QtCore.QRect(10, 710, 31, 16))
        self.el1ff_15.setObjectName(_fromUtf8("el1ff_15"))
        self.el1ff_16 = QtGui.QLabel(self.frame_9)
        self.el1ff_16.setGeometry(QtCore.QRect(10, 740, 31, 16))
        self.el1ff_16.setObjectName(_fromUtf8("el1ff_16"))
        self.el1ff_17 = QtGui.QLabel(self.frame_9)
        self.el1ff_17.setGeometry(QtCore.QRect(10, 770, 31, 16))
        self.el1ff_17.setObjectName(_fromUtf8("el1ff_17"))
        self.el1ff_18 = QtGui.QLabel(self.frame_9)
        self.el1ff_18.setGeometry(QtCore.QRect(10, 800, 31, 16))
        self.el1ff_18.setObjectName(_fromUtf8("el1ff_18"))
        self.estat20_2 = QtGui.QProgressBar(self.frame_9)
        self.estat20_2.setGeometry(QtCore.QRect(380, 710, 21, 23))
        self.estat20_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat20_2.setMaximum(100)
        self.estat20_2.setProperty("value", 0)
        self.estat20_2.setTextVisible(False)
        self.estat20_2.setOrientation(QtCore.Qt.Vertical)
        self.estat20_2.setInvertedAppearance(False)
        self.estat20_2.setObjectName(_fromUtf8("estat20_2"))
        self.estat21_2 = QtGui.QProgressBar(self.frame_9)
        self.estat21_2.setGeometry(QtCore.QRect(380, 740, 21, 23))
        self.estat21_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat21_2.setMaximum(100)
        self.estat21_2.setProperty("value", 0)
        self.estat21_2.setTextVisible(False)
        self.estat21_2.setOrientation(QtCore.Qt.Vertical)
        self.estat21_2.setInvertedAppearance(False)
        self.estat21_2.setObjectName(_fromUtf8("estat21_2"))
        self.estat22_2 = QtGui.QProgressBar(self.frame_9)
        self.estat22_2.setGeometry(QtCore.QRect(380, 770, 21, 23))
        self.estat22_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat22_2.setMaximum(100)
        self.estat22_2.setProperty("value", 0)
        self.estat22_2.setTextVisible(False)
        self.estat22_2.setOrientation(QtCore.Qt.Vertical)
        self.estat22_2.setInvertedAppearance(False)
        self.estat22_2.setObjectName(_fromUtf8("estat22_2"))
        self.estat23_2 = QtGui.QProgressBar(self.frame_9)
        self.estat23_2.setGeometry(QtCore.QRect(380, 800, 21, 23))
        self.estat23_2.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat23_2.setMaximum(100)
        self.estat23_2.setProperty("value", 0)
        self.estat23_2.setTextVisible(False)
        self.estat23_2.setOrientation(QtCore.Qt.Vertical)
        self.estat23_2.setInvertedAppearance(False)
        self.estat23_2.setObjectName(_fromUtf8("estat23_2"))
        self.feb14_2 = QtGui.QLabel(self.frame_9)
        self.feb14_2.setGeometry(QtCore.QRect(50, 530, 151, 20))
        self.feb14_2.setText(_fromUtf8(""))
        self.feb14_2.setObjectName(_fromUtf8("feb14_2"))
        self.feb12_2 = QtGui.QLabel(self.frame_9)
        self.feb12_2.setGeometry(QtCore.QRect(50, 470, 151, 20))
        self.feb12_2.setText(_fromUtf8(""))
        self.feb12_2.setObjectName(_fromUtf8("feb12_2"))
        self.feb10_2 = QtGui.QLabel(self.frame_9)
        self.feb10_2.setGeometry(QtCore.QRect(50, 410, 151, 20))
        self.feb10_2.setText(_fromUtf8(""))
        self.feb10_2.setObjectName(_fromUtf8("feb10_2"))
        self.feb15_2 = QtGui.QLabel(self.frame_9)
        self.feb15_2.setGeometry(QtCore.QRect(50, 560, 151, 20))
        self.feb15_2.setText(_fromUtf8(""))
        self.feb15_2.setObjectName(_fromUtf8("feb15_2"))
        self.feb11_2 = QtGui.QLabel(self.frame_9)
        self.feb11_2.setGeometry(QtCore.QRect(50, 440, 151, 20))
        self.feb11_2.setText(_fromUtf8(""))
        self.feb11_2.setObjectName(_fromUtf8("feb11_2"))
        self.feb9_2 = QtGui.QLabel(self.frame_9)
        self.feb9_2.setGeometry(QtCore.QRect(50, 380, 151, 20))
        self.feb9_2.setText(_fromUtf8(""))
        self.feb9_2.setObjectName(_fromUtf8("feb9_2"))
        self.feb8_2 = QtGui.QLabel(self.frame_9)
        self.feb8_2.setGeometry(QtCore.QRect(50, 350, 151, 20))
        self.feb8_2.setText(_fromUtf8(""))
        self.feb8_2.setObjectName(_fromUtf8("feb8_2"))
        self.feb13_2 = QtGui.QLabel(self.frame_9)
        self.feb13_2.setGeometry(QtCore.QRect(50, 500, 151, 20))
        self.feb13_2.setText(_fromUtf8(""))
        self.feb13_2.setObjectName(_fromUtf8("feb13_2"))
        self.feb21_2 = QtGui.QLabel(self.frame_9)
        self.feb21_2.setGeometry(QtCore.QRect(50, 740, 151, 20))
        self.feb21_2.setText(_fromUtf8(""))
        self.feb21_2.setObjectName(_fromUtf8("feb21_2"))
        self.feb18_2 = QtGui.QLabel(self.frame_9)
        self.feb18_2.setGeometry(QtCore.QRect(50, 650, 151, 20))
        self.feb18_2.setText(_fromUtf8(""))
        self.feb18_2.setObjectName(_fromUtf8("feb18_2"))
        self.feb16_2 = QtGui.QLabel(self.frame_9)
        self.feb16_2.setGeometry(QtCore.QRect(50, 590, 151, 20))
        self.feb16_2.setText(_fromUtf8(""))
        self.feb16_2.setObjectName(_fromUtf8("feb16_2"))
        self.feb19_2 = QtGui.QLabel(self.frame_9)
        self.feb19_2.setGeometry(QtCore.QRect(50, 680, 151, 20))
        self.feb19_2.setText(_fromUtf8(""))
        self.feb19_2.setObjectName(_fromUtf8("feb19_2"))
        self.feb22_2 = QtGui.QLabel(self.frame_9)
        self.feb22_2.setGeometry(QtCore.QRect(50, 770, 151, 20))
        self.feb22_2.setText(_fromUtf8(""))
        self.feb22_2.setObjectName(_fromUtf8("feb22_2"))
        self.feb20_2 = QtGui.QLabel(self.frame_9)
        self.feb20_2.setGeometry(QtCore.QRect(50, 710, 151, 20))
        self.feb20_2.setText(_fromUtf8(""))
        self.feb20_2.setObjectName(_fromUtf8("feb20_2"))
        self.feb17_2 = QtGui.QLabel(self.frame_9)
        self.feb17_2.setGeometry(QtCore.QRect(50, 620, 151, 20))
        self.feb17_2.setText(_fromUtf8(""))
        self.feb17_2.setObjectName(_fromUtf8("feb17_2"))
        self.feb23_2 = QtGui.QLabel(self.frame_9)
        self.feb23_2.setGeometry(QtCore.QRect(50, 800, 151, 20))
        self.feb23_2.setText(_fromUtf8(""))
        self.feb23_2.setObjectName(_fromUtf8("feb23_2"))
        self.sca_fib9_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib9_2.setGeometry(QtCore.QRect(220, 380, 51, 17))
        self.sca_fib9_2.setText(_fromUtf8(""))
        self.sca_fib9_2.setObjectName(_fromUtf8("sca_fib9_2"))
        self.sca_fib10_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib10_2.setGeometry(QtCore.QRect(220, 410, 51, 17))
        self.sca_fib10_2.setText(_fromUtf8(""))
        self.sca_fib10_2.setObjectName(_fromUtf8("sca_fib10_2"))
        self.sca_fib11_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib11_2.setGeometry(QtCore.QRect(220, 440, 51, 17))
        self.sca_fib11_2.setText(_fromUtf8(""))
        self.sca_fib11_2.setObjectName(_fromUtf8("sca_fib11_2"))
        self.sca_fib12_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib12_2.setGeometry(QtCore.QRect(220, 470, 51, 17))
        self.sca_fib12_2.setText(_fromUtf8(""))
        self.sca_fib12_2.setObjectName(_fromUtf8("sca_fib12_2"))
        self.sca_fib14_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib14_2.setGeometry(QtCore.QRect(220, 530, 51, 17))
        self.sca_fib14_2.setText(_fromUtf8(""))
        self.sca_fib14_2.setObjectName(_fromUtf8("sca_fib14_2"))
        self.sca_fib15_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib15_2.setGeometry(QtCore.QRect(220, 560, 51, 17))
        self.sca_fib15_2.setText(_fromUtf8(""))
        self.sca_fib15_2.setObjectName(_fromUtf8("sca_fib15_2"))
        self.sca_fib13_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib13_2.setGeometry(QtCore.QRect(220, 500, 51, 17))
        self.sca_fib13_2.setText(_fromUtf8(""))
        self.sca_fib13_2.setObjectName(_fromUtf8("sca_fib13_2"))
        self.sca_fib8_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib8_2.setGeometry(QtCore.QRect(220, 350, 51, 17))
        self.sca_fib8_2.setText(_fromUtf8(""))
        self.sca_fib8_2.setObjectName(_fromUtf8("sca_fib8_2"))
        self.sca_fib17_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib17_2.setGeometry(QtCore.QRect(220, 620, 51, 17))
        self.sca_fib17_2.setText(_fromUtf8(""))
        self.sca_fib17_2.setObjectName(_fromUtf8("sca_fib17_2"))
        self.sca_fib22_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib22_2.setGeometry(QtCore.QRect(220, 770, 51, 17))
        self.sca_fib22_2.setText(_fromUtf8(""))
        self.sca_fib22_2.setObjectName(_fromUtf8("sca_fib22_2"))
        self.sca_fib16_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib16_2.setGeometry(QtCore.QRect(220, 590, 51, 17))
        self.sca_fib16_2.setText(_fromUtf8(""))
        self.sca_fib16_2.setObjectName(_fromUtf8("sca_fib16_2"))
        self.sca_fib18_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib18_2.setGeometry(QtCore.QRect(220, 650, 51, 17))
        self.sca_fib18_2.setText(_fromUtf8(""))
        self.sca_fib18_2.setObjectName(_fromUtf8("sca_fib18_2"))
        self.sca_fib21_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib21_2.setGeometry(QtCore.QRect(220, 740, 51, 17))
        self.sca_fib21_2.setText(_fromUtf8(""))
        self.sca_fib21_2.setObjectName(_fromUtf8("sca_fib21_2"))
        self.sca_fib19_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib19_2.setGeometry(QtCore.QRect(220, 680, 51, 17))
        self.sca_fib19_2.setText(_fromUtf8(""))
        self.sca_fib19_2.setObjectName(_fromUtf8("sca_fib19_2"))
        self.sca_fib23_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib23_2.setGeometry(QtCore.QRect(220, 800, 51, 17))
        self.sca_fib23_2.setText(_fromUtf8(""))
        self.sca_fib23_2.setObjectName(_fromUtf8("sca_fib23_2"))
        self.sca_fib20_2 = QtGui.QLabel(self.frame_9)
        self.sca_fib20_2.setGeometry(QtCore.QRect(220, 710, 51, 17))
        self.sca_fib20_2.setText(_fromUtf8(""))
        self.sca_fib20_2.setObjectName(_fromUtf8("sca_fib20_2"))
        self.serial12_2 = QtGui.QLabel(self.frame_9)
        self.serial12_2.setGeometry(QtCore.QRect(290, 470, 67, 17))
        self.serial12_2.setText(_fromUtf8(""))
        self.serial12_2.setObjectName(_fromUtf8("serial12_2"))
        self.serial15_2 = QtGui.QLabel(self.frame_9)
        self.serial15_2.setGeometry(QtCore.QRect(290, 560, 67, 17))
        self.serial15_2.setText(_fromUtf8(""))
        self.serial15_2.setObjectName(_fromUtf8("serial15_2"))
        self.serial9_2 = QtGui.QLabel(self.frame_9)
        self.serial9_2.setGeometry(QtCore.QRect(290, 380, 67, 17))
        self.serial9_2.setText(_fromUtf8(""))
        self.serial9_2.setObjectName(_fromUtf8("serial9_2"))
        self.serial14_2 = QtGui.QLabel(self.frame_9)
        self.serial14_2.setGeometry(QtCore.QRect(290, 530, 67, 17))
        self.serial14_2.setText(_fromUtf8(""))
        self.serial14_2.setObjectName(_fromUtf8("serial14_2"))
        self.serial10_2 = QtGui.QLabel(self.frame_9)
        self.serial10_2.setGeometry(QtCore.QRect(290, 410, 67, 17))
        self.serial10_2.setText(_fromUtf8(""))
        self.serial10_2.setObjectName(_fromUtf8("serial10_2"))
        self.serial11_2 = QtGui.QLabel(self.frame_9)
        self.serial11_2.setGeometry(QtCore.QRect(290, 440, 67, 17))
        self.serial11_2.setText(_fromUtf8(""))
        self.serial11_2.setObjectName(_fromUtf8("serial11_2"))
        self.serial8_2 = QtGui.QLabel(self.frame_9)
        self.serial8_2.setGeometry(QtCore.QRect(290, 350, 71, 17))
        self.serial8_2.setText(_fromUtf8(""))
        self.serial8_2.setObjectName(_fromUtf8("serial8_2"))
        self.serial13_2 = QtGui.QLabel(self.frame_9)
        self.serial13_2.setGeometry(QtCore.QRect(290, 500, 67, 17))
        self.serial13_2.setText(_fromUtf8(""))
        self.serial13_2.setObjectName(_fromUtf8("serial13_2"))
        self.serial20_2 = QtGui.QLabel(self.frame_9)
        self.serial20_2.setGeometry(QtCore.QRect(290, 710, 67, 17))
        self.serial20_2.setText(_fromUtf8(""))
        self.serial20_2.setObjectName(_fromUtf8("serial20_2"))
        self.serial22_2 = QtGui.QLabel(self.frame_9)
        self.serial22_2.setGeometry(QtCore.QRect(290, 770, 67, 17))
        self.serial22_2.setText(_fromUtf8(""))
        self.serial22_2.setObjectName(_fromUtf8("serial22_2"))
        self.serial23_2 = QtGui.QLabel(self.frame_9)
        self.serial23_2.setGeometry(QtCore.QRect(290, 800, 67, 17))
        self.serial23_2.setText(_fromUtf8(""))
        self.serial23_2.setObjectName(_fromUtf8("serial23_2"))
        self.serial18_2 = QtGui.QLabel(self.frame_9)
        self.serial18_2.setGeometry(QtCore.QRect(290, 650, 67, 17))
        self.serial18_2.setText(_fromUtf8(""))
        self.serial18_2.setObjectName(_fromUtf8("serial18_2"))
        self.serial19_2 = QtGui.QLabel(self.frame_9)
        self.serial19_2.setGeometry(QtCore.QRect(290, 680, 67, 17))
        self.serial19_2.setText(_fromUtf8(""))
        self.serial19_2.setObjectName(_fromUtf8("serial19_2"))
        self.serial17_2 = QtGui.QLabel(self.frame_9)
        self.serial17_2.setGeometry(QtCore.QRect(290, 620, 67, 17))
        self.serial17_2.setText(_fromUtf8(""))
        self.serial17_2.setObjectName(_fromUtf8("serial17_2"))
        self.serial21_2 = QtGui.QLabel(self.frame_9)
        self.serial21_2.setGeometry(QtCore.QRect(290, 740, 67, 17))
        self.serial21_2.setText(_fromUtf8(""))
        self.serial21_2.setObjectName(_fromUtf8("serial21_2"))
        self.serial16_2 = QtGui.QLabel(self.frame_9)
        self.serial16_2.setGeometry(QtCore.QRect(290, 590, 71, 17))
        self.serial16_2.setText(_fromUtf8(""))
        self.serial16_2.setObjectName(_fromUtf8("serial16_2"))
        self.verticalLayout_10.addWidget(self.frame_9)
        self.vtrx0_2 = QtGui.QLabel(self.tab_2)
        self.vtrx0_2.setGeometry(QtCore.QRect(340, 920, 67, 17))
        self.vtrx0_2.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx0_2.setObjectName(_fromUtf8("vtrx0_2"))
        self.vtrx6_2 = QtGui.QLabel(self.tab_2)
        self.vtrx6_2.setGeometry(QtCore.QRect(1010, 920, 67, 17))
        self.vtrx6_2.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx6_2.setObjectName(_fromUtf8("vtrx6_2"))
        self.vtrx9_2 = QtGui.QLabel(self.tab_2)
        self.vtrx9_2.setGeometry(QtCore.QRect(1480, 930, 67, 17))
        self.vtrx9_2.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx9_2.setObjectName(_fromUtf8("vtrx9_2"))
        self.vtrx10_2 = QtGui.QLabel(self.tab_2)
        self.vtrx10_2.setGeometry(QtCore.QRect(1560, 920, 67, 17))
        self.vtrx10_2.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx10_2.setObjectName(_fromUtf8("vtrx10_2"))
        self.vtrx8_2 = QtGui.QLabel(self.tab_2)
        self.vtrx8_2.setGeometry(QtCore.QRect(1300, 920, 67, 17))
        self.vtrx8_2.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx8_2.setObjectName(_fromUtf8("vtrx8_2"))
        self.vtrx4_2 = QtGui.QLabel(self.tab_2)
        self.vtrx4_2.setGeometry(QtCore.QRect(720, 930, 67, 17))
        self.vtrx4_2.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx4_2.setObjectName(_fromUtf8("vtrx4_2"))
        self.vtrx1_2 = QtGui.QLabel(self.tab_2)
        self.vtrx1_2.setGeometry(QtCore.QRect(430, 920, 67, 17))
        self.vtrx1_2.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx1_2.setObjectName(_fromUtf8("vtrx1_2"))
        self.vtrx3_2 = QtGui.QLabel(self.tab_2)
        self.vtrx3_2.setGeometry(QtCore.QRect(620, 920, 67, 17))
        self.vtrx3_2.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx3_2.setObjectName(_fromUtf8("vtrx3_2"))
        self.vtrx11_2 = QtGui.QLabel(self.tab_2)
        self.vtrx11_2.setGeometry(QtCore.QRect(1660, 930, 67, 17))
        self.vtrx11_2.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx11_2.setObjectName(_fromUtf8("vtrx11_2"))
        self.vtrx2_2 = QtGui.QLabel(self.tab_2)
        self.vtrx2_2.setGeometry(QtCore.QRect(520, 920, 67, 17))
        self.vtrx2_2.setMinimumSize(QtCore.QSize(67, 0))
        self.vtrx2_2.setObjectName(_fromUtf8("vtrx2_2"))
        self.label_136 = QtGui.QLabel(self.tab_2)
        self.label_136.setGeometry(QtCore.QRect(490, 120, 111, 17))
        self.label_136.setObjectName(_fromUtf8("label_136"))
        self.wedgeid_2 = QtGui.QLineEdit(self.tab_2)
        self.wedgeid_2.setGeometry(QtCore.QRect(600, 120, 301, 21))
        self.wedgeid_2.setObjectName(_fromUtf8("wedgeid_2"))
        self.line_23 = QtGui.QFrame(self.tab_2)
        self.line_23.setGeometry(QtCore.QRect(470, 110, 3, 61))
        self.line_23.setFrameShape(QtGui.QFrame.VLine)
        self.line_23.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_23.setObjectName(_fromUtf8("line_23"))
        self.upload_btn_2 = QtGui.QPushButton(self.tab_2)
        self.upload_btn_2.setGeometry(QtCore.QRect(490, 150, 411, 21))
        self.upload_btn_2.setObjectName(_fromUtf8("upload_btn_2"))
        self.tabWidget.addTab(self.tab_2, _fromUtf8(""))
        self.tab_3 = QtGui.QWidget()
        self.tab_3.setObjectName(_fromUtf8("tab_3"))
        self.verticalLayoutWidget_18 = QtGui.QWidget(self.tab_3)
        self.verticalLayoutWidget_18.setGeometry(QtCore.QRect(0, 0, 1771, 851))
        self.verticalLayoutWidget_18.setObjectName(_fromUtf8("verticalLayoutWidget_18"))
        self.verticalLayout_18 = QtGui.QVBoxLayout(self.verticalLayoutWidget_18)
        self.verticalLayout_18.setMargin(0)
        self.verticalLayout_18.setObjectName(_fromUtf8("verticalLayout_18"))
        self.frame_18 = QtGui.QFrame(self.verticalLayoutWidget_18)
        self.frame_18.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_18.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_18.setObjectName(_fromUtf8("frame_18"))
        self.production_loc = QtGui.QLabel(self.frame_18)
        self.production_loc.setGeometry(QtCore.QRect(10, 40, 461, 17))
        self.production_loc.setObjectName(_fromUtf8("production_loc"))
        self.production_btn = QtGui.QPushButton(self.frame_18)
        self.production_btn.setGeometry(QtCore.QRect(160, 10, 141, 21))
        self.production_btn.setObjectName(_fromUtf8("production_btn"))
        self.label_285 = QtGui.QLabel(self.frame_18)
        self.label_285.setGeometry(QtCore.QRect(520, 10, 141, 17))
        self.label_285.setObjectName(_fromUtf8("label_285"))
        self.digital_loc = QtGui.QLabel(self.frame_18)
        self.digital_loc.setGeometry(QtCore.QRect(520, 40, 281, 17))
        self.digital_loc.setObjectName(_fromUtf8("digital_loc"))
        self.label_284 = QtGui.QLabel(self.frame_18)
        self.label_284.setGeometry(QtCore.QRect(10, 10, 141, 17))
        self.label_284.setObjectName(_fromUtf8("label_284"))
        self.digital_btn = QtGui.QPushButton(self.frame_18)
        self.digital_btn.setGeometry(QtCore.QRect(660, 10, 141, 21))
        self.digital_btn.setObjectName(_fromUtf8("digital_btn"))
        self.line_37 = QtGui.QFrame(self.frame_18)
        self.line_37.setGeometry(QtCore.QRect(480, 10, 3, 61))
        self.line_37.setFrameShape(QtGui.QFrame.VLine)
        self.line_37.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_37.setObjectName(_fromUtf8("line_37"))
        self.pushButton = QtGui.QPushButton(self.frame_18)
        self.pushButton.setGeometry(QtCore.QRect(110, 80, 161, 21))
        self.pushButton.setObjectName(_fromUtf8("pushButton"))
        self.verticalLayoutWidget_19 = QtGui.QWidget(self.frame_18)
        self.verticalLayoutWidget_19.setGeometry(QtCore.QRect(20, 120, 381, 201))
        self.verticalLayoutWidget_19.setObjectName(_fromUtf8("verticalLayoutWidget_19"))
        self.verticalLayout_19 = QtGui.QVBoxLayout(self.verticalLayoutWidget_19)
        self.verticalLayout_19.setMargin(0)
        self.verticalLayout_19.setObjectName(_fromUtf8("verticalLayout_19"))
        self.frame_31 = QtGui.QFrame(self.verticalLayoutWidget_19)
        self.frame_31.setFrameShape(QtGui.QFrame.StyledPanel)
        self.frame_31.setFrameShadow(QtGui.QFrame.Raised)
        self.frame_31.setObjectName(_fromUtf8("frame_31"))
        self.label_286 = QtGui.QLabel(self.frame_31)
        self.label_286.setGeometry(QtCore.QRect(30, 10, 121, 31))
        self.label_286.setObjectName(_fromUtf8("label_286"))
        self.mtfout1 = QtGui.QLabel(self.frame_31)
        self.mtfout1.setGeometry(QtCore.QRect(170, 10, 201, 31))
        self.mtfout1.setObjectName(_fromUtf8("mtfout1"))
        self.estat0_3 = QtGui.QProgressBar(self.frame_31)
        self.estat0_3.setGeometry(QtCore.QRect(340, 70, 21, 23))
        self.estat0_3.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat0_3.setMaximum(100)
        self.estat0_3.setProperty("value", 0)
        self.estat0_3.setTextVisible(False)
        self.estat0_3.setOrientation(QtCore.Qt.Vertical)
        self.estat0_3.setInvertedAppearance(False)
        self.estat0_3.setObjectName(_fromUtf8("estat0_3"))
        self.estat0_4 = QtGui.QProgressBar(self.frame_31)
        self.estat0_4.setGeometry(QtCore.QRect(340, 110, 21, 23))
        self.estat0_4.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat0_4.setMaximum(100)
        self.estat0_4.setProperty("value", 0)
        self.estat0_4.setTextVisible(False)
        self.estat0_4.setOrientation(QtCore.Qt.Vertical)
        self.estat0_4.setInvertedAppearance(False)
        self.estat0_4.setObjectName(_fromUtf8("estat0_4"))
        self.estat0_5 = QtGui.QProgressBar(self.frame_31)
        self.estat0_5.setGeometry(QtCore.QRect(340, 150, 21, 23))
        self.estat0_5.setStyleSheet(_fromUtf8("QProgressBar{\n"
"    border: 2px solid gray;\n"
"    border-radius: 3px;\n"
"    text-align: center;\n"
"    background-color: qlineargradient(spread:pad, x1:0.5, y1:1, x2:0.5, y2:0, \n"
"                               stop:0 black, stop:0.33 black,\n"
"                               stop:0.6 black, stop:1 black);\n"
"}\n"
"\n"
"QProgressBar::chunk {\n"
"    background-color: lime;\n"
"    margin: 0px;\n"
"    height: 5px;\n"
"}\n"
""))
        self.estat0_5.setMaximum(100)
        self.estat0_5.setProperty("value", 0)
        self.estat0_5.setTextVisible(False)
        self.estat0_5.setOrientation(QtCore.Qt.Vertical)
        self.estat0_5.setInvertedAppearance(False)
        self.estat0_5.setObjectName(_fromUtf8("estat0_5"))
        self.label_288 = QtGui.QLabel(self.frame_31)
        self.label_288.setGeometry(QtCore.QRect(10, 70, 67, 17))
        self.label_288.setObjectName(_fromUtf8("label_288"))
        self.label_289 = QtGui.QLabel(self.frame_31)
        self.label_289.setGeometry(QtCore.QRect(10, 110, 67, 17))
        self.label_289.setObjectName(_fromUtf8("label_289"))
        self.label_290 = QtGui.QLabel(self.frame_31)
        self.label_290.setGeometry(QtCore.QRect(10, 150, 67, 17))
        self.label_290.setObjectName(_fromUtf8("label_290"))
        self.label_287 = QtGui.QLabel(self.frame_31)
        self.label_287.setGeometry(QtCore.QRect(180, 70, 21, 17))
        self.label_287.setObjectName(_fromUtf8("label_287"))
        self.label_291 = QtGui.QLabel(self.frame_31)
        self.label_291.setGeometry(QtCore.QRect(180, 110, 21, 17))
        self.label_291.setObjectName(_fromUtf8("label_291"))
        self.label_292 = QtGui.QLabel(self.frame_31)
        self.label_292.setGeometry(QtCore.QRect(180, 150, 21, 17))
        self.label_292.setObjectName(_fromUtf8("label_292"))
        self.line_36 = QtGui.QFrame(self.frame_31)
        self.line_36.setGeometry(QtCore.QRect(310, 60, 20, 121))
        self.line_36.setFrameShape(QtGui.QFrame.VLine)
        self.line_36.setFrameShadow(QtGui.QFrame.Sunken)
        self.line_36.setObjectName(_fromUtf8("line_36"))
        self.sca_id1 = QtGui.QLabel(self.frame_31)
        self.sca_id1.setGeometry(QtCore.QRect(70, 70, 101, 16))
        self.sca_id1.setObjectName(_fromUtf8("sca_id1"))
        self.sca_id1_2 = QtGui.QLabel(self.frame_31)
        self.sca_id1_2.setGeometry(QtCore.QRect(70, 110, 101, 16))
        self.sca_id1_2.setObjectName(_fromUtf8("sca_id1_2"))
        self.sca_id1_3 = QtGui.QLabel(self.frame_31)
        self.sca_id1_3.setGeometry(QtCore.QRect(70, 150, 101, 16))
        self.sca_id1_3.setObjectName(_fromUtf8("sca_id1_3"))
        self.sca_id1_4 = QtGui.QLabel(self.frame_31)
        self.sca_id1_4.setGeometry(QtCore.QRect(210, 70, 101, 16))
        self.sca_id1_4.setObjectName(_fromUtf8("sca_id1_4"))
        self.sca_id1_5 = QtGui.QLabel(self.frame_31)
        self.sca_id1_5.setGeometry(QtCore.QRect(210, 110, 101, 16))
        self.sca_id1_5.setObjectName(_fromUtf8("sca_id1_5"))
        self.sca_id1_6 = QtGui.QLabel(self.frame_31)
        self.sca_id1_6.setGeometry(QtCore.QRect(210, 150, 101, 16))
        self.sca_id1_6.setObjectName(_fromUtf8("sca_id1_6"))
        self.label_293 = QtGui.QLabel(self.frame_31)
        self.label_293.setGeometry(QtCore.QRect(80, 50, 91, 17))
        self.label_293.setObjectName(_fromUtf8("label_293"))
        self.label_294 = QtGui.QLabel(self.frame_31)
        self.label_294.setGeometry(QtCore.QRect(230, 50, 81, 17))
        self.label_294.setObjectName(_fromUtf8("label_294"))
        self.verticalLayout_19.addWidget(self.frame_31)
        self.verticalLayout_18.addWidget(self.frame_18)
        self.tabWidget.addTab(self.tab_3, _fromUtf8(""))
        MainWindow.setCentralWidget(self.centralwidget)
        self.statusbar = QtGui.QStatusBar(MainWindow)
        self.statusbar.setObjectName(_fromUtf8("statusbar"))
        MainWindow.setStatusBar(self.statusbar)
        self.actionhello = QtGui.QAction(MainWindow)
        self.actionhello.setObjectName(_fromUtf8("actionhello"))

        self.retranslateUi(MainWindow)
        self.tabWidget.setCurrentIndex(2)
        self.hextabs.setCurrentIndex(6)
        self.tabWidget_3.setCurrentIndex(6)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)
        MainWindow.setTabOrder(self.pingbtn, self.vtrxbtn)
        MainWindow.setTabOrder(self.vtrxbtn, self.gbtx_write)
        MainWindow.setTabOrder(self.gbtx_write, self.gbtx_read)
        MainWindow.setTabOrder(self.gbtx_read, self.open_btn)
        MainWindow.setTabOrder(self.open_btn, self.save_btn)
        MainWindow.setTabOrder(self.save_btn, self.gbtx0)
        MainWindow.setTabOrder(self.gbtx0, self.gbtx1)
        MainWindow.setTabOrder(self.gbtx1, self.gbtx2)
        MainWindow.setTabOrder(self.gbtx2, self.gbtx3)
        MainWindow.setTabOrder(self.gbtx3, self.gbtx4)
        MainWindow.setTabOrder(self.gbtx4, self.gbtx5)
        MainWindow.setTabOrder(self.gbtx5, self.gbtx6)
        MainWindow.setTabOrder(self.gbtx6, self.gbtx7)
        MainWindow.setTabOrder(self.gbtx7, self.flx0)
        MainWindow.setTabOrder(self.flx0, self.flx1)
        MainWindow.setTabOrder(self.flx1, self.flx2)
        MainWindow.setTabOrder(self.flx2, self.flx3)
        MainWindow.setTabOrder(self.flx3, self.flx4)
        MainWindow.setTabOrder(self.flx4, self.flx5)
        MainWindow.setTabOrder(self.flx5, self.flx6)
        MainWindow.setTabOrder(self.flx6, self.flx7)
        MainWindow.setTabOrder(self.flx7, self.phase0)
        MainWindow.setTabOrder(self.phase0, self.phase1)
        MainWindow.setTabOrder(self.phase1, self.phase2)
        MainWindow.setTabOrder(self.phase2, self.phase3)
        MainWindow.setTabOrder(self.phase3, self.phase4)
        MainWindow.setTabOrder(self.phase4, self.mtf0)
        MainWindow.setTabOrder(self.mtf0, self.mtf1)
        MainWindow.setTabOrder(self.mtf1, self.mtf2)
        MainWindow.setTabOrder(self.mtf2, self.mtf3)
        MainWindow.setTabOrder(self.mtf3, self.mtf4)
        MainWindow.setTabOrder(self.mtf4, self.mtf5)
        MainWindow.setTabOrder(self.mtf5, self.mtf6)
        MainWindow.setTabOrder(self.mtf6, self.mtf7)
        MainWindow.setTabOrder(self.mtf7, self.hextabs)
        MainWindow.setTabOrder(self.hextabs, self.wedgeid)
        MainWindow.setTabOrder(self.wedgeid, self.upload_btn)
        MainWindow.setTabOrder(self.upload_btn, self.default_vtn)

    def retranslateUi(self, MainWindow):
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow", None))
        self.vtrx0.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx1.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx3.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx4.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx5.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx6.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx7.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx8.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx9.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx10.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx11.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.fiber4.setText(_translate("MainWindow", "3", None))
        self.fiber3.setText(_translate("MainWindow", "2", None))
        self.fiber8.setText(_translate("MainWindow", "8", None))
        self.gbtx_nbr.setToolTip(_translate("MainWindow", "<html><head/><body><p>Which GBTx the fiber is connected to (1/2).</p></body></html>", None))
        self.gbtx_nbr.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">GBTX Nbr.</span></p></body></html>", None))
        self.fiber6.setText(_translate("MainWindow", "5", None))
        self.fiber1.setText(_translate("MainWindow", "0", None))
        self.fiber2.setText(_translate("MainWindow", "1", None))
        self.gbtx_nbr_2.setToolTip(_translate("MainWindow", "<html><head/><body><p>The FLX-card (0/1 or 2/3). Designates which FLX-card device to use.</p></body></html>", None))
        self.gbtx_nbr_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">FLX Card Nbr.</span></p></body></html>", None))
        self.fiber_nbr.setToolTip(_translate("MainWindow", "<html><head/><body><p>The fiber number / the GBT link number.</p></body></html>", None))
        self.fiber_nbr.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Fiber Nbr.</span></p></body></html>", None))
        self.fiber7.setText(_translate("MainWindow", "7", None))
        self.fiber5.setText(_translate("MainWindow", "6", None))
        self.label_63.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt; font-weight:600;\">GBTX &amp; FLX Card Mapping</span></p></body></html>", None))
        self.gbtx_write.setText(_translate("MainWindow", "Configure", None))
        self.gbtx_read.setText(_translate("MainWindow", "Read Configuration", None))
        self.label_57.setText(_translate("MainWindow", "1", None))
        self.label_55.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Group</span></p></body></html>", None))
        self.label_56.setText(_translate("MainWindow", "0", None))
        self.label_61.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Phase</span></p></body></html>", None))
        self.label_58.setText(_translate("MainWindow", "2", None))
        self.label_54.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt; font-weight:600;\">GBTX Group Phases</span></p></body></html>", None))
        self.label_60.setText(_translate("MainWindow", "4", None))
        self.label_59.setText(_translate("MainWindow", "3", None))
        self.label_67.setText(_translate("MainWindow", "5 (inactive)", None))
        self.label_69.setText(_translate("MainWindow", "6 (inactive)", None))
        self.hextabs.setTabText(self.hextabs.indexOf(self.gch0), _translate("MainWindow", "Board 0", None))
        self.hextabs.setTabText(self.hextabs.indexOf(self.gch1), _translate("MainWindow", "Board 1", None))
        self.hextabs.setTabText(self.hextabs.indexOf(self.gch2), _translate("MainWindow", "Board 2", None))
        self.hextabs.setTabText(self.hextabs.indexOf(self.gch3), _translate("MainWindow", "Board 3", None))
        self.hextabs.setTabText(self.hextabs.indexOf(self.gch4), _translate("MainWindow", "Board 4", None))
        self.hextabs.setTabText(self.hextabs.indexOf(self.gch5), _translate("MainWindow", "Board 5", None))
        self.hextabs.setTabText(self.hextabs.indexOf(self.gch6), _translate("MainWindow", "Board 6", None))
        self.hextabs.setTabText(self.hextabs.indexOf(self.gch7), _translate("MainWindow", "Board 7", None))
        self.label_64.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt; font-weight:600;\">GBTX Hex Registry Configurations</span></p></body></html>", None))
        self.label_11.setText(_translate("MainWindow", "File:", None))
        self.label_12.setText(_translate("MainWindow", "Config:", None))
        self.open_btn.setText(_translate("MainWindow", "Open", None))
        self.save_btn.setText(_translate("MainWindow", "Save", None))
        self.upload_btn.setText(_translate("MainWindow", "Upload L1DDC Mapping", None))
        self.label_9.setText(_translate("MainWindow", "Wedge MTF ID:", None))
        self.label_68.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt; font-weight:600;\">MTF Numbers (Manual)</span></p></body></html>", None))
        self.label_14.setText(_translate("MainWindow", "Board 0", None))
        self.label_15.setText(_translate("MainWindow", "Board 1", None))
        self.label_16.setText(_translate("MainWindow", "Board 2", None))
        self.label_17.setText(_translate("MainWindow", "Board 3", None))
        self.label_18.setText(_translate("MainWindow", "Board 4", None))
        self.label_19.setText(_translate("MainWindow", "Board 5", None))
        self.label_20.setText(_translate("MainWindow", "Board 6", None))
        self.label_21.setText(_translate("MainWindow", "Board 7", None))
        self.default_vtn.setText(_translate("MainWindow", "Load Default Configuration", None))
        self.fileloc.setText(_translate("MainWindow", "/afs/cern.ch/user/m/mkongsor/L1DDC/", None))
        self.label_133.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt; font-weight:600;\">GBTX Phase Lock Status</span></p></body></html>", None))
        self.label_134.setText(_translate("MainWindow", "1", None))
        self.label_135.setText(_translate("MainWindow", "6", None))
        self.label_137.setText(_translate("MainWindow", "4", None))
        self.label_138.setText(_translate("MainWindow", "GBTX", None))
        self.readstat.setText(_translate("MainWindow", "Read Status", None))
        self.label_139.setText(_translate("MainWindow", "All Groups Locked", None))
        self.label_140.setText(_translate("MainWindow", "3", None))
        self.label_141.setText(_translate("MainWindow", "0", None))
        self.label_142.setText(_translate("MainWindow", "5", None))
        self.label_143.setText(_translate("MainWindow", "2", None))
        self.label_144.setText(_translate("MainWindow", "7", None))
        self.tabWidget_3.setTabText(self.tabWidget_3.indexOf(self.tab_13), _translate("MainWindow", "Overview", None))
        self.label_70.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt; font-weight:600;\">GBTX1 Phase Lock Status</span></p></body></html>", None))
        self.label_26.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">EportRX Phase Aligner</span></p></body></html>", None))
        self.label_27.setText(_translate("MainWindow", "Group", None))
        self.label_28.setText(_translate("MainWindow", "All Channels Locked", None))
        self.label_29.setText(_translate("MainWindow", "0", None))
        self.label_31.setText(_translate("MainWindow", "1", None))
        self.label_32.setText(_translate("MainWindow", "2", None))
        self.label_33.setText(_translate("MainWindow", "3", None))
        self.label_34.setText(_translate("MainWindow", "4", None))
        self.label_35.setText(_translate("MainWindow", "5", None))
        self.label_36.setText(_translate("MainWindow", "6", None))
        self.label_37.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">EportRX dll</span></p></body></html>", None))
        self.label_38.setText(_translate("MainWindow", "Group 0-4 locked", None))
        self.tabWidget_3.setTabText(self.tabWidget_3.indexOf(self.tab_5), _translate("MainWindow", "GBTX0", None))
        self.label_66.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt; font-weight:600;\">GBTX Phase Lock Status</span></p></body></html>", None))
        self.label_3.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">EportRX Phase Aligner</span></p></body></html>", None))
        self.label_4.setText(_translate("MainWindow", "Group", None))
        self.label_5.setText(_translate("MainWindow", "All Channels Locked", None))
        self.label_6.setText(_translate("MainWindow", "0", None))
        self.label_7.setText(_translate("MainWindow", "1", None))
        self.label_8.setText(_translate("MainWindow", "2", None))
        self.label_10.setText(_translate("MainWindow", "3", None))
        self.label_13.setText(_translate("MainWindow", "4", None))
        self.label_22.setText(_translate("MainWindow", "5", None))
        self.label_23.setText(_translate("MainWindow", "6", None))
        self.label_24.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">EportRX dll</span></p></body></html>", None))
        self.label_25.setText(_translate("MainWindow", "Group 0-4 locked", None))
        self.tabWidget_3.setTabText(self.tabWidget_3.indexOf(self.tab_6), _translate("MainWindow", "GBTX1", None))
        self.label_71.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt; font-weight:600;\">GBTX Phase Lock Status</span></p></body></html>", None))
        self.label_39.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">EportRX Phase Aligner</span></p></body></html>", None))
        self.label_40.setText(_translate("MainWindow", "Group", None))
        self.label_41.setText(_translate("MainWindow", "All Channels Locked", None))
        self.label_42.setText(_translate("MainWindow", "0", None))
        self.label_43.setText(_translate("MainWindow", "1", None))
        self.label_44.setText(_translate("MainWindow", "2", None))
        self.label_45.setText(_translate("MainWindow", "3", None))
        self.label_46.setText(_translate("MainWindow", "4", None))
        self.label_47.setText(_translate("MainWindow", "5", None))
        self.label_48.setText(_translate("MainWindow", "6", None))
        self.label_49.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">EportRX dll</span></p></body></html>", None))
        self.label_50.setText(_translate("MainWindow", "Group 0-4 locked", None))
        self.tabWidget_3.setTabText(self.tabWidget_3.indexOf(self.tab_7), _translate("MainWindow", "GBTX2", None))
        self.label_72.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt; font-weight:600;\">GBTX Phase Lock Status</span></p></body></html>", None))
        self.label_51.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">EportRX Phase Aligner</span></p></body></html>", None))
        self.label_52.setText(_translate("MainWindow", "Group", None))
        self.label_53.setText(_translate("MainWindow", "All Channels Locked", None))
        self.label_65.setText(_translate("MainWindow", "0", None))
        self.label_73.setText(_translate("MainWindow", "1", None))
        self.label_74.setText(_translate("MainWindow", "2", None))
        self.label_75.setText(_translate("MainWindow", "3", None))
        self.label_76.setText(_translate("MainWindow", "4", None))
        self.label_77.setText(_translate("MainWindow", "5", None))
        self.label_78.setText(_translate("MainWindow", "6", None))
        self.label_79.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">EportRX dll</span></p></body></html>", None))
        self.label_80.setText(_translate("MainWindow", "Group 0-4 locked", None))
        self.tabWidget_3.setTabText(self.tabWidget_3.indexOf(self.tab_8), _translate("MainWindow", "GBTX3", None))
        self.label_81.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt; font-weight:600;\">GBTX Phase Lock Status</span></p></body></html>", None))
        self.label_82.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">EportRX Phase Aligner</span></p></body></html>", None))
        self.label_83.setText(_translate("MainWindow", "Group", None))
        self.label_84.setText(_translate("MainWindow", "All Channels Locked", None))
        self.label_85.setText(_translate("MainWindow", "0", None))
        self.label_86.setText(_translate("MainWindow", "1", None))
        self.label_87.setText(_translate("MainWindow", "2", None))
        self.label_88.setText(_translate("MainWindow", "3", None))
        self.label_89.setText(_translate("MainWindow", "4", None))
        self.label_90.setText(_translate("MainWindow", "5", None))
        self.label_91.setText(_translate("MainWindow", "6", None))
        self.label_92.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">EportRX dll</span></p></body></html>", None))
        self.label_93.setText(_translate("MainWindow", "Group 0-4 locked", None))
        self.tabWidget_3.setTabText(self.tabWidget_3.indexOf(self.tab_9), _translate("MainWindow", "GBTX4", None))
        self.label_94.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt; font-weight:600;\">GBTX Phase Lock Status</span></p></body></html>", None))
        self.label_95.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">EportRX Phase Aligner</span></p></body></html>", None))
        self.label_96.setText(_translate("MainWindow", "Group", None))
        self.label_97.setText(_translate("MainWindow", "All Channels Locked", None))
        self.label_98.setText(_translate("MainWindow", "0", None))
        self.label_99.setText(_translate("MainWindow", "1", None))
        self.label_100.setText(_translate("MainWindow", "2", None))
        self.label_101.setText(_translate("MainWindow", "3", None))
        self.label_102.setText(_translate("MainWindow", "4", None))
        self.label_103.setText(_translate("MainWindow", "5", None))
        self.label_104.setText(_translate("MainWindow", "6", None))
        self.label_105.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">EportRX dll</span></p></body></html>", None))
        self.label_106.setText(_translate("MainWindow", "Group 0-4 locked", None))
        self.tabWidget_3.setTabText(self.tabWidget_3.indexOf(self.tab_10), _translate("MainWindow", "GBTX5", None))
        self.label_107.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt; font-weight:600;\">GBTX Phase Lock Status</span></p></body></html>", None))
        self.label_108.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">EportRX Phase Aligner</span></p></body></html>", None))
        self.label_109.setText(_translate("MainWindow", "Group", None))
        self.label_110.setText(_translate("MainWindow", "All Channels Locked", None))
        self.label_111.setText(_translate("MainWindow", "0", None))
        self.label_112.setText(_translate("MainWindow", "1", None))
        self.label_113.setText(_translate("MainWindow", "2", None))
        self.label_114.setText(_translate("MainWindow", "3", None))
        self.label_115.setText(_translate("MainWindow", "4", None))
        self.label_116.setText(_translate("MainWindow", "5", None))
        self.label_117.setText(_translate("MainWindow", "6", None))
        self.label_118.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">EportRX dll</span></p></body></html>", None))
        self.label_119.setText(_translate("MainWindow", "Group 0-4 locked", None))
        self.tabWidget_3.setTabText(self.tabWidget_3.indexOf(self.tab_11), _translate("MainWindow", "GBTX6", None))
        self.label_120.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:16pt; font-weight:600;\">GBTX Phase Lock Status</span></p></body></html>", None))
        self.label_121.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">EportRX Phase Aligner</span></p></body></html>", None))
        self.label_122.setText(_translate("MainWindow", "Group", None))
        self.label_123.setText(_translate("MainWindow", "All Channels Locked", None))
        self.label_124.setText(_translate("MainWindow", "0", None))
        self.label_125.setText(_translate("MainWindow", "1", None))
        self.label_126.setText(_translate("MainWindow", "2", None))
        self.label_127.setText(_translate("MainWindow", "3", None))
        self.label_128.setText(_translate("MainWindow", "4", None))
        self.label_129.setText(_translate("MainWindow", "5", None))
        self.label_130.setText(_translate("MainWindow", "6", None))
        self.label_131.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">EportRX dll</span></p></body></html>", None))
        self.label_132.setText(_translate("MainWindow", "Group 0-4 locked", None))
        self.tabWidget_3.setTabText(self.tabWidget_3.indexOf(self.tab_12), _translate("MainWindow", "GBTX7", None))
        self.febadd_label.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">FEB</span></p></body></html>", None))
        self.elff.setText(_translate("MainWindow", "ff", None))
        self.elbf.setText(_translate("MainWindow", "bf", None))
        self.el7f.setText(_translate("MainWindow", "7f", None))
        self.pingbtn.setToolTip(_translate("MainWindow", "<html><head/><body><p>Press this button to initiate pinging the SCAs.</p></body></html>", None))
        self.pingbtn.setText(_translate("MainWindow", "Ping SCAs", None))
        self.scalabel.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-weight:600;\">L1DDC SCA Connections</span></p></body></html>", None))
        self.el17f.setText(_translate("MainWindow", "17f", None))
        self.elink_label.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-weight:600;\">Elink</span></p></body></html>", None))
        self.el1ff.setText(_translate("MainWindow", "1ff", None))
        self.sernbr_label.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Serial</span></p></body></html>", None))
        self.febadd_label_3.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Status</span></p></body></html>", None))
        self.el1bf.setText(_translate("MainWindow", "1bf", None))
        self.febadd_label_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Fibers</span></p></body></html>", None))
        self.el3f.setText(_translate("MainWindow", "3f", None))
        self.el13f.setText(_translate("MainWindow", "13f", None))
        self.label_30.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:18pt; font-weight:600;\">L1DDC Settings GUI</span></p></body></html>", None))
        self.label_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Marius Kongsore, University of Michigan</span></p></body></html>", None))
        self.label.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Contact: marius.kongsore@cern.ch</span></p></body></html>", None))
        self.label_62.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-weight:600;\">VTRX Connections</span></p></body></html>", None))
        self.febadd_label_4.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Status</span></p></body></html>", None))
        self.febadd_label_5.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Status</span></p></body></html>", None))
        self.elink_label_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Chan</span></p></body></html>", None))
        self.elink_label_3.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Chan</span></p></body></html>", None))
        self.el3f_2.setText(_translate("MainWindow", "0", None))
        self.el3f_3.setText(_translate("MainWindow", "1", None))
        self.el3f_4.setText(_translate("MainWindow", "2", None))
        self.el3f_5.setText(_translate("MainWindow", "3", None))
        self.el3f_6.setText(_translate("MainWindow", "4", None))
        self.el3f_7.setText(_translate("MainWindow", "5", None))
        self.el3f_8.setText(_translate("MainWindow", "6", None))
        self.el3f_9.setText(_translate("MainWindow", "7", None))
        self.el3f_10.setText(_translate("MainWindow", "8", None))
        self.el3f_11.setText(_translate("MainWindow", "9", None))
        self.el3f_12.setText(_translate("MainWindow", "10", None))
        self.el3f_13.setText(_translate("MainWindow", "11", None))
        self.vtrxbtn.setText(_translate("MainWindow", "Check VTRXs", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab), _translate("MainWindow", "L1DDC Settings GUI", None))
        self.vtrx5_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx7_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.febadd_label_6.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">FEB</span></p></body></html>", None))
        self.elff_2.setText(_translate("MainWindow", "40", None))
        self.elbf_2.setText(_translate("MainWindow", "2", None))
        self.el7f_2.setText(_translate("MainWindow", "1", None))
        self.pingbtn_feb.setToolTip(_translate("MainWindow", "<html><head/><body><p>Press this button to initiate pinging the SCAs.</p></body></html>", None))
        self.pingbtn_feb.setText(_translate("MainWindow", "Ping SCAs", None))
        self.scalabel_2.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-size:18pt; font-weight:600;\">FEB SCA Connections</span></p></body></html>", None))
        self.el17f_2.setText(_translate("MainWindow", "42", None))
        self.elink_label_4.setText(_translate("MainWindow", "<html><head/><body><p align=\"center\"><span style=\" font-weight:600;\">Elink</span></p></body></html>", None))
        self.el1ff_2.setText(_translate("MainWindow", "81", None))
        self.sernbr_label_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Serial</span></p></body></html>", None))
        self.febadd_label_7.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Status</span></p></body></html>", None))
        self.el1bf_2.setText(_translate("MainWindow", "80", None))
        self.febadd_label_8.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Fibers</span></p></body></html>", None))
        self.el3f_14.setText(_translate("MainWindow", "0", None))
        self.el13f_2.setText(_translate("MainWindow", "41", None))
        self.el1ff_3.setText(_translate("MainWindow", "82", None))
        self.el1ff_4.setText(_translate("MainWindow", "C0", None))
        self.el1ff_5.setText(_translate("MainWindow", "C1", None))
        self.el1ff_6.setText(_translate("MainWindow", "C2", None))
        self.el1ff_7.setText(_translate("MainWindow", "100", None))
        self.el1ff_8.setText(_translate("MainWindow", "101", None))
        self.el1ff_9.setText(_translate("MainWindow", "102", None))
        self.el1ff_10.setText(_translate("MainWindow", "140", None))
        self.el1ff_11.setText(_translate("MainWindow", "141", None))
        self.el1ff_12.setText(_translate("MainWindow", "142", None))
        self.el1ff_13.setText(_translate("MainWindow", "180", None))
        self.el1ff_14.setText(_translate("MainWindow", "181", None))
        self.el1ff_15.setText(_translate("MainWindow", "182", None))
        self.el1ff_16.setText(_translate("MainWindow", "1C0", None))
        self.el1ff_17.setText(_translate("MainWindow", "1C1", None))
        self.el1ff_18.setText(_translate("MainWindow", "1C2", None))
        self.vtrx0_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx6_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx9_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx10_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx8_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx4_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx1_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx3_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx11_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.vtrx2_2.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:6pt; color:#ffffff;\">TextLabel</span></p></body></html>", None))
        self.label_136.setText(_translate("MainWindow", "Wedge MTF ID:", None))
        self.upload_btn_2.setText(_translate("MainWindow", "Upload FEB SCA Mapping", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_2), _translate("MainWindow", "FEB SCA Pinging", None))
        self.production_loc.setText(_translate("MainWindow", "/afs/cern.ch/user/m/mkongsor/L1DDC/", None))
        self.production_btn.setText(_translate("MainWindow", "Open", None))
        self.label_285.setText(_translate("MainWindow", "Digital Output File:", None))
        self.digital_loc.setText(_translate("MainWindow", "/afs/cern.ch/user/m/mkongsor/L1DDC/", None))
        self.label_284.setText(_translate("MainWindow", "Production Log File:", None))
        self.digital_btn.setText(_translate("MainWindow", "Open", None))
        self.pushButton.setToolTip(_translate("MainWindow", "<html><head/><body><p>Compare the SCA IDs as found digitally by the GUI with the SCA IDs recorded during the production of the L1DDCs.</p></body></html>", None))
        self.pushButton.setText(_translate("MainWindow", "Compare", None))
        self.label_286.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:20pt; font-weight:600;\">L1DDC 1:</span></p></body></html>", None))
        self.mtfout1.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-size:20pt;\">MTF</span></p></body></html>", None))
        self.label_288.setText(_translate("MainWindow", "SCA IDs:", None))
        self.label_289.setText(_translate("MainWindow", "VTRX 1:", None))
        self.label_290.setText(_translate("MainWindow", "VTRX 2:", None))
        self.label_287.setText(_translate("MainWindow", "vs.", None))
        self.label_291.setText(_translate("MainWindow", "vs.", None))
        self.label_292.setText(_translate("MainWindow", "vs.", None))
        self.sca_id1.setText(_translate("MainWindow", "ID", None))
        self.sca_id1_2.setText(_translate("MainWindow", "ID", None))
        self.sca_id1_3.setText(_translate("MainWindow", "ID", None))
        self.sca_id1_4.setText(_translate("MainWindow", "ID", None))
        self.sca_id1_5.setText(_translate("MainWindow", "ID", None))
        self.sca_id1_6.setText(_translate("MainWindow", "ID", None))
        self.label_293.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Production</span></p></body></html>", None))
        self.label_294.setText(_translate("MainWindow", "<html><head/><body><p><span style=\" font-weight:600;\">Digital</span></p></body></html>", None))
        self.tabWidget.setTabText(self.tabWidget.indexOf(self.tab_3), _translate("MainWindow", "Mapping Comparison", None))
        self.actionhello.setText(_translate("MainWindow", "hello", None))

